package edu.uw.bothell.css.dsl.mass.apps.BreadthFirst;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;

public class Crawler extends Agent implements Serializable {
    // function identifiers
    public static final int onArrival_ = 0;
    public static final int departure_ = 1;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case onArrival_: return onArrival( argument );
	case departure_: return departure( argument );
	}
	return null;
    }

    /**
     * Is the default constructor.
     */
    public Crawler( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg an integer array whose elements are:
     *            0th: the next node index, and
     */
    public Crawler( Object arg ) {
	Args2Agents arguments = ( Args2Agents )arg;
	nextNode = arguments.nextNode;

	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born, going to " +
				 " nextNode = " + nextNode );
    }

    /**
     * @param  arg has no actual parameters.
     * @return     null
     */
    public Object onArrival( Object arg ) {
	Node node = ( Node )getPlace( );

	if ( ( ( Node )getPlace( ) ).footprint == -1 ) {
	    // This is the 1st arrival of the crawler.
	    node.footprint = prevNode;

	    // Check all the neighbors from the new node.
	    int[] neighbors = node.neighbors;
	    int[] distances = node.distances;

	    // footprint == -2 means that I'm at the source node
	    if ( node.footprint == -2 && neighbors.length == 0 ||
		 node.footprint != -2 && neighbors.length == 1 ) {
		MASS.getLogger( ).debug( "agent(" + getAgentId( ) +
					 ") terminated onArrival at a deadend "
					 + getPlace( ).getIndex( )[0] );
		kill( );
	    }
	    else {
		// Set my next node before spawning children.
		nextNode = ( neighbors[0] != prevNode ) ? 
		    neighbors[0] : neighbors[1];

		// Spawn children to disseminate all the neighbors
		// except my previous and next nodes
		Args2Agents[] args
		    = new Args2Agents[( node.footprint == -2 ) ?
				      neighbors.length - 1:
				      neighbors.length - 2];
		
		for ( int i = 0, j = 0; i < neighbors.length; i++ ) {
		    if ( neighbors[i] == nextNode 
			 || neighbors[i] == prevNode )
			// skip the parent's next node or previous node
			continue;
		    args[j++] = new Args2Agents( neighbors[i] );
		}
		spawn( args.length, args );
	    }
	}
	else if ( node.footprint != prevNode && justMigrated ) {
	    // Another crawler agent has already visited. No more crawler
	    // dissemination
	    MASS.getLogger( ).debug( "agent(" + getAgentId( ) +
				     ") terminated onArrival at the revisited node "
				     + getPlace( ).getIndex( )[0] );

	    kill( );
	    return null;
	}
	else {
	    // if ( node.footprint == prevNode )
	    justMigrated = false;
	}

	return null;
    }

    /**
     * Migrates to a next node.
     * @param      arg that is void
     * @return     nothing
     */
    public Object departure( Object arg ) {
	// if I'm the very first agent just moving to the source, the source node's
	// prevNode should be -2: no previous node.
	prevNode = ( getAgentId( ) == 0 && getPlace( ).getIndex( )[0] == 0 && ( ( Node )getPlace( ) ).footprint == -1 ) ? -2 : getPlace( ).getIndex( )[0];

	justMigrated = true;
	migrate( nextNode );
	MASS.getLogger( ).debug( ": agent(" + getAgentId( ) + ") will migrate from " +
				 prevNode + " to " + nextNode );

	return null;
    }

    // private data members
    private int nextNode = -1;
    private int prevNode = -1;

    private boolean justMigrated = false;
}
