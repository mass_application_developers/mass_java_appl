package edu.uw.bothell.css.dsl.mass.apps.GradientDescent;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

/**
 * SwarmOptimization.java - Simulates a gradient descent
 * @author  Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since   November 2, 2017
 */
public class GradientDescent {
    /**
     * Simulates a gradient descent over a sin(1/2*x^2 - 1/4*y^2 + 3)*cos(2*x + 1 - e^y)
     * @param args TBD
     */
    public static void main( String[] args ) {

	int statAgents = 0;

	// Read and validate input parameters
	if ( args.length != 6 ) {
            System.err.println( "args = " + args.length + " should be 6:" +
				" size, nAgents, iteration, nProcs, nThrs show[y/n]" );
            System.exit( -1 );
        }
        int size = Integer.parseInt( args[0] );
	int nAgents = Integer.parseInt( args[1] );
	int iteration = Integer.parseInt( args[2] );
        if ( size < 1 || nAgents > size * size || iteration <= 0 ) {
            System.err.println( "size(" + size + ") should be > 0" );
	    System.err.println( "nAgents(" + nAgents + ") should be < size(" + size + ")" );
	    System.err.println( "iteration(" + iteration + " should be > 0 " );
            System.exit( -1 );
        }

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[3] );
	int nThr = Integer.parseInt( args[4] );
	boolean show = args[5].equals( "y" );
	System.out.println( "size = " + size + " nAgents = " + nAgents +
			    " nProc = " + nProc + " nThr = " + nThr );
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( arguments, nProc, nThr );

	// Create a SinCos space
	Places plain = new Places( 1, SinCos.class.getName( ), 1, null, size, size );
	plain.callAll( SinCos.init_ );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );

	// Create ghost spaces
	plain.exchangeBoundary( );
	
	// Instantiate an agent at each node
	Agents particles = new Agents( 2, Particle.class.getName( ), null, plain, nAgents );
	int[][] initLocations = new int[nAgents][2];
	int agentId = 0;
	int interval = ( int )( Math.sqrt( size * size / nAgents ) );
	while ( agentId < nAgents ) {
	    for ( int i = 0; i < size; i += interval ) {
		for ( int j = 0; j < size; j += interval ) {
		    if ( agentId == nAgents )
			break;
		    initLocations[agentId][0] = i;
		    initLocations[agentId][1] = j;
		    agentId++;
		}
	    }
	}
	particles.callAll( Particle.init_, initLocations );
	particles.manageAll( );

	BestXYV best = new BestXYV( 0, 0, Double.MAX_VALUE );
	Object[] retValues = new Object[nAgents];
	for ( int i = 0; i < nAgents; i++ )
	    retValues[i] = new BestXYV( 0, 0, 0.0 ); // Dummy args to agents. Used for receiving return values
	    
	for ( int i = 0; i < iteration; i++ ) {
	    Object currStep = ( Object )(new Integer( i ) );
	    if ( show )
		System.out.println( "*** iteration = " + i + "************" );

	    // STEP 1: get a new value and move to a next place
	    if ( show )
		System.out.println( "... step 1: touch and go..............." );
	    retValues = ( Object[] )particles.callAll( Particle.touchNgo_, retValues );
	    particles.manageAll( );

	    // STEP 2: find the best-known X, Y, V
	    if ( show )
		System.out.println( "... step 2: evaluate the best known...." );
	    for ( Object o : retValues )
		if ( ( ( BestXYV )o ).v < best.v ) {
		    best.x = ( ( BestXYV )o ).x;
		    best.y = ( ( BestXYV )o ).y;
		    best.v = ( ( BestXYV )o ).v;
		}
	    if ( show )
		System.out.println( "best know value so far [" + best.x + ", " + best.y + "] = " + best.v );
	}
	
	// Get results
	System.out.println( "Final best known value so far [" + best.x + ", " + best.y + "] = " + best.v );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	MASS.finish( );
    }
}
