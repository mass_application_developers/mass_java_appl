package edu.uw.bothell.css.dsl.mass.apps.GradientDescent;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

public class SinCos extends Place {
    // function identifiers
    public static final int init_ = 0;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case init_: return init( argument );
	}
	return null;
    }

    /**
     * Is the default constructor.
     */
    public SinCos( ) {
	super( );
    }

    public SinCos( Object arg ) {
    }

    // SinCos constants
    private static final double lower = -2.0;
    private static final double upper = 2.0;
    private static final double e = Math.E;

    // This place's [x,y] coordinates and value in the SinCos space
    public double x = 0.0;
    public double y = 0.0;
    public double value = 0.0;

    public Object init( Object arg ) {

	// Compute my [x,y] coordinates in [-lower, -lower] to [upper, upper]
	x = getIndex( )[0];
	y = getIndex( )[1];
	x = x / getSize( )[0] * ( upper - lower ) + lower;
	y = y / getSize( )[1] * ( upper - lower ) + lower;

	// Computer my value with the SinCos function
	// f(x) = sin(1/2x^2 - 1/4y^2 + 3)cos(2x + 1 - e^y)
	value = Math.sin( 0.5 * x * x - 0.25 * y * y + 3.0 ) * Math.cos( 2 * x + 1 - Math.pow( e, y ) );
	setOutMessage( new Double( value ) );

	MASS.getLogger( ).debug( "place[" + getIndex( )[0] + ", " + 
				 getIndex( )[1] + "] = " + "(" + x + ", " + y + ") = " 
				 + value );
	
	return null;
    }

    public double getValue( int x, int y ) {
	int[] offset = {x, y};
	Object value = getOutMessage( 1, offset );
	return ( value != null ) ? ( ( Double )value ).doubleValue( ) : Double.MAX_VALUE;
    }
}
