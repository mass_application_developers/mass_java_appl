package edu.uw.bothell.css.dsl.mass.apps.GradientDescent;
import java.util.*;

public class MinSinCosObject {
    private static final double lower = -2.0;
    private static final double upper = 2.0;
    private static final double e = Math.E;

    public int x;
    public int y;
    public double v;
    public MinSinCosObject( int x, int y, double v ) {
	this.x = x;
	this.y = y;
	this.v = v;
    }

    public static void main( String[] args ) {
	// Get the size.
	int size = Integer.parseInt( args[0] );

	// Create a space.
	MinSinCosObject[][] plain = new MinSinCosObject[size][size];

	// Generate values.
	for ( int i = 0; i < size; i++ )
	    for ( int j = 0; j < size; j++ ) {
		double x = ( double )i / size * ( upper - lower ) + lower;
		double y = ( double )j / size * ( upper - lower ) + lower;

		// Computer my value with the SinCos function
		// f(x) = sin(1/2x^2 - 1/4y^2 + 3)cos(2x + 1 - e^y)
		plain[i][j]
		    = new MinSinCosObject( i, j, Math.sin( 0.5 * x * x - 0.25 * y * y + 3.0 ) * Math.cos( 2 * x + 1 - Math.pow( e, y ) ) );
		//System.out.println( "place[" + i + ", " + j + "] = " + "(" + x + ", " + y + ") = " + plain[i][j] );
	    }

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );

	// Find the minimum
	int lowestX = 0;
	int lowestY = 0;
	double lowestV = Double.MAX_VALUE;
	
	for ( int i = 0; i < size; i++ )
	    for ( int j = 0; j < size; j++ ) {
		if ( plain[i][j].v < lowestV ) {
		    lowestX = i;
		    lowestY = j;
		    lowestV = plain[i][j].v;
		}
	    }

        // Get results
	System.out.println( "Final best known value so far [" + lowestX + ", " + lowestY + "] = " + lowestV );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );
		    
    }
}
