#!/bin/bash
# $1 node size (size * size)
# $2 nAgents
# $3 iterations
# $4 # computing nodes
# $5 # threads
# $6 [y/n] show
cd target
java -Xms1g -Xmx16g -jar GradientDescent-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 $4 $5 $6
cd classes
java -Xms1g -Xmx16g edu.uw.bothell.css.dsl.mass.apps.GradientDescent.MinSinCos $1

