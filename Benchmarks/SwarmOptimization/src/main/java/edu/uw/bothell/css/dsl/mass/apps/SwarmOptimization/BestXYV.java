package edu.uw.bothell.css.dsl.mass.apps.SwarmOptimization;
import java.io.*;

public class BestXYV implements Serializable {
    public int x = 0;
    public int y = 0;
    public double v = ( double )0.0;
    public BestXYV( int x, int y, double v ) {
	this.x = x;
	this.y = y;
	this.v = v;
    }
}
