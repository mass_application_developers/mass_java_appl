package edu.uw.bothell.css.dsl.mass.apps.SwarmOptimization;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Particle extends Agent implements Serializable {
    // function identifiers
    public static final int onArrival_   = 1;
    public static final int departure_   = 2;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case onArrival_:   return onArrival( argument );
	case departure_:   return departure( argument );
	}
	return null;
    }

    // private data members
    private static final int w = 1;
    private static final int wp = 1;
    private static final int wg = 1;
    private int velocityX = 0;
    private int velocityY = 0;
    private Random rand = null;
    private int bestPX = 0;
    private int bestPY = 0;
    private double bestPV = Double.MAX_VALUE;
    private int bestGX = 0;
    private int bestGY = 0;
    private double bestGV = 0;

    /**
     * Is the default constructor.
     */
    public Particle( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg should be null.
     */
    public Particle( Object arg ) {
	// nothing to do
	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born." );
    }

    public Object departure( Object arg ) {
	if ( rand == null ) { // initial departure
	    // Prapare a random number generate
	    int[] initValues = (int [])arg;
	    rand = new Random( initValues[0] * getPlace( ).getSize( )[0] + initValues[1] );

	    // Initialize my velocity between [-1,-1] and [1,1]
	    velocityX = rand.nextInt( 3 ) - 1;
	    velocityY = rand.nextInt( 3 ) - 1;

	    // Go to the initial position
	    go( initValues[0], initValues[1] );
	}
	else {   	    // 2nd or following departures
	    // Update my velocity
	    double rp = rand.nextDouble( );
	    double rg = rand.nextDouble( );
	    velocityX = ( int )( w * velocityX + wp * rp * ( bestPX - getPlace( ).getIndex( )[0] ) + wg * rg * ( bestGX - getPlace( ).getIndex( )[0] ) );
	    velocityY = ( int )( w * velocityY + wp * rp * ( bestPY - getPlace( ).getIndex( )[1] ) + wg * rg * ( bestGY - getPlace( ).getIndex( )[1] ) );

	    // Update my next place
	    int nextX = getPlace( ).getIndex( )[0] + velocityX;
	    int nextY = getPlace( ).getIndex( )[1] + velocityY;
	    if ( nextX < 0 ) nextX = 0; if ( nextX >= getPlace( ).getSize( )[0] ) nextX = getPlace( ).getSize( )[0] - 1;
	    if ( nextY < 0 ) nextY = 0; if ( nextY >= getPlace( ).getSize( )[1] ) nextY = getPlace( ).getSize( )[1] - 1;
	    
	    // Go to the next place
	    go( nextX, nextY );
	}
	return null;
    }

    private void go( int x, int y ) {
	migrate( x, y );
	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") will move to [" + x + ", " + y + 
				 "] with velocity(" + velocityX + ", " + velocityY + ")" );
    }

    public Object onArrival( Object arg ) {
	// Receive the globably best-known coordinates and value
	BestXYV bestData = ( BestXYV )arg;
	bestGX = bestData.x;
	bestGY = bestData.y;
	bestGV = bestData.v;

	// Update my best-known coordinates and value
	if ( ( ( Rastrigin )getPlace( ) ).value < bestPV ) {
	    bestPX = getPlace( ).getIndex( )[0];
	    bestPY = getPlace( ).getIndex( )[1];
	    bestPV = ( ( Rastrigin )getPlace( ) ).value;
	}
	
	// Return either bestP or bestG depending on their value
	if ( bestPV < bestGV ) {
	    bestData.x = bestPX;
	    bestData.y = bestPY;
	    bestData.v = bestPV;
	}
	return bestData;
    }
}
