package edu.uw.bothell.css.dsl.mass.apps.SwarmOptimization;
import edu.uw.bothell.css.dsl.MASS.*;
import java.util.*;

public class Rastrigin extends Place {
    // function identifiers
    public static final int init_ = 0;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case init_: return init( argument );
	}
	return null;
    }

    /**
     * Is the default constructor.
     */
    public Rastrigin( ) {
	super( );
    }

    public Rastrigin( Object arg ) {
    }

    // Rastrigin constatns
    private static final double lower = -5.12;
    private static final double upper = 5.12;
    private static final int A = 10;

    // This place's [x,y] coordinates and value in the Rastrigin space
    public double x = 0.0;
    public double y = 0.0;
    public double value = 0.0;

    public Object init( Object arg ) {

	// Compute my [x,y] coordinates in [-lower, -lower] to [upper, upper]
	x = getIndex( )[0];
	y = getIndex( )[1];
	x = x / getSize( )[0] * ( upper - lower ) + lower;
	y = y / getSize( )[1] * ( upper - lower ) + lower;

	// Computer my value with the Rastrigin function
	// f(x) = An + sigma(i = 1, n)(x_i^2 - A cos(2 pi x_i) ); where n = 2 dimension
	value = 2 * A
	    + ( x * x - A * Math.cos( 2 * Math.PI * x ) )
	    + ( y * y - A * Math.cos( 2 * Math.PI * y ) );

	MASS.getLogger( ).debug( "place[" + getIndex( )[0] + ", " + getIndex( )[1] + 
				 "] = " + "(" + x + ", " + y + ") = " + value );
	
	return null;
    }
}
