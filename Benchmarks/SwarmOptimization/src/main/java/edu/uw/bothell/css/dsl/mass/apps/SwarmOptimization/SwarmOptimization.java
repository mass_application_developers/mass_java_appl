package edu.uw.bothell.css.dsl.mass.apps.SwarmOptimization;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

/**
 * SwarmOptimization.java - Simulates a particle swarm optimization
 * @author  Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since   November 2, 2017
 */
public class SwarmOptimization {
    /**
     * Simulates a particle swarm optimizaiton over a Rastrigin space
     * @param args TBD
     */
    public static void main( String[] args ) {

	int statAgents = 0;

	// Read and validate input parameters
	if ( args.length != 6 ) {
            System.err.println( "args = " + args.length + " should be 6:" +
				" size, nAgents, iteration, nProcs, nThrs show[y/n]" );
            System.exit( -1 );
        }
        int size = Integer.parseInt( args[0] );
	int nAgents = Integer.parseInt( args[1] );
	int iteration = Integer.parseInt( args[2] );
        if ( size < 1 || nAgents > size * size || iteration <= 0 ) {
            System.err.println( "size(" + size + ") should be > 0" );
	    System.err.println( "nAgents(" + nAgents + ") should be < size(" + size + ")" );
	    System.err.println( "iteration(" + iteration + " should be > 0 " );
            System.exit( -1 );
        }

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[3] );
	int nThr = Integer.parseInt( args[4] );
	boolean show = args[5].equals( "y" );
	System.out.println( "size = " + size + " nAgents = " + nAgents +
			    " nProc = " + nProc + " nThr = " + nThr );
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( arguments, nProc, nThr );

	// Create a Rastrigin space
	Places rastrigin = new Places( 1, Rastrigin.class.getName( ), null, size, size );
	rastrigin.callAll( Rastrigin.init_ );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );
	
	// Instantiate an agent at each node
	Agents particles = new Agents( 2, Particle.class.getName( ), null, rastrigin, nAgents );
	int[][] initLocations = new int[nAgents][2];
	int agentId = 0;
	int interval = ( int )( Math.sqrt( size * size / nAgents ) );
	while ( agentId < nAgents ) {
	    for ( int i = 0; i < size; i += interval ) {
		for ( int j = 0; j < size; j += interval ) {
		    if ( agentId == nAgents )
			break;
		    initLocations[agentId][0] = i;
		    initLocations[agentId][1] = j;
		    agentId++;
		}
	    }
	}

	// Prepare the space to maintain the best x, y, and value
	BestXYV[] bestData = new BestXYV[nAgents];
	for ( int i = 0; i < nAgents; i++ )
	    bestData[i] = new BestXYV( 0, 0, Double.MAX_VALUE );
	
	for ( int i = 0; i < iteration; i++ ) {
	    Object currStep = ( Object )(new Integer( i ) );
	    if ( show )
		System.out.println( "*** iteration = " + i + "************" );

	    // STEP 1: have each agent migrated to a next position
	    if ( show )
		System.out.println( "... step 1: departure ................." );
	    if ( i == 0 )
		particles.callAll( Particle.departure_, initLocations );
	    else
		particles.callAll( Particle.departure_ );

	    // STEP 2: have each agent evaluate the best known G and P
	    if ( show )
		System.out.println( "... step 2: onArrival ................." );
	    Object[] retValues = ( Object[] )particles.callAll( Particle.onArrival_, bestData );
	    particles.manageAll( );

	    // STEP 3: find the best-known X, Y, V
	    if ( show )
		System.out.println( "... step 3: evaluate the best known...." );
	    BestXYV tempData = new BestXYV( 0, 0, Double.MAX_VALUE );
	    for ( Object o : retValues )
		if ( ( ( BestXYV )o ).v < tempData.v ) {
		    tempData.x = ( ( BestXYV )o ).x;
		    tempData.y = ( ( BestXYV )o ).y;
		    tempData.v = ( ( BestXYV )o ).v;
		}
	    if ( show )
		System.out.println( "best know value so far [" + tempData.x + ", " + tempData.y + "] = " + tempData.v );
	    for ( int j = 0; j < nAgents; j++ ) {
		bestData[j].x = tempData.x;
		bestData[j].y = tempData.y;
		bestData[j].v = tempData.v;
	    }
	}
	
	// Get results
	System.out.println( "Final best know value so far [" + bestData[0].x + ", " + bestData[0].y + "] = " + bestData[0].v );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	MASS.finish( );
    }
}
