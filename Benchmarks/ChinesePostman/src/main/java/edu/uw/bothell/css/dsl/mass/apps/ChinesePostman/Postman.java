package edu.uw.bothell.css.dsl.mass.apps.ChinesePostman;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Postman extends Agent implements Serializable {
    // function identifiers
    public static final int onArrival_ = 0;
    public static final int departure_ = 1;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case onArrival_: return onArrival( argument );
	case departure_: return departure( argument );
	}
	return null;
    }

    /**
     * Is the default constructor.
     */
    public Postman( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg an integer array that carries nodes this agent or
     *            parent has travelled so far.
     */
    public Postman( Object arg ) {
	itinerary = ( ( Args2Agents )arg ).itinerary;
	footprints = ( ( Args2Agents )arg ).footprints;
	distance = ( ( Args2Agents )arg ).distance;
	total = ( ( Args2Agents )arg ).total;

	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born." );
    }

    /**
     * @param  arg an Integer object that indicates the current step
     * @return     nothing
     */
    public Object onArrival( Object arg ) {
	int currStep = ( ( Integer )arg ).intValue( );

	// Retrieve the current node's information
	int[] neighbors = ( ( Node )getPlace( ) ).neighbors;
	int currNode = getPlace( ).getIndex( )[0];

	if ( footprints[currNode] == null ) // my first visit to this node
	    footprints[currNode] = new int[neighbors.length];

	// Mark my footprint on the link between the previous and current node
	int prevNode = ( currStep > 0 ) ? itinerary[currStep - 1] : -1;
	if ( prevNode >= 0 ) {
	    for ( int i = 0; i < neighbors.length; i++ )
		if ( neighbors[i] == prevNode ) {
		    footprints[currNode][i]++;
		    break;
		}
	}

	// Spawn children and take my next direction
	spawnNmigrate( ( neighbors.length % 2 == 0 ), neighbors, footprints[currNode], currStep );

	return null;
    }

    /**
     * @param even true if the number of neighbors of the current node is even, otherwise false 
     * @param neighbors a list of neighboring places
     * @param footprint a list of neighboring places this agent has visisted so far
     * @param currStep the current step
     *
     * @return nothing
     */
    private void spawnNmigrate( boolean even, int[] neighbors, int[] footprint, int currStep ) {
	// Retrieve the current node's information
	int[] distances = ( ( Node )getPlace( ) ).distances;
	int currNode = getPlace( ).getIndex( )[0];
	
	// Count the number of edges available to visit
	int availableEdges = 0;
	for ( int i = 0; i < neighbors.length; i++ ) {

	    //if ( getAgentId( ) == 0 )
	    //	MASS.getLogger( ).debug( "Step " + currStep + ": agent(0)'s footprint to " + neighbors[i] + " = " + footprint[i] );
	    
	    if ( even && footprint[i] == 0 || !even && footprint[i] < 2 )
		availableEdges++;
	}

	if ( availableEdges == 0 ) {
	    // No more edges to explore
	    if ( currNode == itinerary[0] ) { // Came back to the original node

		// Need to check if I visited all places
		int[] results = itinerary.clone( );
		Arrays.sort( results );
		
		boolean seamlessVisits = true;
		int nPlaces = 0;
		for ( int i = 0; i < results.length; i++ ) {
		    if ( results[i] == -1 ) // skip all -1s
			continue;
		    else if ( i == 0 ) { // results[0] has a place
			nPlaces++;
			continue;
		    }
		    else if ( results[i - 1] == results[i] ) // duplicated visits to the same place
			continue;
		    else if ( results[i - 1] + 1 == results[i] ) { // visit to a new place
			nPlaces++;
			continue;
		    }
		    else { // missing some places
			seamlessVisits = false;
			break;
		    }
		}
		if ( seamlessVisits && nPlaces == getPlace( ).getSize( )[0] ) {
		    // visited all nodes. Let's print out results
		    System.out.println("agent(" + getAgentId( ) + ") tripped along:" );
		    for ( int i = 0; i < itinerary.length; i++ ) {
			if ( itinerary[i] != -1 )
			    System.out.println( itinerary[i] );
		    }
		    System.out.println( "total = " + total );
		}
	    }
	    // Now I'm done.
	    MASS.getLogger( ).debug( "Step " + currStep + ": agent(" + getAgentId( ) + 
				     ") gets terminated at " + currNode );
	    kill( );
	}
	else {
	    // Some edges to explore

	    // Prepare arguments to be passed to children
	    Args2Agents args[] =  ( availableEdges > 1 ) ? new Args2Agents[availableEdges - 1] : null;
	    int argsCount = 0; // eventually reaches # children

	    // Scan all neighbors of the current place.
	    for ( int i = 0; i < neighbors.length; i++ ) {
		if ( even && footprint[i] == 0 || !even && footprint[i] < 2 ) {
		    // if # neighbors == even, we can explore only a neighbor with no foot print
		    // if # neighbors == odd, we can explore a neighbor with up to one foot print
		    
		    if ( --availableEdges == 0 ) {
			// Parent takes the last available edge
			itinerary[currStep + 1] = neighbors[i];
			footprint[i]++; // increment myFootprint[]
			distance = distances[i];
		    }
		    else {
			// Children take the first availableEdges - 1.
			int[] childItinerary = itinerary.clone( );
			childItinerary[currStep + 1] = neighbors[i];
			int[][] childFootprints = footprints.clone( );
			for ( int k = 0; k < childFootprints.length; k++ )
			    if ( footprints[k] != null )
				childFootprints[k] = footprints[k].clone( );
			childFootprints[currNode][i]++;
			args[argsCount++] = new Args2Agents( childItinerary, childFootprints, distances[i], total );
		    }
		}
	    }

	    // Finally, spawn all my children
	    if ( args != null )
		spawn( args.length, args ); 
	}
    }

    /**
     * Migrates to itinerary[currStep]
     * @param  arg an Integer object that indicates the current step
     * @return     nothing
     */
    public Object departure( Object arg ) {
	int currStep = ( ( Integer )arg ).intValue( );


	// Add the distance from the current to the next place except
	// the very first hop to the original place.
	total += distance;

	migrate( itinerary[currStep] );
	int prev = ( currStep > 0 ) ? itinerary[currStep - 1] : -1;
	MASS.getLogger( ).debug( "Step " + currStep + ": agent(" + getAgentId( ) +
				 ") will migrate from " +
				 prev + " to " + itinerary[currStep] );
	return null;
    }

    // private data members
    private int[] itinerary = null;
    private int[][] footprints = null;
    private int distance = 0;
    private int total = 0;
}
