package edu.uw.bothell.css.dsl.mass.apps.ChinesePostman;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;

/**
 * ChinesePostman.java - an agent-based Chinese postman problem
 * @author  Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since   October 17, 2017
 */
public class ChinesePostman {
    /**
     * Computes the shortest route to visit all nodes by dispatching postman 
     * agents from a start node, (i.e., a post office)
     * @param args TBD
     */
    public static void main( String[] args ) {

	int statSteps = 0;
	int statAgents = 0;

	// Read and validate input parameters
	if ( args.length != 4 ) {
            System.err.println( "args = " + args.length + " should be 4:" +
				" nNodes, source, nProcs, nThrs" );
            System.exit( -1 );
        }
        int nNodes = Integer.parseInt( args[0] );
        int src = Integer.parseInt( args[1] );
        if ( nNodes < 1 || src >= nNodes ) {
            System.err.println( "Nodes(" + nNodes + "), src(" + src
                                + ") should be "
                                + "> 0 and < nNodes.");
            System.exit( -1 );
        }

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[2] );
	int nThr = Integer.parseInt( args[3] );
	System.out.println( "nNodes = " + nNodes + " src = " + src +
			    " nProc = " + nProc + " nThr = " + nThr );
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( arguments, nProc, nThr );

	// Create a map
	Places network = new Places( 1, Node.class.getName( ), null, nNodes );
	network.callAll( Node.init_ );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );
	
	// Instantiate the very first gravity agent.
	int[] itinerary = new int[nNodes * 2];
	itinerary[0] = src;
	for ( int i = 1; i < nNodes * 2; i++ )
	    itinerary[i] = -1; // not visited yet
	int[][] footprint = new int[nNodes][];
	Args2Agents arg = new Args2Agents( itinerary, footprint, 0, 0 );
	
	Agents postmen = new Agents( 2, Postman.class.getName( ), ( Object )arg,
				     network, 1 );

	// For stats
	int steps = 0;     // # steps
	int maxAgents = 0; // max # agents in computation
	
	for ( steps = 0; postmen.nAgents() > 0; steps++ ) {
	    Object currStep = ( Object )(new Integer( steps ) );
	    System.out.println( "*** steps = " + steps + "************" );

	    // STEP 1: have each agent migrate along a different edge 
	    // emanating from the current node
	    // at i == 0, the very first agent hops to the origin
	    System.out.println( "... step 1: migration ................." );
	    postmen.callAll( Postman.departure_, currStep );
	    postmen.manageAll( );
	    
	    // STEP2: have each agent spawn its children and let them
	    // choose their next destination
	    System.out.println( "... step 2: spawn ....................." );
	    postmen.callAll( Postman.onArrival_, currStep );
	    postmen.manageAll( );

	    // For stats
	    int nAgents = postmen.nAgents( );
	    maxAgents = ( maxAgents >= nAgents ) ? maxAgents : nAgents;
	}

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	// Stats
	System.out.println( "Statistics: #steps = " + steps +
			    " #agents = " + maxAgents );
	
	MASS.finish( );
    }
}
