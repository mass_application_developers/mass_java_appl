#!/bin/bash
# $1 graph node size
# $2 src node id
# $3 dst node id
# $4 # computing nodes
# $5 # threads
echo "MASS *****"
cd target
java -Xms1g -Xmx8g -jar ShortestPath-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 $4 $5

echo "Sequentail *****"
cd classes
java -Xms1g -Xmx8g edu.uw.bothell.css.dsl.mass.apps.ShortestPath.Dijkstra $1 $2 $3
