package edu.uw.bothell.css.dsl.mass.apps.Triangles;
import edu.uw.bothell.css.dsl.MASS.Agents;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Places;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;

/**
 * CountTriangles.java - Counts and enumerates all triangles in a given graph
 * @author  Munehiro Fukuda <mfukuda@uw.edu>
 * @version 1.0
 * @since   October 23, 2017
 */
public class CountTriangles {
    /**
     * Counts and enumerates all triangles in a given graph
     * @param args TBD
     */
    public static void main( String[] args ) {

	int statAgents = 0;

	// Read and validate input parameters
	if ( args.length != 4 ) {
            System.err.println( "args = " + args.length + " should be 3:" +
				" nNodes, nProcs, nThrs show[y/n]" );
            System.exit( -1 );
        }
        int nNodes = Integer.parseInt( args[0] );
        if ( nNodes < 1 ) {
            System.err.println( "Nodes(" + nNodes + ") should be > 0" );
            System.exit( -1 );
        }

	// Set up arguments to MASS.init( );
	String[] arguments = new String[4];
	arguments[0] = "dslab";
	arguments[1] = "ignored";
	arguments[2] = "machinefile.txt";
	arguments[3] = "12345";
	int nProc = Integer.parseInt( args[1] );
	int nThr = Integer.parseInt( args[2] );
	boolean show = args[3].equals( "y" );
	System.out.println( "nNodes = " + nNodes +
			    " nProc = " + nProc + " nThr = " + nThr );
	MASS.setLoggingLevel( LogLevel.ERROR );
	MASS.init( );

	// Create a map
	Places network = new Places( 1, Node.class.getName( ), null, nNodes );
	network.callAll( Node.init_ );

	// Time measurement starts
	System.out.println( "Go!" );
	long startTime = System.currentTimeMillis( );

	// Instantiate an agent at each node
	Agents crawlers = new Agents( 2, Crawler.class.getName( ), null, network, nNodes );
	int[][] itineraries = new int[nNodes][4];
	for ( int i = 0; i < nNodes; i++ )
	    for ( int j = 0; j < 4; j++ )
		itineraries[i][j] = ( j == 0 ) ? i : -1;
	crawlers.callAll( Crawler.init_, ( Object[] )itineraries );

	// For stats
	int totalAgents = 0; // max # agents in computation

	for ( int i = 0; i < 3; i++ ) {
	    Object currStep = ( Object )(new Integer( i ) );
	    if ( show )
		System.out.println( "*** iteration = " + i + "************" );

	    // STEP 1: have each agent spawn its children and let them
	    // choose their next destination
	    if ( show )
		System.out.println( "... step 1: spawn ....................." );
	    crawlers.callAll( Crawler.onArrival_, currStep );
	    crawlers.manageAll( );

	    // STEP 2: have each agent migrate along a different edge
	    // emanating from the current node
	    // at i == 0, the very first agent hops to the origin
	    if ( show )
		System.out.println( "... step 2: migration ................." );
	    crawlers.callAll( Crawler.departure_, currStep );
	    crawlers.manageAll( );

	    // For stats
	    int nAgents = crawlers.nAgents( );
	    totalAgents += nAgents;
	}

	// Get results
	int nAgents = crawlers.nAgents( );
	System.out.println( "# triangles = " + nAgents );

	// Time measurement ends
	long elapsedTime = System.currentTimeMillis( ) - startTime;
	System.out.println( "Elapsed time = " + elapsedTime );

	// Stats
	System.out.println( "Statistics: total #agents = " + totalAgents );

	// Enumerate all triangles
	if ( show ) {
	    int[][] dummyArgs = new int[nAgents][1];
	    Object[] triangles = ( Object[] )crawlers.callAll( Crawler.getTriangle_, ( Object[] )dummyArgs );

	    for ( int i = 0; i < nAgents; i++ )
		System.out.println( i + ": " + ( String )triangles[i] );
	}

	MASS.finish( );
    }
}
