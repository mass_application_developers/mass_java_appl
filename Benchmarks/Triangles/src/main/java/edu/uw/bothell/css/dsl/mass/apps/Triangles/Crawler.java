package edu.uw.bothell.css.dsl.mass.apps.Triangles;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Crawler extends Agent implements Serializable {
    // private data members
    private int[] itinerary = null;
    
    // function identifiers
    public static final int init_        = 0;
    public static final int onArrival_   = 1;
    public static final int departure_   = 2;
    public static final int getTriangle_ = 3;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case init_:        return init( argument );
	case onArrival_:   return onArrival( argument );
	case departure_:   return departure( argument );
	case getTriangle_: return getTriangle( argument );
	}
	return null;
    }

    /**
     * Is the default constructor.
     */
    public Crawler( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg an integer array that carries nodes this agent or
     *            parent has travelled so far.
     */
    public Crawler( Object arg ) {
	itinerary = ( int[] )arg;

	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born." );
    }

    /** 
     * @param arg an integer array that includes only the first place to go.
     * @return    nothing
     */
    public Object init( Object arg ) {
	itinerary = ( int[] )arg;
	return null;
    }

    /**
     * @param  arg an Integer object that indicates the current step
     * @return     nothing
     */
    public Object onArrival( Object arg ) {
	int currStep = ( ( Integer )arg ).intValue( );

	// Retrieve the current node's information
	int[] neighbors = ( ( Node )getPlace( ) ).neighbors;
	int currNode = getPlace( ).getIndex( )[0];

	switch( currStep ) {
	case 0:
	case 1:
	    // Spawn children and take my next direction
	    spawnNmigrate( neighbors, currStep );
	    return null;
	case 2:
	    // Go back to the origianl node
	    goBackHome( neighbors, currStep );
	}

	return null;
    }

    /**
     * @param even true if the number of neighbors of the current node is even, otherwise false 
     * @param neighbors a list of neighboring places
     * @param footprint a list of neighboring places this agent has visisted so far
     * @param currStep the current step
     *
     * @return nothing
     */
    private void spawnNmigrate( int[] neighbors, int currStep ) {
	// Retrieve the current node's information
	int currNode = getPlace( ).getIndex( )[0];
	
	// Count the number of edges available to visit
	int availableEdges = 0;
	for ( int i = 0; i < neighbors.length; i++ ) {
	    if ( neighbors[i] < currNode )
		// Going to a neighbor with a lower id
		availableEdges++;
	}

	if ( availableEdges == 0 ) {
	    // No more edges to explore. I'm done
	    MASS.getLogger( ).debug( "Step " + currStep + ": agent(" + getAgentId( ) +
				     ") gets terminated at " + currNode );
	    kill( );
	}
	else {
	    // Some edges to explore

	    // Prepare arguments to be passed to children
	    int args[][] =  ( availableEdges > 1 ) ? new int[availableEdges - 1][] : null;
	    int argsCount = 0; // eventually reaches # children

	    // Scan all neighbors of the current place.
	    for ( int i = 0; i < neighbors.length; i++ ) {
		if ( neighbors[i] < currNode ) {
		    // Going to a neighbor with a lower id
		    if ( --availableEdges == 0 ) {
			// Parent takes the last available edge
			itinerary[currStep + 1] = neighbors[i];
		    }
		    else {
			// Children take the first availableEdges - 1.
			int[] childItinerary = itinerary.clone( );
			childItinerary[currStep + 1] = neighbors[i];
			args[argsCount++] = childItinerary;
		    }
		}
	    }

	    // Finally, spawn all my children
	    if ( args != null )
		spawn( args.length, args ); 
	}
    }

    /**
     * Goes back to the original node if available.
     * @param neighbors a list of neighboring places
     * @return          nothing
     */
    private void goBackHome( int[] neighbors, int currStep ) {
	// Check if the current node has my original node as a neighbor
	for ( int i = 0; i < neighbors.length; i++ ) {
	    if ( neighbors[i] == itinerary[0] ) { // YES
		itinerary[currStep + 1] = neighbors[i];
		break;
	    }
	}
	if ( itinerary[currStep + 1] == -1 ) { // NO
	    MASS.getLogger( ).debug( "Step " + currStep +
				     ": agent(" + getAgentId( ) + ") can't go home at " + 
				     itinerary[0] + " and thus gets terminated at " + 
				     getPlace( ).getIndex( )[0] );
	    kill( );
	}
    }

    /**
     * Migrates to itinerary[currStep]
     * @param  arg an Integer object that indicates the current step
     * @return     nothing
     */
    public Object departure( Object arg ) {
	int currStep = ( ( Integer )arg ).intValue( );

	migrate( itinerary[currStep + 1] );
	MASS.getLogger( ).debug( "Step " + currStep + ": agent(" + getAgentId( ) +
				 ") will migrate from " +
				 itinerary[currStep] + " to " + itinerary[currStep + 1] );
	return null;
    }

    /**
     * @param    arg assumes null.
     * @return       a string that includes three vertices forming a triagle
     */
    public Object getTriangle( Object arg ) {
	String triangle = "";
	triangle += "agent(" + Integer.toString( getAgentId( ) ) + "): ";
	for ( int i = 0; i < 4; i++ )
	    triangle += Integer.toString( itinerary[i] ) + " ";

	return triangle;
    }
}
