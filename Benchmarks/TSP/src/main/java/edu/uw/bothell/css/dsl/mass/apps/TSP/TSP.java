package edu.uw.bothell.css.dsl.mass.apps.TSP;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;
import java.util.*;
import java.io.*;

/**
 * TSP.java - a travelling salesman probelem by agents
 * @author  Munehiro Fukuda <mfukuda@uw.edu>                                                         
 * @version 1.0                                                                                      
 * @since   Mar. 6, 2018
 */
public class TSP {
    private static int nCities = 0; // # cities
    private static int nAnts = 0;   // # ants
    private final static double numAntFactor = 0.8; // nAnts = numAntFactor * nCities

    // Reasonable number of iterations
    // - results typically settle down by 500
    private final static int maxIterations = 2000; // 2000

    // The sequential ACO-based TSP converged in 5 runs of 2000 iterations
    private final static int maxSimRuns = 5; // 5

    private static double graph[][] = null; // a city adjacency matrix

    private static int[] bestTour;
    private static double bestTourLength;

    public static void main( String[] args ) {

        // Read and validate input parameters                                                        
        if ( args.length != 3 ) {
            System.err.println( "args = " + args.length + " should be 3:" +
                                " filename, nProcs, nThrs" );
            System.exit( -1 );
        }
        String filename = args[0]; // a file that maintains an adjacency matrix

        // Set up arguments to MASS.init( );                                                         
        String[] arguments = new String[4];
        arguments[0] = "dslab";
        arguments[1] = "W3l0v3MASS!";
        arguments[2] = "machinefile.txt";
        arguments[3] = "12345";
        int nProc = Integer.parseInt( args[1] );
        int nThr = Integer.parseInt( args[2] );
        System.out.println( "filename = " + filename + 
                            " nProc = " + nProc + " nThr = " + nThr );
        MASS.setLoggingLevel( LogLevel.DEBUG ); // ERROR or DEBUG

	// Read an adjacency matrix from filename, and initialize nCities and nAnts
	readGraph( filename );

	// start MASS
        //MASS.init( arguments, nProc, nThr );
	MASS.setNumThreads( nThr );
	MASS.init( );

	// Create a map
	System.out.println( "nCities = " + nCities );
	Places cities = new Places( 1, City.class.getName( ), null, nCities );
	cities.callAll( City.init_, ( Object[] )graph );

	// Populate ants
	System.out.println( "nAnts = " + nAnts );
	Integer arg = new Integer( nCities );
	Agents ants = new Agents( 2, Ant.class.getName( ), arg, cities, nAnts );

	for ( int i = 0; i < maxSimRuns; i++ )
	    solve( cities, ants );

	// finish MASS
	MASS.finish( );
    }

    // Read in graph from a file.                                                                    
    // Allocates all memory.                                                                         
    // Adds 1 to edge lengths to ensure no zero length edges.                                        
    private static void readGraph(String path) {
	try {
	    FileReader fr = new FileReader(path);
	    BufferedReader buf = new BufferedReader(fr);
	    String line;
	    int i = 0;
	    
	    while ((line = buf.readLine()) != null) {
		String splitA[] = line.split(" ");
		LinkedList<String> split = new LinkedList<String>();
		for (String s : splitA)
		    if (!s.isEmpty())
			split.add(s);
		
		if (graph == null)
		    graph = new double[split.size()][split.size()];
		int j = 0;
		
		for (String s : split)
		    if (!s.isEmpty())
			graph[i][j++] = Double.parseDouble(s) + 1; // add 1 to original distance
		i++;
	    }
	} catch ( IOException e ) {
	    e.printStackTrace( );
	    System.exit( -1 );
	}

	// for debugging
	/*
	for ( int i = 0; i < graph.length; i++ )
	    for ( int j = 0; j < graph[i].length; j++ )
		System.out.println( graph[i][j] );
	*/

	nCities = graph.length;
	nAnts = (int) (nCities * numAntFactor);
    }

    private static int[] solve( Places cities, Agents ants ) {
	cities.callAll( City.clear_ );
	for ( int iteration = 0; iteration < maxIterations; iteration++ ) {
	    //System.out.println( "iteration = " + iteration );
	    ants.callAll( Ant.setup_ );
	    ants.manageAll( );
	    for ( int city = 0; city < nCities; city++ ) {
		// System.out.println( "Ant.moveAnts_ = " + city );
		ants.callAll( Ant.moveAnts_ );
		ants.manageAll( );
	    }
	    cities.callAll( City.evapolate_ );
	    for ( int city = 0; city < nCities; city++ ) {
		// System.out.println( "Ant.updateTrails_ = " + city );
		ants.callAll( Ant.updateTrails_ );
		ants.manageAll( );
	    }
	    Object[] args = new Object[nAnts];
	    for ( int i = 0; i < args.length; i++ )
		args[i] = new Object( );
	    Object[] tourLengths = ( Object[] )ants.callAll( Ant.getTourLength_, args );
	    Object[] tours = ( Object[] )ants.callAll( Ant.getTour_, args );
	    
	    updateBest( tourLengths, tours );
	}

	// Subtract n because we added one to edges on load                                                        
	System.out.println( "Best tour length: " + ( bestTourLength - nCities ) );
        System.out.println( "Best tour:" + tourToString( bestTour ) );
        return bestTour.clone( );
    }

    private static void updateBest( Object[] tourLengths, Object[] tours ) {
	if (bestTour == null) {
            bestTour = ( int[] ) tours[0];             // ants[0]'s tour length
            bestTourLength = ( Double )tourLengths[0]; // ant[0]'s tour
        }
        for (int ant = 0; ant < tourLengths.length; ant++ ) {
	    //System.out.println( "ant[" + ant + "]'s tourlength = " + ( ( Double )tourLengths[ant] - nCities ) 
	    //		+ ", tour = " + tourToString( ( int[] )tours[ant] ) );
	    if ( ( Double )tourLengths[ant] < bestTourLength ) {
                bestTourLength = ( Double )tourLengths[ant];
                bestTour = ( ( int[] )tours[ant] ).clone();
            }
        }
	//System.out.println( "Best tour so far: length = " + ( bestTourLength - nCities ) 
	//		    + ", tour = " + tourToString( bestTour ) );
    }

    private static String tourToString( int tour[] ) {
        String t = new String( );
        for ( int i : tour )
            t = t + " " + i;
        return t;
    }
}

