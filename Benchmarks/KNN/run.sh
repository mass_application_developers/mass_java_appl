##!/bin/bash
#export lowerX=0.0
#export upperX=7000.0
#export lowerY=0.0
#export upperY=7000.0
#export placesize=700
#export datasize=490000
#export k=7

#export lowerX=0.0
#export upperX=4000.0
#export lowerY=0.0
#export upperY=4000.0
#export placesize=400
#export datasize=160000
#export k=7

#export lowerX=0.0
#export upperX=1000.0
#export lowerY=0.0
#export upperY=1000.0
#export placesize=100
#export datasize=100000
#export k=7

export lowerX=0.0
export upperX=10.0
export lowerY=0.0
export upperY=10.0
export placesize=10
export datasize=20
export k=3

export filename=~mfukuda/Desktop/MASS/mass_java_appl/KNN/target/data.txt
cd target
cd classes

echo "DataGen *****"
java -Xms1g -Xmx8g edu.uw.bothell.css.dsl.mass.apps.KNN.DataGen $lowerX $upperX $lowerY $upperY $datasize $k > $filename

echo "MASS *****"
cd ..
java -Xms1g -Xmx8g -jar KNN-1.0-SNAPSHOT-jar-with-dependencies.jar $lowerX $upperX $lowerY $upperY $placesize $datasize $k $filename $1 1

echo "KNNSingle ****"
cd classes
java -Xms1g -Xmx8g edu.uw.bothell.css.dsl.mass.apps.KNN.KNNSingle $datasize $k $filename
