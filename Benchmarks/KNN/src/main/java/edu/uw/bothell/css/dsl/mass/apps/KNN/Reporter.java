package edu.uw.bothell.css.dsl.mass.apps.KNN;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Reporter extends Agent implements Serializable {
    // function identifiers
    public static final int spawnChild_ = 1;
    public static final int killAdults_ = 2;
    public static final int reportData_ = 3;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case spawnChild_:   return spawnChild( argument );
	case killAdults_:   return killAdults( argument );
	case reportData_:   return reportData( argument );
	}
	return null;
    }

    public static final int isParent = 1;
    public static final int isSenior = 2;
    public static final int isChild  = 3;
    private int status = 0;
    
    /**
     * Is the default constructor.
     */
    public Reporter( ) {
	super( );
    }

    /**
     * Is the actual constructor.
     * @param arg should be null.
     */
    public Reporter( Object arg ) {
	status = ( ( Integer )arg ).intValue( );
	
	MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") was born as " + 
				 ( ( status == isParent ) ? "a parent" : "a child" ) );
    }
    
    public Object spawnChild( Object arg ) {
	Space space = ( Space )getPlace( );
	if ( status == isParent && space.source != null && space.mydata.size( ) > 0 ) {
	    // Since I have potential k nearest neighbors, spawn a child reporter
	    MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") creates a child at [" +
				     getPlace( ).getIndex( )[0] + ", " + 
				     getPlace( ).getIndex( )[1] + "]... mydata.size = " +
				     space.mydata.size( ) );
	    Object[] param = new Object[1];
	    param[0] = new Integer( isChild );
	    spawn( 1, param );
	    status = isSenior;
	}
	return null;
    }
    
    public Object killAdults( Object arg ) {
	if ( status != isChild )
	    kill( );
	return null;
    }

    public Object reportData( Object arg ) {
	if ( status == isChild ) {
	    // report my potential training data back to the main( )
	    Space space = ( Space )getPlace( );

	    MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") mydata.size = " + 
				     space.mydata.size( ) + ", k = " + space.k );

	    ArrayList<double[]> mydata = new ArrayList<double[]>( );
	    int max = Math.min( space.k, space.mydata.size( ) );
	    for ( int i = 0; i < max; i++ ) {
		mydata.add( space.mydata.get( i ) );
		MASS.getLogger( ).debug( "agent(" + getAgentId( ) + ") retrieved [" + 
					 space.mydata.get( i )[0] + ", " + 
					 space.mydata.get( i )[1] + "]" );
	    }
	    return mydata;
	}
	return null;
    }
}
