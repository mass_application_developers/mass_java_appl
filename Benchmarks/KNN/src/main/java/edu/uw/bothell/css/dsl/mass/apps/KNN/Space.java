package edu.uw.bothell.css.dsl.mass.apps.KNN;
import edu.uw.bothell.css.dsl.MASS.*;
import java.io.*;
import java.util.*;

public class Space extends Place {
    // function identifiers
    public static final int init_ = 0;
    public static final int query_ = 1;
    public static final int detect_ = 2;
    public static final int propagate_ = 3;

    public Object callMethod( int functionId, Object argument ) {
	switch( functionId ) {
	case init_:      return init( argument );
	case query_:     return query( argument );
	case detect_:    return detect( argument );
	case propagate_: return propagate( argument );
	}
	return null;
    }

    // File to read. This is shared among all places on the same computing node.
    public static String filename = null;
    public static int datasize = 0;
    public static FileInputStream file = null;
    public static double[] dataset = null;

    // Each space maintains a subspace defined with lower/upper and
    // contatins a subset of data located in this subspace.
    public  ArrayList<double[]> mydata = new ArrayList<double[]>( ); // a list of data
    private double[] bound = null;          // the global data bound: 0:xmin/1:xmax/2:ymin/3:ymax
    private double[] delta = new double[2]; // delta in x and y
    private double[] lower = new double[2]; // the lower bound
    private double[] upper = new double[2]; // the upper bound
    private int propagationCounter = 0;     // count # repatitions
    public  Source source = null;           // the ripple source info.
    public  int k = 0;                      // k-nearest neighbors

    /**
     * Is the default constructor.
     */
    public Space( ) {
	super( );
    }

    /** 
     * Is the constructor that initialize each subspace with its lower and
     * upper bound in a given data space and read a given dataset in a shared
     * buffer named "dataset" if not yet read.
     */
    public Space( Object arg ) {
	bound = ( ( Args2Places )arg ).bound;
	filename = ( ( Args2Places )arg ).filename;
	datasize = ( ( Args2Places )arg ).datasize;
	this.k = ( ( Args2Places )arg ).k;
    }

    /**
     * Create a list of data in my subspace
     */
    private Object init( Object arg ) {
	// Calculate the interval, lower bound, and upper bound on X
	delta[0] = ( bound[1] - bound[0] ) / getSize( )[0];
	lower[0] = bound[0] + delta[0] * getIndex( )[0]; 
	upper[0] = bound[0] + delta[0] * ( getIndex( )[0] + 1 ) - Double.MIN_VALUE;

	// Calculate the interval, lower bound, and upper bound on Y
	delta[1] = ( bound[3] - bound[2] ) / getSize( )[1];
	lower[1] = bound[2] + delta[1] * getIndex( )[1];
	upper[1] = bound[2] + delta[1] * ( getIndex( )[1] + 1 ) - Double.MIN_VALUE;

	// Read a given data set as a whole
	synchronized( filename ) {
	    if ( file == null ) {
		// open the file
		try {
		    file = new FileInputStream( filename );
		} catch( Exception e ) { e.printStackTrace( ); };
		Scanner scanner = new Scanner( file );

		// read the data ( x, y, attribute )
		dataset = new double[datasize * 3];
		for ( int i = 0; i < dataset.length; i++ ) {
		    if ( !scanner.hasNextDouble( ) )
			break;
		    dataset[i] = scanner.nextDouble( );
		}
	    }
	}

	MASS.getLogger( ).debug( "space[" + getIndex( )[0] + ", " + getIndex( )[1] + "] = " +
				 "(" + lower[0] + ", " + lower[1] + ") ~ " +
				 "(" + upper[0] + ", " + upper[1] + ")" );

	for ( int i = 0; i < dataset.length; i += 3 ) {
	    
	    // check if a given datum is in my range
	    if ( dataset[i] >= lower[0] && dataset[i] <= upper[0] &&
		 dataset[i + 1] >= lower[1] && dataset[i + 1] <= upper[1] ) {

		// keept it in my data list
		double[] datum = new double[4];
		datum[0] = dataset[i];       // x
		datum[1] = dataset[i + 1];   // y
		datum[2] = dataset[i + 2];   // attribute
		datum[3] = Double.MAX_VALUE; // distance
		mydata.add( datum );
	    }
	}

	// Prepare an array of Object in my inMessages
	setInMessages( new Object[1] );
	getInMessages( )[0] = null;


	MASS.getLogger( ).debug( "space[" + getIndex( )[0] + ", " + 
				 getIndex( )[1] + "] includes " );
	for ( int i = 0; i < mydata.size( ); i++ ) {
	    double[] data = mydata.get( i );
	    MASS.getLogger( ).debug( data[0] + " " + data[1] + " " + data[2] );
	}
	return null;
    }

    /**
     *
     */
    private Object query( Object arg ) {
	double[] queryData = ( double[] )arg;
	if ( lower[0] <= queryData[0] && queryData[0] <+ upper[0] &&
	     lower[1] <= queryData[1] && queryData[1] <+ upper[1] ) {
	    // This is my data
	    getInMessages( )[0] = new Source( getIndex( ), queryData, k );
	    MASS.getLogger( ).debug( "space[" + getIndex( )[0] + ", " + 
				     getIndex( )[1] + "] includes centroid" );
	    MASS.getLogger( ).debug( queryData[0] + " " + queryData[1] );
	}
	return null;
    }

    /**
     * Detects a ripple from the source data
     */
    private Object detect( Object arg ) {
	// check if I received a propagated ripple
	if ( getInMessages( )[0] != null && source == null ) {
	    MASS.getLogger( ).debug( "time = " + propagationCounter + ": space[" + 
				     getIndex( )[0] + ", " + getIndex( )[1] + 
				     "] got a ripple of " + getInMessages( )[0] +
				     ", k = " + k );
	    
	    // retrieve the ripple source
	    source = ( Source )getInMessages( )[0];

	    // compute the distance from the source to each of my data
	    for ( int i = 0; i < mydata.size( ); i++ ) {
		double[] data = mydata.get( i );
		data[3] = computeDistance( data[0], data[1], source.coord[0], source.coord[1] );
	    }
	    // Sort my data
	    Collections.sort( mydata, new Comparator<double[]>( ) {
		    @Override
		    public int compare( double[] data1, double[] data2 ) {
			if ( data1[3] < data2[3] ) return -1;
			if ( data1[3] == data2[3] ) return 0;
			return 1;
		    }
		} );
	}
	return null;
    }

    /**
     * Propagates a ripple
     */
    private Object propagate( Object arg ) {
	propagationCounter++;

	if ( source != null ) {
	    // Propagate the ripple to my neighbors
	    // Fist, consider the von-Neuman neighborhood.
	    // N
	    int[] neighborN = {0, -1}; putInMessage( 1, neighborN, 0, source );
	    // E
	    int[] neighborE = {1, 0}; putInMessage( 1, neighborE, 0, source );
	    // S
	    int[] neighborS = {0, 1}; putInMessage( 1, neighborS, 0, source );
	    // W
	    int[] neighborW = {-1, 0}; putInMessage( 1, neighborW, 0, source );

	    // Thereafter, check if the Moore neighborhood is needed, too.
	    if ( propagationCounter % 2 == 0 ) {
		// NE
		int[] neighborNE = {1, -1}; putInMessage( 1, neighborNE, 0, source );
		// SE
		int[] neighborSE = {1, 1}; putInMessage( 1, neighborSE, 0, source );
		// SW
		int[] neighborSW = {-1, 1}; putInMessage( 1, neighborSW, 0, source );
		// NW
		int[] neighborNW = {-1, -1}; putInMessage( 1, neighborNW, 0, source );
	    }
	}
	return null;
    }

    /**
     * Computes the distnace between a given training data (d) and a query data (s)
     */
    private double computeDistance( double dx, double dy, double sx, double sy ) {
	return Math.sqrt( ( dx - sx ) * (dx - sx ) + ( dy - sy ) * ( dy - sy ) );
    }
}
