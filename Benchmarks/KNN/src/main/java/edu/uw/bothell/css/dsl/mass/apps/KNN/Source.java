package edu.uw.bothell.css.dsl.mass.apps.KNN;
import java.io.*;

public class Source implements Serializable {
    int[] index = null;
    double[] coord = null;
    int k = 0;

    public Source( ) {
	this.index = new int[2];
	this.coord = new double[2];
	this.k = 1;
    }

    public Source( int[] index, double[] coord, int k ) {
	this.index = index;
	this.coord = coord;
	this.k = k;
    }
}
