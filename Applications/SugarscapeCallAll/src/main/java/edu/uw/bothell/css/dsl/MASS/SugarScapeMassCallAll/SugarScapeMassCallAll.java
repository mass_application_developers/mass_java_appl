package edu.uw.bothell.css.dsl.MASS.SugarScapeMassCallAll;

import java.util.*;
import java.awt.*;
import java.awt.event.*;

// Library for Multi-Agent Spatial Simulation
import edu.uw.bothell.css.dsl.MASS.Agents;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Places;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;

public class SugarScapeMassCallAll {
	
    public static void main( String[] args ) throws Exception {
		/*
		// Verify user provide enough input data
		if( args. length < 4 ) {
			System.err.println( "\nUsage:\n\tjava SugarScape size nAgents maxTime interval");
			System.exit( -1 );
		}

		// Set variables with the user input data
		int size = Integer.parseInt( args[0] );
		int nAgents = Integer.parseInt( args[1] );
        int maxTime = Integer.parseInt( args[2] );
        int interval = Integer.parseInt( args[3] );
        boolean showGfx = args.length == 5;
		*/
		int vDist = 1;

		int size = 200;
		int nAgents = 4;
		int maxTime = 1000;
		int interval = 5;

		MASS.setLoggingLevel( LogLevel.DEBUG );
	  
		MASS.init( 1 );

        // Create a Land array.
        Places land = new Places( 1, Land.class.getName(), null, size, size );				//<< exchangeAll()
		land.callAll( Land.init_ );
        //Places land = new Places( 1, "Land", null, vDist, false, size, size );	//<< exchangeBoundary()

        // Populate Agents (unit) on the Land array
        Agents unit = new Agents( 2, Unit.class.getName(), null, land, nAgents );

        // Define the neighbors of each cell   
        Vector<int[]> neighbors = new Vector<int[]>( );
		for( int x = 0 - vDist; x <= vDist; x++ ) {
       	    for( int y = 0 - vDist; y <= vDist; y++ ) {
           		if( !(x == 0 && y == 0) ) neighbors.add( new int[]{ x, y } );
       	    }
		}
		land.setAllPlacesNeighbors( neighbors );
        
		//if ( interval > 0 && showGfx ) startGraphics( size );
		
		Object[] agentsCallAllObjects = new Object[size*size];

		long startNano = System.nanoTime();

        // Start simulatin time
        for ( int time = 0; time < maxTime; time++ ) {
			// Exchange #agents with neighbors
            land.exchangeAll( 1, Land.exchange_ );			// << exchangeAll()

            land.callAll( Land.update_ );

            // Move agents to a neighbor with the least population
            Object[] callAllResults = (Object[]) unit.callAll( Unit.decideNewPosition_, agentsCallAllObjects );
            unit.manageAll( );

			// Collect and print (if selected) the local data
			/*
            if ( time % interval == 0 ) {
                Object[] locData =  (Object[]) land.callAll( Land.collectLocData_, null );
				if ( showGfx ) writeToGraphics( locData, size );
            }
            */
        }

        long endNano = System.nanoTime();

        System.out.println("----------------------------");
        System.out.println("Total execution time: " + (endNano - startNano) + " nanoseconds");
        System.out.println("----------------------------");

        //System.out.println("Type anything to finish MASS after reviewing graphics");
		//Scanner reader = new Scanner(System.in);
		//String s = reader.next();
        
		// End graphics
		//if( showGfx ) finishGraphics();

        // finish MASS
        MASS.finish( );
    }
	
	// Graphics ---------------------------------------------------------------------------------
  	private static final int defaultN = 100; 	// the default system size
  	private static final int defaultCellWidth = 8;
  	private static Color 	 bgColor;            	//white background
  	private static Frame 	 gWin;               	// a graphics window
  	private static int 	 cellWidth;            	// each cell's width in the window
  	private static Insets 	 theInsets;         	// the insets of the window 
  	private static Color 	 agentColor[];         	// agent color
  	private static Color 	 sugarColor[];         	// sugar color
  	private static int 	 N = 0;                	// array size
	
	// Start a graphics window ------------------------------------------------------------------
	public static void startGraphics( int arraySize ) {
		// define the array size
		N = arraySize;

		// Graphics must be handled by a single thread
		bgColor = new Color( 255, 255, 255 );	//white background

		// Calculate the cell width in a window
		cellWidth = (int)((double) defaultCellWidth / ((double) N / (double) defaultN ));
		if ( cellWidth == 0 ) cellWidth = 1;

		// Initialize window and graphics:
		gWin = new Frame( "SugarScape Simulation" );
		gWin.setLocation( 50, 50 );  		// screen coordinates of top left corner

		gWin.setResizable( false );
		gWin.setVisible( true );     
		theInsets = gWin.getInsets();
		Dimension frameDim = new Dimension (N * cellWidth + theInsets.left + theInsets.right,
											N * cellWidth + theInsets.top + theInsets.bottom);
		gWin.setSize(frameDim);

		// Wait for frame to get initialized
		long resumeTime = System.currentTimeMillis() + 1000;
		do {} while (System.currentTimeMillis() < resumeTime);

		// Paint the background
		Graphics g = gWin.getGraphics( );
		g.setColor( bgColor );
		g.fillRect( theInsets.left, theInsets.top, N * cellWidth, N * cellWidth );

		// Agent Colors
		agentColor = new Color[10];
		agentColor[0] = new Color( 0x0066FF );
		agentColor[1] = new Color( 0x0033FF );
		agentColor[2] = new Color( 0x0000FF );   // blue
		agentColor[3] = new Color( 0x0000CC );
		agentColor[4] = new Color( 0x000099 );
		agentColor[5] = new Color( 0x330099 );
		agentColor[6] = new Color( 0x660099 );
		agentColor[7] = new Color( 0x660066 );
		agentColor[8] = new Color( 0x660033 );
		agentColor[9] = new Color( 0x660000 );

		// Sugar Colors
		sugarColor = new Color[10];
		sugarColor[0] = new Color( 0xFFFFFF ); 	// white
		sugarColor[1] = new Color( 0x00FF99 );	// Lt Green
		sugarColor[2] = new Color( 0x00FF66 );
		sugarColor[3] = new Color( 0x00FF33 );
		sugarColor[4] = new Color( 0x00CC33 );
		sugarColor[5] = new Color( 0x00FF00 );
		sugarColor[6] = new Color( 0x00CC00 );
		sugarColor[7] = new Color( 0x009900 );
		sugarColor[8] = new Color( 0x006600 );
		sugarColor[9] = new Color( 0x003300 );
  	}
	
	// Update graphics window with new cell information -----------------------------------------
  	public static void writeToGraphics( Object[] locData, int arraySize ) {
        Graphics g = gWin.getGraphics( );

		int maxAgentColor = agentColor.length;
		int maxSugarColor = sugarColor.length;

		for ( int i = 0; i < arraySize; i++  ) {
		   for ( int j = 0; j < arraySize; j++ ) {

				Color cellColor = null;
				int numAgents = ((int[])( (Object)locData[ i * arraySize + j ] ))[0];
				int numSugar  = ((int[])( (Object)locData[ i * arraySize + j ] ))[1];

				// **********************************************************************************
				if( numAgents > 0 ) { 		// Has agents
					cellColor = ( numAgents >= maxAgentColor ) ? agentColor[ maxAgentColor - 1 ]  : agentColor[ numAgents ];
				} else if( numSugar > 0 ) {	// Has sugar
					cellColor = ( numSugar >= maxSugarColor ) ? sugarColor[ maxSugarColor - 1 ] : sugarColor[ numSugar ];
				} else {
					cellColor = bgColor;
				}

				// show a cell
				g.setColor( cellColor );
				g.fill3DRect( theInsets.left + i * cellWidth, theInsets.top  + j * cellWidth, cellWidth, cellWidth, true ); //mod by John
			}
		}
  	}
	
	// Finish graphics window -------------------------------------------------------------------
  	public static void finishGraphics() {
        Graphics g = gWin.getGraphics( );

		if( g != null ) {
        	g.dispose( );
        	gWin.removeNotify( );
        	gWin = null;
		}
  	}
}
