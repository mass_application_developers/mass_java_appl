/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.motif;

import edu.uw.bothell.css.dsl.MASS.MASS;

public class Driver {

	public static void main(String args[]) {
        // verify arguments
        if (args.length < 3) {
            System.out.println(
                "usage: Driver " +
                "threads_per_node data_file motif_size [--show-results]");
            System.exit(-1);
        }

        // read in the user's password
       /* String password = "";
        try {
            BufferedReader passReader = new BufferedReader(
                new FileReader(args[1]));
            password = passReader.readLine().trim();
            passReader.close();
        } catch (Exception e) {
            System.out.println("Unable to read user password.");
            e.printStackTrace();
            System.exit(-1);
        }*/

        // configure arguments for MASS
        //String[] massArgs = new String[5];
       // massArgs[0] = args[0];           // username
        //massArgs[1] = password;          // password
       // massArgs[2] = "nodes.xml"; // machine file
       // massArgs[3] = args[2];           // port UNUSED
       // massArgs[4] = args[3];           // additions to classpath

       // int nProcesses = Integer.parseInt(args[0]);
        int nThreads = Integer.parseInt(args[0]);

        boolean showResults = false;
        if (args.length == 4 && args[3].equals("--show-results")) {
            showResults = true;
        }

        MASS.setNodeFilePath("nodes.xml");
        MASS.setCommunicationPort(50951);
        MASS.setNumThreads(nThreads);

        // start MASS
        MASS.init();

        long start = System.currentTimeMillis();

        // run the program
        Main app = new Main(args[1], Integer.parseInt(args[2]), showResults);
        app.run();

        System.out.println(
            (System.currentTimeMillis() - start) + " milliseconds to " +
            "complete the program");

        // finish MASS
        MASS.finish();

    }
}
