/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.motif;

// GraphNode.java
//
// by Matt Kipps
// 12/12/14

import java.util.HashMap;
import java.util.Map;

import edu.uw.bothell.css.dsl.MASS.*;


public class GraphNode extends Place {

    public static final int collectSubgraphs_ = 0;
    public static final int initializeEdges_ = 1;
    public Object callMethod(int functionID, Object args) {
        switch(functionID) {
            case initializeEdges_:
                AdjacencyList edges = (AdjacencyList)args;
                initializeEdges(edges);
                break;
            case collectSubgraphs_:
                return (Object)collectSubgraphs();
            default:
                break;
        }
        return null;
    }

    private int networkSize;
    private AdjacencyList adjacencyList;
    private Map<String, Integer> subgraphs;

    public GraphNode(Object rawArgs) {
        super();

        Constructor constructor = (Constructor)rawArgs;
        this.networkSize   = constructor.getNetworkSize();
        this.subgraphs     = new HashMap<String, Integer>();
        this.adjacencyList = null;
    }

    public int getNetworkSize() {
        return networkSize;
    }

    public Map<String, Integer> collectSubgraphs() {
        return subgraphs;
    }

    // set all the network edges for this node
    public void initializeEdges(AdjacencyList adjacencyList) {
        this.adjacencyList = adjacencyList;
    }

    public void addToSubgraphs(Subgraph subgraph) {
        String repr = subgraph.getByteString();
        int count = 1;
        synchronized(subgraphs) {
            if (subgraphs.containsKey(repr)) {
                count += subgraphs.get(repr);
            }
            subgraphs.put(repr, count);
        }
    }

    public AdjacencyList getAdjacencyList() {
        return adjacencyList;
    }

    public int getFirstIndex() {
        return getIndex()[0];
    }

    // the nested Constructor class simplifies the instantiation of GraphNode
    // objects through MASS library calls
    @SuppressWarnings("serial")
	public static class Constructor implements java.io.Serializable {
        private int networkSize;

        public Constructor(int networkSize) {
            this.networkSize = networkSize;
        }

        public int getNetworkSize() {
            return networkSize;
        }
    }
}
