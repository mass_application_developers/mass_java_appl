package edu.uw.bothell.css.dsl.MASS.RandomWalk_cleanCallAll;

import java.util.Vector;   // for Vector
//import java.awt.*;         // uses the abstract windowing toolkit
//import java.awt.event.*;   // also uses key events so we need this

// Library for Multi-Agent Spatial Simulation
import edu.uw.bothell.css.dsl.MASS.Place;

// Land Array
public class Land extends Place {
	// function identifiers
	public static final int exchange_ = 0;
	public static final int update_ = 1;
	public static final int init_ = 2;
	public static final int collectAgents_ = 3;

	// the array size and my index in (x, y) coordinates
	private int sizeX, sizeY;
	private int myX, myY;
	
	// wave height from four neighbors: north, east, south, and west
	private final int north = 0, east = 1, south = 2, west = 3;
    int[][] neighbors = new int[2][2]; // my four neighbors' #agents
    
    // constructor
    public Land( Object object ) { 
		Vector<int[]> placeNeighbors = new Vector<int[]>();
		placeNeighbors.add( new int[] { 0, -1 } );
		placeNeighbors.add( new int[] { 1, 0 } );
		placeNeighbors.add( new int[] { 0, 1 } );
		placeNeighbors.add( new int[] { -1, 0 } );
		setNeighbors( placeNeighbors );
	}  
    
	// --------------------------------------------------------------------------
	/** 
	* Return the local wave height to the cell[0,0]
	* @param args formally declared but actually not used.
	*/
	public double collectAgents( Object args ) {
		//if(agents.size() > 0 ) System.err.println(agents.size());     
		return ( getAgents().size() ); 
	}
  
	/**
     * Is called from callAll( ) or exchangeAll( ), and forwards this call
     * to update( ) or exchange( ).
     * @param funcId the function Id to call
     * @param args argumenets passed to this funcId.
     */
    public Object callMethod( int funcId, Object args ) {
        switch ( funcId ) {
            case exchange_: return exchange( args );
            case update_: return update( args );
            case init_ : return init(args); 
            case collectAgents_ : return ( Object )collectAgents(args);
        }
        return null;
    }

    /**
     * Is called from exchangeAll( ) to exchange #agents with my 
     * four neighbors
     * @param args formally requested but actuall not used.
     */
    public Object exchange( Object args ) {
        return new Integer( getAgents().size( ) );
    }

    /**
     * Is called from callAll( ) to update my four neighbors' #agents.
     * @param args formally requested but actuall not used.
     */
    public Object update( Object args ) {
        int index = 0;
        for ( int x = 0; x < 2; x++ )
            for ( int y = 0; y < 2; y++ )
                neighbors[x][y] = ( getInMessages()[index] == null ) ? Integer.MAX_VALUE : ( Integer )getInMessages()[index];
        return null;
    }

	/** 
	* Since size[] and index[] are not yet set by
	* the system when the constructor is called, this init( ) method must
	* be called "after" rather than "during" the constructor call
	* @param args formally declared but actually not used
	*/
	public Object init( Object args ) {
		sizeX = getSize()[0]; sizeY = getSize()[1]; // size  is the base data members
		myX = getIndex()[0];  myY = getIndex()[1];  // index is the base data members
			
		return null;
	}
	
	public Number getDebugData() {
		if ( getNumAgents() > 0 )
			return new Double(1);
		else 
			return new Double(0);
	}
}