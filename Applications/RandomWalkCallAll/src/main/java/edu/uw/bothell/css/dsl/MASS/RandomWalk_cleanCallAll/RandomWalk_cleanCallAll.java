package edu.uw.bothell.css.dsl.MASS.RandomWalk_cleanCallAll;

// uses the abstract windowing toolkit
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;

// Library for Multi-Agent Spatial Simulation
import edu.uw.bothell.css.dsl.MASS.Agents;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Places;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;

public class RandomWalk_cleanCallAll {
	
    /**
     * Starts a RandomWalk application with the MASS library
     * @param receives the Land array size, the number of initial agents, and
     *                 the maximum simaution time.
     */
    public static void main( String[] args ) throws Exception {
		/*
		if ( args.length < 4 ) {
			System.err.println( "usage: " + 
					"java RanodomWalk size nAgents maxTime interval" );
			System.exit( -1 );
		}

		int size = Integer.parseInt( args[0] );
		int nAgents = Integer.parseInt( args[1] );
		int maxTime = Integer.parseInt( args[2] );
		int interval = Integer.parseInt( args[3] );
		//boolean showGfx = args.length == 5;
		*/

		int size = 200;
		int nAgents = 4;
		int maxTime = 1000;
		int interval = 5;

		MASS.setLoggingLevel( LogLevel.ERROR );
		  
		MASS.init( 1 );

		// create a Land array.
		Places land = new Places( 1, Land.class.getName(), null, size, size );
		land.callAll( Land.init_ );

		// populate Nomda agents on the land.
		Agents nomad = new Agents( 2, Nomad.class.getName(), null, land, nAgents );

		// start graphics
		//if ( interval > 0 && showGfx ) startGraphics( size );

		Object[] agentsCallAllObjects = new Object[size*size];
			
		long startNano = System.nanoTime();
			
		// now go into a cyclic simulation
		for ( int time = 0; time < maxTime; time++ ) {
			// exchange #agents with four neighbors
			land.exchangeAll( 1, Land.exchange_ );
			land.callAll( Land.update_ );

			// move agents to a neighbor with the least population
			Object[] callAllResults = (Object[]) nomad.callAll( Nomad.decideNewPosition_, agentsCallAllObjects );
			nomad.manageAll( );
			
			/*
			if ( time % interval == 0 ) {
			    Object[] agents = land.callAll( Land.collectAgents_, (Object[])null );
				if ( showGfx ) writeToGraphics( agents, size );

				System.out.println("Type to go");
				Scanner reader = new Scanner(System.in);
				String s = reader.next();
			}            
			*/
			 //if ( !showGfx ) MASS.debugUpdate();
		}

		long endNano = System.nanoTime();

        System.out.println("----------------------------");
        System.out.println("Total execution time: " + (endNano - startNano) + " nanoseconds");
        System.out.println("----------------------------");

		/*
		System.out.println("Type anything to finish MASS after reviewing graphics");
		Scanner reader = new Scanner(System.in);
		String s = reader.next();
		*/
		//if ( interval > 0 && showGfx ) finishGraphics();
		
		// finish MASS
		MASS.finish( );
    }

	// Graphics -------------------------------------------------------------------------------
	private static final int defaultN = 100; // the default system size
	private static final int defaultCellWidth = 8;
	private static Color bgColor;            //white background
	private static Frame gWin;               // a graphics window
	private static int cellWidth;            // each cell's width in the window
	private static Insets theInsets;         // the insets of the window 
	private static Color wvColor[];          // wave color
	private static int N = 0;                // array size
	
	// start a graphics window ----------------------------------------------------------------
	public static void startGraphics( int arraySize ) {
		// define the array size
		N = arraySize;

		// Graphics must be handled by a single thread
		bgColor = new Color( 255, 255, 255 );//white background

		// the cell width in a window
		cellWidth = (int)((double) defaultCellWidth / ((double) N / (double) defaultN )); //mod by John
		if ( cellWidth == 0 )
		  cellWidth = 1;

		// initialize window and graphics:
		gWin = new Frame( "Random Walk Simulation" );
		gWin.setLocation( 50, 50 );  // screen coordinates of top left corner

		gWin.setResizable( false );
		gWin.setVisible( true );     // show it!
		theInsets = gWin.getInsets();
		Dimension frameDim = new Dimension (N * cellWidth + theInsets.left + theInsets.right,
											N * cellWidth + theInsets.top + theInsets.bottom);
		gWin.setSize(frameDim);

		// wait for frame to get initialized
		long resumeTime = System.currentTimeMillis() + 1000;
		do {} while (System.currentTimeMillis() < resumeTime);

		// paint the back ground
		Graphics g = gWin.getGraphics( );
		g.setColor( bgColor );
		g.fillRect( theInsets.left,
				   theInsets.top,
				   N * cellWidth,
				   N * cellWidth );

		// prepare cell colors
		wvColor = new Color[21];
		wvColor[0] = new Color( 0x0000FF );   // blue
		wvColor[1] = new Color( 0x0033FF );
		wvColor[2] = new Color( 0x0066FF );
		wvColor[3] = new Color( 0x0099FF );
		wvColor[4] = new Color( 0x00CCFF );
		wvColor[5] = new Color( 0x00FFFF );
		wvColor[6] = new Color( 0x00FFCC );
		wvColor[7] = new Color( 0x00FF99 );
		wvColor[8] = new Color( 0x00FF66 );
		wvColor[9] = new Color( 0x00FF33 );
		wvColor[10] = new Color( 0x00FF00 );  // green
		wvColor[11] = new Color( 0x33FF00 );
		wvColor[12] = new Color( 0x66FF00 );
		wvColor[13] = new Color( 0x99FF00 );
		wvColor[14] = new Color( 0xCCFF00 );
		wvColor[15] = new Color( 0xFFFF00 );
		wvColor[16] = new Color( 0xFFCC00 );
		wvColor[17] = new Color( 0xFF9900 );
		wvColor[18] = new Color( 0xFF6600 );
		wvColor[19] = new Color( 0xFF3300 );
		wvColor[20] = new Color( 0xFF0000 );  // red
	}
  
	// update a graphics window with new cell information -------------------------------------
	public static void writeToGraphics( Object[] agents, int arraySize ) {
		Graphics g = gWin.getGraphics( );
		for ( int i = 0; i < arraySize; i++  ) {
			for ( int j = 0; j < arraySize; j++ ) {
				// convert a wave height to a color index ( 0 through to 20 ) 
				int cellValue = (int)((Double)agents[i * arraySize + j ]).intValue();
				Color cellColor = null; 

				if(cellValue > 0) {
					int index =  (int)((Double)agents[i * arraySize + j ]).intValue();// + 10;
					//if((Double)agents[i * arraySize + j ] > 0.0) System.err.println(" index is " + ((Double)agents[i * arraySize + j ]).intValue());
					index = ( index > 20 ) ? 20 : ( ( index < 0 ) ? 0 : index );
					cellColor = wvColor[index];
				} else {
					cellColor = bgColor;
				}

				// show a cell
				g.setColor( cellColor );
				g.fill3DRect( theInsets.left + i * cellWidth,
					 theInsets.top  + j * cellWidth,
					 cellWidth, cellWidth, true ); //mod by John
			}
		}
	}
  
	// finish the graphics window -------------------------------------------------------------
	public static void finishGraphics() {
		Graphics g = gWin.getGraphics( );
		g.dispose( );
		gWin.removeNotify( );
		gWin = null;
	}
}
