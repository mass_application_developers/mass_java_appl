#!/bin/sh

FILE="build/resources/main/points.txt"
XMIN=50
XMAX=90
YMIN=45
YMAX=80
NUM_POINTS=100


java -cp build/classes/java/main:../dependencies/mass-core-1.3.0-SNAPSHOT.jar edu.uw.bothell.rangesearch.mass.RangeSearchMASS $FILE $XMIN $XMAX $YMIN $YMAX $NUM_POINTS