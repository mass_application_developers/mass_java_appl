package edu.uw.bothell.rangesearch.mass;
import edu.uw.bothell.css.dsl.MASS.VertexPlace;

/**
 * KDTreeNode.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 *
 * Represents a single node in the K Dimensional tree
 */
public class KDTreeNode extends VertexPlace {

    public Point2D point;

    public KDTreeNode(Object args) {
        super();
        this.point = (Point2D)args;
    }


    public Object getPoint() {
        return point;
    }


    @Override
    public int hashCode() {
        return (Integer.toString(point.getX()) + ","
                + Integer.toString(point.getY())).hashCode();
    }

}
