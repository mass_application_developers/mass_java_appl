package edu.uw.bothell.rangesearch.mass;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * InputProcessing.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 *
 * InputProcessing class reads input from a file to create a list of Point2D points, which are used to build a KDTree.
 * The input file format is one "x,y" value on each line.
 *
 * @author Satine Paronyan
 */

public class InputProcessing {

    private List<Point2D> pointsList;

    InputProcessing() {
        this.pointsList = new ArrayList<>();
    }


    public Point2D[] getPointsList() {
        Point2D[] result = new Point2D[pointsList.size()];
        return pointsList.toArray(result);
    }


    public void readInput(String fileName) {
        try{
            Scanner scanner = new Scanner(new File(fileName));

            /*
             * Reads points file line by line, where each line contains X and Y coordinate values for one point.
             * Constructs Point with read x,y values and stores in the list.
             */
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] lineValues = line.split(",");

                Point2D point = new Point2D(Integer.parseInt(lineValues[0]), Integer.parseInt(lineValues[1]));
                pointsList.add(point);
            }

        }
        catch(FileNotFoundException EE) {
            System.err.println("The file " + fileName + " does not exist");
        }

    }

}
