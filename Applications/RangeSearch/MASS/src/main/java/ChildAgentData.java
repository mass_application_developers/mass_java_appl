package edu.uw.bothell.rangesearch.mass;

import java.io.Serializable;

/**
 * ChildAgentData.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 *
 * ChildAgentData is an object argument that is passed to a child agent.
 *
 * @author Satine Paronyan
 */
public class ChildAgentData implements Serializable {

    private int[] searchRange;
    private int[] destination; // to migrate
    private boolean isChild;


    public ChildAgentData() {
        super();
    }

    public ChildAgentData(int[] searchRange, int[] destination, boolean isChild) {
        this.searchRange = searchRange;
        this.destination = destination;
        this.isChild = isChild;
    }

    public int[] getSearchRange() {return  searchRange; }

    public int[] destination(){ return destination; }

    public boolean isChild() { return isChild; }
}
