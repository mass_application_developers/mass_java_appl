package edu.uw.bothell.rangesearch.mass;

import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.*;
import edu.uw.bothell.css.dsl.MASS.logging.LogLevel;

import java.util.*;

/**
 * RangeSearchMASS.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */
public class RangeSearchMASS {

    public static GraphPlaces graphPlaces;
    private static int vertexId = 1;
    private static int totalNumPoints;


    // args: filename Xmin Xmax Ymin Ymax totalNumPoints
    public static void main(String[] args) {
        MASS.setLoggingLevel(LogLevel.DEBUG);

        if (args.length < 5) {
            MASS.getLogger().error("----- Wrong number of arguments passed to main: expected 5");
        }

        int Xmin = Integer.parseInt(args[1]);
        int Xmax = Integer.parseInt(args[2]);
        int Ymin = Integer.parseInt(args[3]);
        int Ymax = Integer.parseInt(args[4]);
        Integer[] searchRange = {Xmin, Xmax, Ymin, Ymax};
        totalNumPoints = Integer.parseInt(args[5]);


        MASS.init();
        long startTime = System.nanoTime(); // start timer

        // ======= READ POINTS AND BUILD KD TREE (GRAPH) =========
        String file = args[0];
        InputProcessing input = new InputProcessing();
        input.readInput(file);
        // points read from file
        Point2D[] points = input.getPointsList();
        // sort points by x values
        Arrays.sort(points, X_COMPARATOR);

        int newSize = removeDuplicates(points, points.length);
        if (newSize < points.length) {
            points = Arrays.copyOfRange(points, 0, newSize);
            Arrays.sort(points, X_COMPARATOR);
        }

        RangeSearchMASS kdTreeMASS = new RangeSearchMASS(newSize);
        int rootID = kdTreeMASS.buildKDGraph(points);
        boolean flag = graphPlaces != null;;


        // =================== DO RANGE SEARCH ====================
        //Agents agents = new Agents(2, RangeSearchAgent.class.getName(), null, graphPlaces, 1);
        Agents agents = new Agents(2, "RangeSearchAgent", null, graphPlaces, 1);

        // vertex destinations where agents are initialized. We start with one agent at root node
        int destination = 0; // root vertex id
        agents.callAll(RangeSearchAgent.INIT, destination);
        agents.manageAll();
        agents.callAll(RangeSearchAgent.GET_SEARCHRANGE, searchRange);
        agents.manageAll();

        int nAgents = agents.nAgents();
        MASS.getLogger().debug("***** Number of initial Agents: " + nAgents);
        Object result = agents.callAll(RangeSearchAgent.DO_SEARCH, searchRange);
        agents.manageAll();

        long endTime = System.nanoTime();  // stop timer
        long totalTime = endTime - startTime;
        MASS.getLogger().debug("***** Elapsed time: " + totalTime / 1000000000 + " seconds");

        Point2D[] res = (Point2D[])result;

        // Output found points
        MASS.getLogger().debug("====== TOTAL INPUT NUMBER OF POINTS: " + totalNumPoints);
        MASS.getLogger().debug("======= Points found in requested range: " + res.length);
        MASS.getLogger().debug("======= Found points in range:\n");

        for (Point2D p: res) {
            MASS.getLogger().debug(p.getX() + "," + p.getY());
        }

        MASS.finish();
    }


    public static final Comparator<Point2D> X_COMPARATOR = (p1, p2) -> {
        int result = Integer.compare(p1.getX(), p2.getX());
        if (result == 0) {
            return Integer.compare(p1.getY(), p2.getY());
        }
        return result;
    };

    public static final Comparator<Point2D> Y_COMPARATOR = (p1, p2) -> {
        int result = Integer.compare(p1.getY(), p2.getY());
        if (result == 0) {
            return Integer.compare(p1.getX(), p2.getX());
        }
        return result;
    };


    public RangeSearchMASS(int size) {
        graphPlaces = new GraphPlaces(0, "edu.uw.bothell.rangesearch.mass.KDTreeNode", size);
    }

    // Receives list of points that are sorted by X-coordinate then build a KD tree
    public int buildKDGraph(Point2D[] points) {

        /* find median point in the sorted array. This point is the root of KD tree */
        int median = points.length / 2;
        //Point2D point2D = new Point2D(points[median].getX(), points[median].getY(), points[median].getDimension());
        Point2D point2D = new Point2D(points[median].getX(), points[median].getY(), Point2D.Dimension.X);
        int rootId = graphPlaces.addVertex(vertexId, point2D);  // add a new vertex to graph and initialize with point
        vertexId++;

        /* Build left subtree with the points that come before median point (root) */
        int leftChildId = buildKDGraphHelper(points, 0, median, point2D.getDimension(), -1);

        /* Build right subtree with the points that come after median point (root) */
        int rightChildId = buildKDGraphHelper(points, median + 1, points.length, point2D.getDimension(),-1);

        graphPlaces.addEdge(rootId, leftChildId, 0);
        graphPlaces.addEdge(rootId, rightChildId, 0);
        return rootId;
    }


    private  int buildKDGraphHelper(Point2D[] points, int start, int end, Point2D.Dimension dimension, int parentID) {
        if (start >= end) { return parentID; }

        Point2D.Dimension dim;
        Comparator<Point2D> comparator;

        // if parent node has x-dimension, then the comparison values for insertion in the BST is
        // x-coordinate values of parent and its children points.
        if (dimension == Point2D.Dimension.X) {
            dim = Point2D.Dimension.Y;
            comparator = Y_COMPARATOR;
        } else {
            // if parents node has y-dimension, then comparison is done
            // by its point and children points y-coordinate values.
            dim = Point2D.Dimension.X;
            comparator = X_COMPARATOR;
        }

        // sort points by corresponding comparator, either by x or y values
        Arrays.sort(points, start, end, comparator);

        int median = (start + end) / 2;
        Point2D pointParent = new Point2D(points[median].getX(), points[median].getY(), dim);
        int newParentID = graphPlaces.addVertex(vertexId, pointParent);  //add new vertex and initialize with point
        vertexId++;

        int lefChildId = buildKDGraphHelper(points, start, median, dim, newParentID);
        int rightChildId = buildKDGraphHelper(points, median + 1, end, dim, newParentID);

        graphPlaces.addEdge(newParentID, lefChildId, 0);
        graphPlaces.addEdge(newParentID, rightChildId, 0);

        return newParentID;
    }


    /* Function to remove duplicate elements in the given list of Points2D.
     * @returns new size of modified array
     */
    private static int removeDuplicates(Point2D[] points, int size) {
        if (size == 0) { return size; }

        //Assuming all elements in the input array are unique
        int numUniqueElements = points.length;

        for (int i = 0; i < numUniqueElements; i++) {
            for (int j = i + 1; j < numUniqueElements; j++) {
                //if any two elements are found equal
                if(points[i].equals(points[j])) {
                    //replace duplicate element with last unique element
                    points[j] = points[numUniqueElements - 1];

                    numUniqueElements--;
                    j--;
                }
            }
        }
        return numUniqueElements;
    }

}
