package edu.uw.bothell.rangesearch.mass;

import edu.uw.bothell.css.dsl.MASS.Agent;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.MASSBase;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * RangeSearchAgent.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */
public class RangeSearchAgent extends Agent implements Serializable {

    public static final int INIT = 0;
    public static final int GET_SEARCHRANGE = 1;
    public static final int DO_SEARCH = 2;
    public static final int GET_POINTS = 3;

    // points that are in requested range found by this agent
    private static ArrayList<Point2D> pointsInRange = new ArrayList<>();
    private static int[] searchRange;
    private static int[] migrateLocation = new int[2];
    private boolean isChild = false;


    /**
     * Default constructor.
     */
    public RangeSearchAgent() { super(); }


    /**
     * Constructs RangeSearchAgent object.
     * @param args passed to child agent, null for parent
     */
    public RangeSearchAgent(Object args) {
        this();
        if (args != null) {
            ChildAgentData childData = (ChildAgentData) args;
            searchRange = childData.getSearchRange();
            migrateLocation = childData.destination();
            isChild = childData.isChild();
        }
        MASS.getLogger().debug("***** agent(" + getAgentId() + ") was created.");
    }

    public Object setSearchRange(Object args) {
        searchRange = (int[])args;
        return null;
    }

    /**
     * This method is invoked by callAll() in main program to invoke a specified
     * function on each of the agents.
     * @param function - function to be invoked on agent
     * @param args - arguments for the function, if required
     * @return values returned by called function
     */
    public Object callMethod(int function, Object[] args) {

        switch (function) {
            case INIT:
                return initHop(args);
            case GET_SEARCHRANGE:
                return setSearchRange(args);
            case DO_SEARCH:
                return atVertex(args);
            case GET_POINTS:
                getPoints();
            default:
                return "Unknown function call to callMethod.";
        }
    }


    /**
     * Initial destination where first agent starts.
     * @param args int array with vertex number where agent to be initialized.
     */
    public boolean initHop(Object args) {
        migrate(MASSBase.getGlobalIndexForKey((int)args));
        return true;
    }


    /*
     * @param args int array containing the search range for points
     * in the following order: [Xmin, Xmax, Ymin, Ymax]
     * @return
     */
    public Object[] atVertex(Object[] args) {

        Integer[] temp = Arrays.copyOf(args, args.length, Integer[].class);
        searchRange = new int[temp.length];
        for (int i = 0; i < searchRange.length; i++) {
            searchRange[i] = (int)temp[i];
        }

        if (!getPlace().getVisited()) {

            Point2D pointsAtCurVertex = (Point2D)((KDTreeNode)getPlace()).getPoint();
            // if points is in range add it to my points list
            if ( (pointsAtCurVertex.getX() >= searchRange[0] && pointsAtCurVertex.getX() <= searchRange[1]) &&
                    (pointsAtCurVertex.getY() >= searchRange[2] && pointsAtCurVertex.getY() <= searchRange[3]) ) {
                pointsInRange.add(pointsAtCurVertex);
            }

            // get neighbors (actual retrieved value is neighbor id's)
            Object[] neighbors = (Object[])((KDTreeNode) getPlace()).getNeighbors();
            migrateLocation = new int[neighbors.length];
            int idx = 0;
            for(Object o: neighbors) {
                migrateLocation[idx] = (int)o;
                idx++;
            }

            if (neighbors.length == 0) { // no more vertexes to visit by this agent
                return (Object[])( pointsInRange.toArray(new Point2D[0]));
            } else {
                migrateSpawn(neighbors);
            }

        }
        return null;
    }

    private void migrateSpawn(Object[] children) {
        // vertex can have up to two children nodes, since the graph is a BST
        if (children.length > 1) {
            spawnChild(children);
            MASS.getLogger().debug("***** Agent #" + getAgentId() + " spawned a child to " + children[1]);
        }
        migrateTo(children);
    }


    /*
     * Spawns a child agent and passes arguments to child.
     * @param - destination where new child agent should migrate
     */
    private Object spawnChild(Object args) {
        // spawn(1, (new Object[]{children[1]})); // spawn a child on a right node

        int[] destination = (int[])args;
        ChildAgentData childData = new ChildAgentData(searchRange, destination,true );
        Object[] arg = new Object[]{childData};
        spawn(1, arg);
        return null;
    }


    private Object migrateTo(Object children) {
        if (isChild) {
            isChild = false;
            migrate(MASSBase.getGlobalIndexForKey(migrateLocation[1])); // migrate to right neighbor vertex
        } else {
            migrate(MASSBase.getGlobalIndexForKey(migrateLocation[0])); // migrate to right neighbor vertex
        }
        return null; // should change return value
    }


    //might be needed in future
    private Object getPoints() {
        StringBuilder result = new StringBuilder();
        result.append("Agent #");
        result.append(getAgentId());
        result.append(":\n");

        for (Point2D point: pointsInRange) {
            result.append(point.toString());
            result.append("\n");
        }

        return result.toString();
    }


}
