package edu.uw.bothell.rangesearch.spark;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * RangeSearch.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */
public class RangeSearch {

    public static Point2D[] kdTree(Point2D[] points) {
        Point2D[] kdTree = new Point2D[(points.length) * 2 + 1];
        buildKDTree(kdTree, points, 0, points.length - 1, 0, Point2D.Dimension.Y);
        return kdTree;
    }

    /*
     * Build KDTree in an array.
     * @param kdTree - array in which KDTree is built
     * @param points - points that used as a values in KDTree
     * @param start - start index in points[]
     * @param end - end index in points[]
     * @param parentIdx - parents index in kdTree[]
     * @param dimension - parent's dimension
     */
    private static void buildKDTree(Point2D[] kdTree, Point2D[] points, int start, int end,
                                    int parentIdx, Point2D.Dimension dimension) {

        if (start >= end) { return; }

        Point2D.Dimension dim;
        Comparator<Point2D> comparator;

        // if parent node has x-dimension, then the comparison values for insertion in the BST is
        // x-coordinate values of parent and its children points.
        if (dimension == Point2D.Dimension.X) {
            dim = Point2D.Dimension.Y;
            comparator = Point2D.Y_COMPARATOR;
        } else {
            // if parents node has y-dimension, then comparison is done
            // by its point and children points y-coordinate values.
            dim = Point2D.Dimension.X;
            comparator = Point2D.X_COMPARATOR;
        }

        // sort points by corresponding comparator, either by x or y values
        Arrays.sort(points, start, end, comparator);

        /* find median point in the sorted array. This point is the root of KD tree */
        int median = (start + end) / 2;
        points[median].setDimension(dim);
        kdTree[parentIdx] = points[median];

        // left child
        buildKDTree(kdTree, points, start, median,(parentIdx * 2) + 1, dim);
        //right child
        buildKDTree(kdTree, points, median+1, end, (parentIdx * 2) + 2, dim);
    }


    public static Point2D[] search(Point2D[] kdTree, int xMin, int xMax, int yMin, int yMax) {
        if (kdTree == null || kdTree.length == 0) { return new Point2D[0]; }
        List<Point2D> res = rangeSearch(kdTree,  xMin, xMax, yMin, yMax);

        return res.toArray(new Point2D[0]);
    }

    /*
     * Retrieves all the points present in the given query rectangle.
     * @param xMin bottom-left x-coordinate of query rectangle's point
     * @param xMax bottom-right x coordinate of query rectangle's point
     * @param yMin bottom-left y-coordinate of query rectangle's point
     * @param yMax top-left y-coordinate of query rectangle's point
     * @return list of points present in the query rectangle
     */
    private static List<Point2D> rangeSearch(Point2D[] kdTree, int xMin, int xMax, int yMin, int yMax) {
        List<Point2D> result = new ArrayList<>();
        if (kdTree == null || kdTree.length == 0) { return result; }
        rangeSearchHelper(result, kdTree, 0, xMin, xMax, yMin, yMax);

        return result;
    }


    /*
     * Find all points in a query axis-aligned rectangle.
     * Check if point in node lies in given rectangle.
     * Recursively search left/bottom (if any could fall in rectangle).
     * Recursively search right/top (if any could fall in rectangle).
     */
    private static void rangeSearchHelper(List<Point2D> result, Point2D[] kdTree, int parentIdx,
                                          int xMin, int xMax, int yMin, int yMax) {

        if (parentIdx >= kdTree.length || kdTree[parentIdx] == null) { return; }
        int parentXVal = kdTree[parentIdx].getX();
        int parentYVal = kdTree[parentIdx].getY();

        /* If parent point is in the given range add its point to result */
        if ((parentXVal >= xMin && parentXVal <= xMax) &&
                (parentYVal >= yMin && parentYVal <= yMax)) {
            result.add(kdTree[parentIdx]);
        }

        if (kdTree[parentIdx].getDimension() == Point2D.Dimension.X) {  // skip children with X < xMin and X > xMax
            if (parentXVal >= xMin) {
                // left child
                rangeSearchHelper(result, kdTree,parentIdx*2 + 1, xMin, xMax, yMin, yMax);
            }

            if (parentXVal <= xMax) {
                // right child
                rangeSearchHelper(result, kdTree, parentIdx*2 + 2, xMin, xMax, yMin, yMax);
            }
        }

        if (kdTree[parentIdx].getDimension() == Point2D.Dimension.Y) {  // skip children with Y < yMin and Y > yMax
            if (parentYVal >= yMin) {
                //left child
                rangeSearchHelper(result, kdTree, parentIdx*2 + 1, xMin, xMax, yMin, yMax);
            }

            if (parentYVal <= yMax) {
                //right child
                rangeSearchHelper(result, kdTree,parentIdx*2 + 2, xMin, xMax, yMin, yMax);
            }
        }
    }
}
