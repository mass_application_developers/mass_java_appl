import java.io.Serializable;

/**
 * Points2D.java
 * Project: RangeSearch
 * University of Washington Bothell, Distributed Systems Laboratory
 * Spring 2020
 * @author Satine Paronyan
 */

public class Point2D implements Serializable {
    private int x;
    private int y;
    private Dimension dim;

    enum Dimension {
       O, X, Y;
    }

    Point2D(int x, int y) {
        this.x = x;
        this.y = y;
        this.dim = Dimension.O;
    }

    Point2D(int x, int y, Dimension dim) {
        this.x = x;
        this.y = y;
        this.dim = dim;
    }

    public void setDimension(Dimension dim) {
        this.dim = dim;
    }

    public int getX(){
        return x;
    }

    public int getY() {
        return y;
    }

    public Dimension getDimension() { return dim; }

    // Overriding equals() to compare two MASS.Point2D objects
    @Override
    public boolean equals(Object o) {

        if (o == null) {
            return false;
        }
        // If the object is compared with itself then return true
        if (o == this) {
            return true;
        }

        // Check if o is an instance of Point or not
         // "null instanceof [type]" also returns false
        if (!(o instanceof Point2D)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Point2D c = (Point2D) o;

        // Compare the data members and return accordingly
        //return Integer.compare(x, c.x) == 0 && Integer.compare(y, c.y) == 0;
        return (x == ((Point2D) o).x && y == ((Point2D) o).y);
    }


    @Override
    public int hashCode() {
        int result = 373;
        result = 37 * result + x;
        result = 37 * result + y;
        return result;
    }


    @Override
    public String toString(){
        return "[" + x + "," + y + "," + dim + "]";
    }


}
