/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.RandomWalk;

import java.util.Random;
import java.util.Vector;

import edu.uw.bothell.css.dsl.MASS.Agent;

// Nomad Agents
@SuppressWarnings("serial")
public class Nomad extends Agent {
	
	// function identifiers
    public static final int decideNewPosition_ = 0;
	
	// define the four neighbors of each cell
    private Vector<int[]> neighbors = new Vector<int[]>( );
	
    public Nomad( ) {
        super(  );
    }

    public Nomad( Object object ) {
        super(  );

		int[] north = { 0, -1 }; neighbors.add( north );
		int[] east  = { 1,  0 }; neighbors.add( east );
		int[] south = { 0,  1 }; neighbors.add( south );
		int[] west  = { -1, 0 }; neighbors.add( west );  
    }
    
    /**
     * Instantiate an agent at each of the cells that form a square
     * in the middle of the matrix
     */
    public int map( int maxAgents, int[] size, int[] coordinates ) {         
        int sizeX = size[0], sizeY = size[1];
        int populationPerCell = (int)Math.ceil( maxAgents / ( sizeX * sizeY * 0.6 ) );
        int currX = coordinates[0], currY = coordinates[1];
        if ( sizeX * 0.4 < currX && currX < sizeX * 0.6 && sizeY * 0.4 < currY && currY < sizeY * 0.6 ) {
            //System.err.println("mapping max agents " + maxAgents + " size: " + size[0] + " population per cell: " + populationPerCell);
            return populationPerCell;
        } else
            return 0;
    }

    /**
     * Is called from callAll( ) and forwards this call to 
     * decideNewePosition( )
     * @param funcId the function Id to call
     * @param args argumenets passed to this funcId.
     */
    public Object callMethod( int funcId, Object args ) {
        switch ( funcId ) {
			case decideNewPosition_: return decideNewPosition( args );
        }
        return null;
    }
  
    /**
     * Computes the index of a next cell to migrate to.
     * @param args formally requested but actually not used
     */
    public Object decideNewPosition( Object args ) {
        int newX = 0;                 // a new destination's X-coordinate
        int newY = 0;                 // a new destination's Y-coordinate

        int currX = getPlace().getIndex()[0], currY = getPlace().getIndex()[1]; // curr index
        int sizeX = getPlace().getSize()[0], sizeY = getPlace().getSize()[1];   // land size
		
        Random generator = new Random();
        boolean candidatePicked = false;
        int next = 0;
        while(!candidatePicked)
        {
           next = generator.nextInt(4);
           int[] neighbor = neighbors.get(next); 
           newX = currX + neighbor[0];
           newY = currY + neighbor[1]; 
           if ( newY < 0 )
               continue; // no north
           if ( newX >= sizeX )
               continue; // no east
           if ( newY >= sizeY )
               continue; // no south
           if ( newX < 0 )
               continue; // no west 
            
           candidatePicked = true; 
        }
        // let's migrate
        migrate( newX, newY );
        //System.err.println("Migrating to X:" + newX + " Y:" +newY );

        return null;
    }

    @Override
    public Number getDebugData()
    {
        return new Double(1);
    }
}
