/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uwca;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import edu.uw.bothell.css.dsl.MASS.MASS;

/**
 *
 * @author jwoodrin
 * Our JobRunner grabs jobs from the JobManager and executes them using MASS
 */
@Singleton
public class JobRunner {
    
    // our reference to our singleton job manager class
    JobManager jobMgr;    
    
    int numProc = 1;
    int numThr = 8;
    
//    private Boolean mass_init = false;
    
    /**
     * Our default Constructor
     */
    public JobRunner(){
        jobMgr = JobManager.getInstance();        
    }
    
    /**
     * Starts up MASS on deployment with required libraries for clustering
     */
    @PostConstruct
    public void initMassLibrary(){    

    	// attempt to clean out the jobs directory on each startup      
//        File jobDir = new File(jobMgr.getJobsDirectory());
//        try {
//            FileUtils.cleanDirectory(jobDir);
//        } catch (IOException ex) {
//            Logger.getLogger(JobRunner.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        // copy the class files from the build directory to the mass working directory for nodes
//        File srcDir = new File("/net/metis/home/jman5000/mass_java_appl/UWCA/UWCA-ejb/target/classes");
//        File destDir = new File("/net/cssfs01p/opt/mfukuda-data/UWCA/uwca_mass_home");
//        try {            
//            FileUtils.copyDirectory(srcDir, destDir);
//        } catch (IOException ex) {
//            Logger.getLogger(JobRunner.class.getName()).log(Level.SEVERE, null, ex);
//        }       
        
        /**
         * Init MASS for the calculations to execute
         */
//        String netCdfLib = "netcdf.jar";
//        MASS.addLibrary(netCdfLib);
//        String apacheMathLib = "apachemath-3.3.3.jar";
//        MASS.addLibrary(apacheMathLib);
   
        MASS.setCommunicationPort(45454); // port # to use
        MASS.setNumThreads(numThr);            // # of threads to use
        MASS.init();
    }
    
    /**
     * Shut down mass on re-deploy
     */
    @PreDestroy
    public void finishMassLibrary(){
        // end mass
        MASS.finish();
    }
    
    /**
     *  Our scheduled job runner -- runs continuously
     */
    @Schedule(second="*/1", minute="*",hour="*", persistent=false)
    public void doWork(){
        try{
            // get the next job to be processed from the job manager
            Job j = jobMgr.getNextJob();
    
            if(j != null){
                // run the calculations
                j.executeJob();
                // update job status
                j.setStatus("Completed");  
            }
            Thread.sleep(1000);
        }catch(Exception e){
         //   MASS.getLogger().error("Error in job runner: ", e);
        }
    }    
}
