/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.climatemodels;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import ucar.ma2.Array;
import ucar.ma2.ArrayFloat;
import ucar.ma2.DataType;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;
import ucar.nc2.units.DateUnit;

/**
 *
 * @author jwoodrin
 */
public class Tasmax_1 implements ClimateModelInterface {

    /*
     float longitude(longitude=462);
     :_Netcdf4Dimid = 0; // int
     :standard_name = "longitude";
     :long_name = "Longitude";
     :units = "degrees_east";
     :axis = "X";
     :bounds = "longitude_bnds";

     float longitude_bnds(longitude=462, nb2=2);
     :_Netcdf4Dimid = 0; // int

     float latitude(latitude=222);
     :_Netcdf4Dimid = 2; // int
     :standard_name = "latitude";
     :long_name = "Latitude";
     :units = "degrees_north";
     :axis = "Y";
     :bounds = "latitude_bnds";

     float latitude_bnds(latitude=222, nb2=2);
     :_Netcdf4Dimid = 2; // int

     double time(time=20820);
     :standard_name = "time";
     :long_name = "Time axis";
     :units = "days since 1950-01-01 00:00:00";
     :calendar = "standard";
     :_Netcdf4Dimid = 3; // int

     float tasmax(time=20820, latitude=222, longitude=462);
     :missing_value = 1.0E20f; // float
     :units = "C";
     :_FillValue = 1.0E20f; // float
     */
  //  private String file1 = "/net/cssfs01p/opt/mfukuda-data/UWCA/data_models/model1/conus_c5.noresm1-m_hist_r1i1p1.daily.tasmax.1950-2005.nc";
    //   private String file1 = "/data/UWCA/data_models/model1/conus_c5.noresm1-m_hist_r1i1p1.daily.tasmax.1950-2005.nc";
    //   private String file1 = "C:\\UWCA\\model1\\conus_c5.noresm1-m_hist_r1i1p1.daily.tasmax.1950-2005.nc";
    private String file1;

    /*
     float longitude(longitude=462);
     :_Netcdf4Dimid = 0; // int
     :standard_name = "longitude";
     :long_name = "Longitude";
     :units = "degrees_east";
     :axis = "X";
     :bounds = "longitude_bnds";

     float longitude_bnds(longitude=462, nb2=2);
     :_Netcdf4Dimid = 0; // int

     float latitude(latitude=222);
     :_Netcdf4Dimid = 2; // int
     :standard_name = "latitude";
     :long_name = "Latitude";
     :units = "degrees_north";
     :axis = "Y";
     :bounds = "latitude_bnds";

     float latitude_bnds(latitude=222, nb2=2);
     :_Netcdf4Dimid = 2; // int

     double time(time=7670);
     :standard_name = "time";
     :long_name = "Time axis";
     :units = "days since 1950-01-01 00:00:00";
     :calendar = "standard";
     :_Netcdf4Dimid = 3; // int

     float tasmax(time=7670, latitude=222, longitude=462);
     :missing_value = 1.0E20f; // float
     :units = "C";
     :_FillValue = 1.0E20f; // float    
     */
 //   private String file2 = "/net/cssfs01p/opt/mfukuda-data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2006-2026.nc";
    //   private String file2 = "/data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2006-2026.nc";
    //   private String file2 = "C:\\UWCA\\model1\\conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2006-2026.nc";
    private String file2;
    /*
     float longitude(longitude=462);
     :_Netcdf4Dimid = 0; // int
     :standard_name = "longitude";
     :long_name = "Longitude";
     :units = "degrees_east";
     :axis = "X";
     :bounds = "longitude_bnds";

     float longitude_bnds(longitude=462, nb2=2);
     :_Netcdf4Dimid = 0; // int

     float latitude(latitude=222);
     :_Netcdf4Dimid = 2; // int
     :standard_name = "latitude";
     :long_name = "Latitude";
     :units = "degrees_north";
     :axis = "Y";
     :bounds = "latitude_bnds";

     float latitude_bnds(latitude=222, nb2=2);
     :_Netcdf4Dimid = 2; // int

     double time(time=8766);
     :standard_name = "time";
     :long_name = "Time axis";
     :units = "days since 1950-01-01 00:00:00";
     :calendar = "standard";
     :_Netcdf4Dimid = 3; // int

     float tasmax(time=8766, latitude=222, longitude=462);
     :missing_value = 1.0E20f; // float
     :units = "C";
     :_FillValue = 1.0E20f; // float
     */
 //   private String file3 = "/net/cssfs01p/opt/mfukuda-data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2027-2050.nc";
    //   private String file3 = "/data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2027-2050.nc";
    //   private String file3 = "C:\\UWCA\\model1\\conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2027-2050.nc";
    private String file3;
    /*
     float longitude(longitude=462);
     :_Netcdf4Dimid = 0; // int
     :standard_name = "longitude";
     :long_name = "Longitude";
     :units = "degrees_east";
     :axis = "X";
     :bounds = "longitude_bnds";

     float longitude_bnds(longitude=462, nb2=2);
     :_Netcdf4Dimid = 0; // int

     float latitude(latitude=222);
     :_Netcdf4Dimid = 2; // int
     :standard_name = "latitude";
     :long_name = "Latitude";
     :units = "degrees_north";
     :axis = "Y";
     :bounds = "latitude_bnds";

     float latitude_bnds(latitude=222, nb2=2);
     :_Netcdf4Dimid = 2; // int

     double time(time=8766);
     :standard_name = "time";
     :long_name = "Time axis";
     :units = "days since 1950-01-01 00:00:00";
     :calendar = "standard";
     :_Netcdf4Dimid = 3; // int

     float tasmax(time=8766, latitude=222, longitude=462);
     :missing_value = 1.0E20f; // float
     :units = "C";
     :_FillValue = 1.0E20f; // float
     */
   // private String file4 = "/net/cssfs01p/opt/mfukuda-data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2051-2074.nc";
//     private String file4 = "/data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2051-2074.nc";
    //   private String file4 = "C:\\UWCA\\model1\\conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2051-2074.nc";
    private String file4;

    /*
     float longitude(longitude=462);
     :_Netcdf4Dimid = 0; // int
     :standard_name = "longitude";
     :long_name = "Longitude";
     :units = "degrees_east";
     :axis = "X";
     :bounds = "longitude_bnds";

     float longitude_bnds(longitude=462, nb2=2);
     :_Netcdf4Dimid = 0; // int

     float latitude(latitude=222);
     :_Netcdf4Dimid = 2; // int
     :standard_name = "latitude";
     :long_name = "Latitude";
     :units = "degrees_north";
     :axis = "Y";
     :bounds = "latitude_bnds";

     float latitude_bnds(latitude=222, nb2=2);
     :_Netcdf4Dimid = 2; // int

     double time(time=9131);
     :standard_name = "time";
     :long_name = "Time axis";
     :units = "days since 1950-01-01 00:00:00";
     :calendar = "standard";
     :_Netcdf4Dimid = 3; // int

     float tasmax(time=9131, latitude=222, longitude=462);
     :missing_value = 1.0E20f; // float
     :units = "C";
     :_FillValue = 1.0E20f; // float

     */
    // total time dimension across files 55153
    // lat and long are the same for all files 
    // lat Y dim
    // long X dim
    // time Z?
    //   private String file5 = "/net/cssfs01p/opt/mfukuda-data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2075-2099.nc";
    //   private String file5 = "/data/UWCA/data_models/model1/conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2075-2099.nc";
    //  private String file5 = "C:\\UWCA\\model1\\conus_c5.noresm1-m_rcp45_r1i1p1.daily.tasmax.2075-2099.nc";
    private String file5;

    // our file list
    //  private String[] files = new String[]{file1, file2, file3, file4, file5};
    private final String[] files = new String[5];

    // Latitude = 222
    // Longitude = 462
    // Time, 3rd var
    private final int[][] dimensions = new int[][]{{462, 222, 20820}, {462, 222, 7670}, {462, 222, 8766}, {462, 222, 8766}, {462, 222, 9131}};

    private int[][] readIndexes;

    // tvalue for the climate model
    private final double tvalue = 1.986;

    String longitude = "longitude";
    String latitude = "latitude";
    String time = "time";
    String varname = "tasmax";
    private int startYear = 1950;
    private int endYear = 2099;
    private int numYears = 150;

    private static String path = "";
    private static String yearIndicesFileName = "year_indices.dat";

    /**
     * Default Constructor Reads in the file paths specific to the machine we're
     * working on
     */
    public Tasmax_1() throws IOException {
        path = "uwca_config" + File.separator + "climate_model_files" + File.separator;
        String file = "uwca_config" + File.separator + "climate_model_files" + File.separator + "Tasmax_1.txt";
        Scanner in = null;

        // read in the file paths from the file config directory
        FileReader fr = new FileReader(file);
        in = new Scanner(fr);
        files[0] = in.nextLine();
        files[1] = in.nextLine();
        files[2] = in.nextLine();
        files[3] = in.nextLine();
        files[4] = in.nextLine();
        in.close();
        fr.close();
    }

    /**
     * @return the files
     */
    public String[] getFiles() {
        return files;
    }

    /**
     * @return the dimensions
     */
    public int[][] getDimensions() {
        return dimensions;
    }

    /**
     * Reads the entire 365-6 days over the entire lat long coordinates for that
     * time frame.
     *
     * @param readIndex
     * @return
     */
    public Object readFullYear(int x, int y, int z, int[][] yearIndices) {
        readIndexes = yearIndices;

        NetcdfFile inputFile = null;    // target netCDF file   
        int readIndex;
        int yr;
        int readAmount;
        int[] orgin;
        int[] shape;
        int latitude;
        int longitude;
        int time;
        Variable ncdfTasMaxVar;
        Array dataSection;
        String varname = "tasmax";
        yr = z + getStartYear();

        readIndex = readIndexes[z][0];
        readAmount = readIndexes[z][1] - readIndexes[z][0];

        String fileToRead = "";

        if (yr > 2074) {
            fileToRead = files[4];
        } else if (yr > 2050) {
            fileToRead = files[3];
        } else if (yr > 2026) {
            fileToRead = files[2];
        } else if (yr > 2005) {
            fileToRead = files[1];
        } else {
            fileToRead = files[0];
        }

        try {
            inputFile = NetcdfFile.open(fileToRead);
        } catch (IOException ex) {
        }
        // get the netcdf variable
        ncdfTasMaxVar = inputFile.findVariable(varname);

        latitude = x;  // 222
        longitude = y; // 462

        orgin = new int[]{readIndex, 0, 0};
        shape = new int[]{readAmount, longitude, latitude};
        Object yearData = null;
        try {
            dataSection = ncdfTasMaxVar.read(orgin, shape);

            dataSection = dataSection.transpose(0, 2);
            yearData = dataSection.copyToNDJavaArray();

        } catch (Exception e) {
            String s = e.toString();
        }
        try {
            inputFile.close();
        } catch (Exception e) {
            String s = e.toString();
        }
        return yearData;
    }

    /**
     * Finds the indices for the beginning and end of the years in the netcdf
     * data model
     *
     * @return
     */
    @Override
    public int[][] findYearReadIndexes() {

        // try reading from file first to avoid performance hit
        int[][] indices = null;
        String filePath = path + yearIndicesFileName;
        ObjectInputStream mySecondStream;
        try {
            mySecondStream = new ObjectInputStream(new FileInputStream(filePath));

        indices = (int[][]) mySecondStream.readObject();
                } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Tasmax_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(indices != null) return indices;
        
        indices = new int[150][2];
        int yearIndexCnt = 0;

        NetcdfFile inputFile = null;

        Variable ncdfVar;               // NetCDF Variable
        ArrayFloat.D3 d3Var;            // 3D NetCDF float array        
        List<Variable> inputVariables;
        Array dataSection = null;

        String time = "time";
        int[] shape;
        int[] orgin;
        int size;

        // starting index always 0
        indices[0][0] = 0;

        int activeYear = getStartYear();

        // loop through each of the five files
        for (String file : files) {

            try {
                inputFile = NetcdfFile.open(file);

                ncdfVar = inputFile.findVariable("time");

                // lets check out the time var 
                ncdfVar = inputFile.findVariable(time);
                DataType dataType = ncdfVar.getDataType();
                List<Dimension> dims = ncdfVar.getDimensions();

                size = dims.get(0).getLength();
                shape = new int[]{size};

                Attribute attr = ncdfVar.findAttribute("units");
                String dateString = attr.toString();
                String[] ss = dateString.split(" ");
                String date = ss[4];
                String[] dateArr = date.split("-");
                String year = dateArr[0];
                String month = dateArr[1];
                String day = dateArr[2];
                DateUnit dateUnit = null;
                Calendar calDate = Calendar.getInstance();
                calDate.set(Integer.parseInt(year), (Integer.parseInt(month) - 1), Integer.parseInt(day));
                Date javaDate = calDate.getTime();
                String[] dates = new String[size];

                orgin = new int[]{0};
                shape = new int[]{size};
                try {
                    dataSection = ncdfVar.read(orgin, shape);
                } catch (InvalidRangeException ex) {
                    Logger.getLogger(Tasmax_1.class.getName()).log(Level.SEVERE, null, ex);
                }

                // iterate through the time values
                for (int i = 0; i < size; i++) {

                    if (i == 0) {
                        indices[yearIndexCnt][0] = 0;
                    }
                    Double val = null;
                    try {
                        val = dataSection.getDouble(i);
                    } catch (Exception e) {
                        String s = e.toString();
                    }
                    //   Double val = dataSection.getDouble(Index.factory(shape));
                    try {
                        /*
                         value - number of time units
                         timeUnitString - eg "secs"
                         since - date since, eg "secs since 1970-01-01T00:00:00Z"
                         */
                        dateUnit = new DateUnit(val, "days", javaDate);
                    } catch (Exception ex) {
                        Logger.getLogger(Tasmax_1.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Date curDate = dateUnit.getDate();

                    dates[i] = curDate.toString();
                    // System.out.println(i + ": " + val + " Date: " + curDate.toString());
                    // figure out what year this value is
                    String[] tempVals = curDate.toString().split(" ");
                    int tempYr = Integer.parseInt(tempVals[tempVals.length - 1]);
                    if (tempYr != activeYear && i != 0) {
                        indices[yearIndexCnt][1] = i;
                        yearIndexCnt++;
                        indices[yearIndexCnt][0] = i + 1;
                        activeYear = tempYr;
                    }
                    if (i == size - 1) {
                        indices[yearIndexCnt][1] = size - 1;
                        yearIndexCnt++;
                        activeYear++;
                    }
                }
                inputFile.close();

            } catch (IOException ex) {
                Logger.getLogger(Tasmax_1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        readIndexes = indices;
        // cache the year indices to file
        
        ObjectOutputStream myStream;
        try {
            myStream = new ObjectOutputStream(new FileOutputStream(filePath));

            myStream.writeObject(indices);
            myStream.close();
        } catch (IOException ex) {
            Logger.getLogger(Tasmax_1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return indices;
    }

    /**
     * Reads in the netcdf 365-6 days for a given lat / long coordinate
     *
     * @param o
     * @return
     */
    public float[] readLocalizedYear(int x, int y, int z) {

        NetcdfFile inputFile = null;    // target netCDF file
        int zIndex;
        int yr;
        int readIndex;
        int readAmount;
        int inputFileIndex;// the index 0-4 of which file to start with
        int[] orgin;
        int[] shape;
        int latitude;
        int longitude;
        int time;
        Variable ncdfTasMaxVar;
        Array dataSection;
        // our climate model file set
        int[][] dims = this.getDimensions();
        String[] files = this.getFiles();

        yr = 1950 + z; // 2099 is the last year, zIndex will be 0-149

        readIndex = readIndexes[z][0];
        readAmount = readIndexes[z][1] - readIndexes[z][0];

        String fileToRead = "";

        if (yr > 2074) {
            fileToRead = files[4];
        } else if (yr > 2050) {
            fileToRead = files[3];
        } else if (yr > 2026) {
            fileToRead = files[2];
        } else if (yr > 2005) {
            fileToRead = files[1];
        } else {
            fileToRead = files[0];
        }
        // open the file
        try {
            inputFile = NetcdfFile.open(fileToRead);
        } catch (Exception e) {
            String s = e.toString();
            return null;
        }
        ncdfTasMaxVar = inputFile.findVariable(varname);

        longitude = x;
        latitude = y;
        time = readIndex;
        orgin = new int[]{time, latitude, longitude};
        shape = new int[]{readAmount, 1, 1};
        float[] daysTemps = null;
        try {
            // read and set the array as a class variable
            dataSection = ncdfTasMaxVar.read(orgin, shape);
            daysTemps = (float[]) dataSection.copyTo1DJavaArray();

        } catch (Exception e) {
            String s = e.toString();
            return null;
        }
        try {
            inputFile.close();
        } catch (Exception e) {
        }
        return daysTemps;
    }

    /**
     * @return the startYear
     */
    public int getStartYear() {
        return startYear;
    }

    /**
     * @return the tvalue
     */
    public double getTvalue() {
        return tvalue;
    }

    /**
     * @return the numYears
     */
    public int getNumYears() {
        return numYears;
    }
}
