
/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.toe;

/**
 * Provenance collection enabled version of the Tasmax class.
 *
 * The main purpose of this class is to separate provenance collection from the
 * simulation. The only modification necessary in the original code is swapping
 * the class type in a single line of code within JobManager.submitJob.
 *
 * This class is almost identical to the Tasmax class, as of 8/11/2015. This
 * class was introduced to provide protected visibility for some of the Tasmax
 * class data. Data that was elevated to protected visibility is required for
 * verbose provenance collection. By modifying the visibility to protected, a
 * provenance wrapper (the TasmaxProv class) can include the member data in the
 * provenance record.
 *
 * @author jwoodrin and Delmar B. Davis
 */

    
    /**
     * READ ME
     * Entire class commented out because there were many code changes made to the Tasmax class and this class has too much code duplication. 
     *
     */
public class TasmaxProtected{
//public class TasmaxProtected extends AbstractToe {

//    private final int TOE_NULL_VAL = 0;
//
//    // step 2 vars
//    private double[][] minHistTolVals;
//    private double[][] maxHistTolVals;
//
//    // step 4 vars
//    private double[][] slopes;
//    private double[][] confidenceInterval;
//
//    // step 5 vars
//    private double[][] climatologies;
//    private double[][] slopeMinusConInt;
//    private double[][] slopePlusConInt;
//    private double[][][] climaSlope1;
//    private double[][][] climaSlope2;
//    private double[][][] climaSlope3;
//    // the start year for the first year toe calculation
//    protected static final int TOE_START_YEAR = 2001;
//
//    // final vars
//    private int[][] toeReg;
//    private int[][] toePls;
//    private int[][] toeMin;
//
//    // params
//    protected float climateTempThreshold;
//    protected double minMaxTol;
//
//    private int toeThreshold;
//
//    private String jobsDirectory = "";
//
//    protected boolean asyncAgents = true;
//
//    protected boolean placesRead = true;
//
//    protected int nodes = 1;
//
//    /**
//     * Main constructor Accepts params arguments from web page
//     *
//     * @param params
//     */
//    public TasmaxProtected(String[] params, String jobsDir) {
//
//        jobsDirectory = jobsDir;
//
//        try {
//            climateTempThreshold = Float.parseFloat(params[0]);
//        } catch (Exception e) {
//            climateTempThreshold = 18.3F;
//        }
//
//        try {
//            minMaxTol = Double.parseDouble(params[1]);
//            if (minMaxTol > 1.00D || minMaxTol < 0.00D) {
//                minMaxTol = 0.60D;
//            }
//        } catch (Exception e) {
//            minMaxTol = 0.60D;
//        }
//
//        try {
//            numOfYears = Integer.parseInt(params[2]);
//        } catch (Exception e) {
//            numOfYears = 200;
//        }
//
//        ProvMgr provMgr = ProvMgr.getInstance();
//    }
//
//    /**
//     * Inits the Places and Agents for the MASS Tasmax calculation
//     */
//    public void massInit() {
//
//        int interv = 0;
//
//        int[][] grid = inputClimateModel.getDimensions();
//        x = grid[0][0]; // longitude(east / west)
//        y = grid[0][1]; // latitude (north / south)
//        z = 150;        // time    
//
////        x = 5;
////        y = 5;
////        z = 5;
//        String msg = " Init TasmaxPlace Places size  x:" + Integer.toString(x)
//                + " y:" + Integer.toString(y) + " z:" + Integer.toString(z)
//                + " using " + this.nodes + " nodes.";
//        this.getProvLogger().logProvenance(msg);
//
//        // instanciate our places
//        places = new Places(jobNumber, "uwca.calculations.toe.places.TasmaxPlace", (Object) interv, x, y, z);
//        places.callAll(TasmaxPlace.setNumberOfNodes, this.nodes);
//
//        msg = " Init TasmaxAgent Agents size  x:" + Integer.toString(x) + " y:"
//                + Integer.toString(y) + " with async agent migration: " + this.asyncAgents;
//        this.getProvLogger().logProvenance(msg);
//
//        agents = new Agents(jobNumber, "uwca.calculations.toe.agents.TasmaxAgent", null, places, x * y);
//        agents.callAll(TasmaxAgent.setAgentAsync, (Object) this.asyncAgents);
//    }
//
//    /*
//     * STEP 1: Read NetCDF year into TasmaxPlaces Read in each year into a place
//     * by long (x) && lat (y) coordinates - each place in the time dimension
//     * will have 365 - 366 days Each place iterates through the 365-6 days to
//     * find days over threshold Each place iterates through the 365-6 days to
//     * find days over threshold (PARAM 1). The result will be the number of days
//     * over threshold - one int # kept by place class output
//     */
//    protected void readDataIntoPlaces() {
//
//        String msg = "STEP 1 STARTED:  Find days over threshold ";
//        this.getProvLogger().logProvenance(msg);
//
//        msg = " Setting Climate Threshold: " + Double.toString(this.climateTempThreshold);
//        this.getProvLogger().logProvenance(msg);
//        // first set the climate threshold
//        places.callAll(TasmaxPlace.setClimateTempThreshold, this.climateTempThreshold);
//        // set climate model
//        // places.callAll(TasmaxPlace.setClimateModel, (Object)inputClimateModel);
//
//        places.callAll(TasmaxPlace.setNumberOfNodes, (Object) 1);
//
//        msg = " Finding year indexes ";
//        this.getProvLogger().logProvenance(msg);
//        int[][] yearIndices = inputClimateModel.findYearReadIndexes();
//
//        msg = " Starting find stripe size";
//        this.getProvLogger().logProvenance(msg);
//        places.callAll(TasmaxPlace.findStripeSize);
//        msg = " Ending find stripe size";
//        this.getProvLogger().logProvenance(msg);
//
//        if (placesRead) {
//            msg = " Beginning Places read algorithm using " + nodes + " nodes.";
//            this.getProvLogger().logProvenance(msg);
//            this.placesRead(yearIndices);
//        } else {
//            msg = " Beginning master node read algorithm using " + nodes + " nodes.";
//            this.getProvLogger().logProvenance(msg);
//            this.masterNodeDataRead(x, y, z, yearIndices); // method 1 -- read from master node
//
//        }
//
//    }
//
//    /*
//     * STEP 2: Find Historical tolerance 1950 - 1999. Find the min / max %'s
//     * (PARAM 2 && 3) Find Historical tolerance 1950 - 1999. Find the min / max
//     * %'s (PARAM 2 && 3) OUTPUT: 2 2 dim array's (x * y) of min and max values
//     * - this output is used in the final step for finding the ToE
//     */
//    protected void findHistoricalTolerance() {
//        String msg = "STEP 2 STARTED:  Finding historical Tolerances Parm Tolerance used: " + Double.toString(minMaxTol);
//        this.getProvLogger().logProvenance(msg);
//        // AGENTS
//        /**
//         * We need to tell the agents where to start their journey for step 3
//         */
//        int[] dims = new int[2];
//        dims[0] = x;
//        dims[1] = y;
//        agents.callAll(TasmaxAgent.setInitialHistoricalTolerancePosition, (Object) dims);
//
//        // update agent statuses (need to do after each migration)
//        //    agents.manageAll();
//        // get our historical min / max values
////        if (asyncAgents) {
////
////            int[] funcIds = new int[51];
////            funcIds[0] = TasmaxAgent.migrateToInitialHistTolPos;
////            for (int i = 0; i < 50; i++) {
////                funcIds[i + 1] = TasmaxAgent.gatherHistoricalTolerance;
////            }
////            Object[] agentArgs = new Object[x * y];
////            try {
////                agents.callAllAsync(funcIds, agentArgs);
////            } catch (Throwable e) {
////                String error = e.toString();
////            }
////        } else {
//            agents.callAll(TasmaxAgent.migrateToInitialHistTolPos);
//            agents.manageAll();
//            for (int i = 0; i < 50; i++) {
//                agents.callAll(TasmaxAgent.gatherHistoricalTolerance);
//                agents.manageAll();
//            }
////        }
//
//        agents.callAll(TasmaxAgent.calculateHistoricalTolerance, minMaxTol);
//   //     agents.manageAll();
//
//        // return the values        
//        Object[] historicalToleranceVals = (Object[]) agents.callAll(TasmaxAgent.getHistoricalToleranceVals, new Object[x * y]);
//
//        /**
//         * The return objects contain 1-d int arrays containing
//         * historicalToleranceVals[0] = min historical value
//         * historicalToleranceVals[1] = max historical value
//         * historicalToleranceVals[2] = agent place x index
//         * historicalToleranceVals[3] = agent place y index
//         */
//        minHistTolVals = new double[x][y];
//        maxHistTolVals = new double[x][y];
//        for (int i = 0; i < historicalToleranceVals.length; i++) {
//            double[] vals = (double[]) historicalToleranceVals[i];
//            int xIndex = (int) vals[2];
//            int yIndex = (int) vals[3];
//            // set our array vals
//            minHistTolVals[xIndex][yIndex] = vals[0];
//            maxHistTolVals[xIndex][yIndex] = vals[1];
//        }
//    }
//
//    /*
//     * STEP 3: Find Climatology - 1980 - 2010 average - 1 2 dim (x*y) double
//     * array output representing the average days over threshold for 1980 - 2010
//     */
//    protected void findClimatology() {
//
//        String msg = "STEP 3 STARTED:  Find climatology ";
//        this.getProvLogger().logProvenance(msg);
//
//        //    agents.manageAll();
//        // get our historical min / max values
////        if (asyncAgents) {
////            int climatologyYears = 30;
////            int[] funcIds = new int[climatologyYears + 1];
////            funcIds[0] = TasmaxAgent.setClimatologyInitPosition;
////            for (int i = 0; i < climatologyYears; i++) {
////                funcIds[i + 1] = TasmaxAgent.gatherClimatologyValues;
////            }
////            Object[] agentArgs = new Object[x * y];
////            for (int i = 0; i < (x * y); i++) {
////                agentArgs[i] = 29;
////            }
////
////            try {
////                agents.callAllAsync(funcIds, agentArgs);
////            } catch (Throwable e) {
////                String error = e.toString();
////            }
////        } else {
//            agents.callAll(TasmaxAgent.setClimatologyInitPosition, 29);
//            for (int i = 0; i < 30; i++) {
//                agents.callAll(TasmaxAgent.gatherClimatologyValues);
//                agents.manageAll();
//            }
////        }
//
//        // calculate our historical tolerance
//        agents.callAll(TasmaxAgent.calculateClimatology);
//        agents.manageAll();
//
//        // get and format the climatologies into a local array
//        climatologies = new double[x][y];
//        Object[] agentClimatologies = (Object[]) agents.callAll(TasmaxAgent.getClimatologyValue, new Object[x * y]);
//        for (int i = 0; i < agentClimatologies.length; i++) {
//            int xIndex = i % x;
//            int yIndex = i / x;
//            climatologies[xIndex][yIndex] = (double) agentClimatologies[i];
//        }
//    }
//
//    /*
//     * STEP 4: LSR - Least Squared Regression For 2006-2099 range produce 3 2d
//     * arrays (full long and lat coordinates) - 2d array of slopes - 2d array of
//     * Confidence interval (Error term * tvalue) + slope - 2d array of
//     * Confidence interval (Error term * tvalue) - slope
//     */
//    protected void leastSquaredRegression() {
//
//        String msg = "STEP 4 STARTED:  LSR - Least Squared Regression ";
//        this.getProvLogger().logProvenance(msg);
//
//        slopes = new double[x][y];
//        slopePlusConInt = new double[x][y];
//        slopeMinusConInt = new double[x][y];
//        // set the initial position for step 5
//        int lsrStartPosition = 2006 - inputClimateModel.getStartYear();
//        int lsrEndPosition = z - 1;
//
//        // move the agents along the z axis and gather values
////        if (asyncAgents) {
////            int[] funcIds = new int[lsrEndPosition * 2 + 1];
////            funcIds[0] = TasmaxAgent.setInitialLsrPosition;
////            for (int i = 0; i < lsrEndPosition * 2; i += 2) {
////                funcIds[i + 1] = TasmaxAgent.gatherLsrValue;
////                funcIds[i + 2] = TasmaxAgent.migrateZDimension;
////            }
////            Object[] agentArgs = new Object[x * y];
////            for (int i = 0; i < x * y; i++) {
////                agentArgs[i] = lsrStartPosition;
////            }
////            try {
////                agents.callAllAsync(funcIds, agentArgs);
////            } catch (Throwable e) {
////                String error = e.toString();
////            }
////        } else {
//            agents.callAll(TasmaxAgent.setInitialLsrPosition, (Integer) lsrStartPosition);
//            // update agent statuses
//            agents.manageAll();
//            for (int i = 0; i < lsrEndPosition; i++) {
//                agents.callAll(TasmaxAgent.gatherLsrValue);
//                agents.callAll(TasmaxAgent.migrateZDimension);
//                agents.manageAll();
//            }
////        }
//
//        // calculate the lsr values
//        agents.callAll(TasmaxAgent.calculateLsrValues);
//
//        // get slope values
//        Object[] agentSlopes = (Object[]) agents.callAll(TasmaxAgent.getSlopes, new Object[x * y]);
//        // get confidence
//        Object[] agentErrorTerm = (Object[]) agents.callAll(TasmaxAgent.getErrorTerm, new Object[x * y]);
//
//        int cnt = 0;
//
//        for (int i = 0; i < agentSlopes.length; i++) {
//
//            int xIndex = i % x;
//            int yIndex = i / x;
////            double agentSlope = Math.abs((double)agentSlopes[i]);
////            double agentErrTerm = Math.abs((double)agentErrorTerm[i]);
//            double agentSlope = (double) agentSlopes[i];
//            double agentErrTerm = (double) agentErrorTerm[i];
//            double tval = inputClimateModel.getTvalue();
//            slopes[xIndex][yIndex] = agentSlope;
//            slopePlusConInt[xIndex][yIndex] = agentSlope + (agentErrTerm * tval);
//            slopeMinusConInt[xIndex][yIndex] = agentSlope - (agentErrTerm * tval);
//        }
//    }
//
//    /*
//     * STEP 5 Find ToE Take the 3 output arrays from step 4 and expand them into
//     * 3d arrays which we will use to find our ToE in step 6
//     */
//    protected void findToe() {
//        String msg = "STEP 5 STARTED:  Find ToE, Number of years Param used: " + Integer.toString(numOfYears);
//        this.getProvLogger().logProvenance(msg);
//
//        climaSlope1 = new double[x][y][numOfYears]; // z[0] = clima; z[1] = z[0] + slopes; z[2] = z[1] + slopes; ... ect
//        climaSlope2 = new double[x][y][numOfYears]; // z[0] = clima; z[1] = z[0] + slopePlusConInt; z[2] = z[1] + slopePlusConInt; ... ect
//        climaSlope3 = new double[x][y][numOfYears]; // z[0] = clima; z[1] = z[0] + slopeMinusConInt; z[2] = z[1] + slopeMinusConInt; ... ect
//
//        for (int i = 0; i < x; i++) {
//            for (int k = 0; k < y; k++) {
//                // assigning extra vars here for debugging / readability purposes
//                double clima1;
//                double clima2;
//                double clima3;
//
//                double maxHist = maxHistTolVals[i][k];
//                double minHist = minHistTolVals[i][k];
//                // if min and max tolerances are 0, then set the toe to 0
//                if (maxHist == 0 && minHist == 0) {
//                    toeReg[i][k] = 0;
//                    toePls[i][k] = 0;
//                    toeMin[i][k] = 0;
//                }
//
//                for (int j = 0; j < numOfYears; j++) {
//
//                    if (j == 0) {
//                        clima1 = climaSlope1[i][k][j] = climatologies[i][k];
//                        clima2 = climaSlope2[i][k][j] = climatologies[i][k];
//                        clima3 = climaSlope3[i][k][j] = climatologies[i][k];
//                    } else {
//                        clima1 = climaSlope1[i][k][j] = climaSlope1[i][k][j - 1] + slopes[i][k];
//                        clima2 = climaSlope2[i][k][j] = climaSlope2[i][k][j - 1] + slopePlusConInt[i][k];
//                        clima3 = climaSlope3[i][k][j] = climaSlope3[i][k][j - 1] + slopeMinusConInt[i][k];
//                    }
//
//                    // FIND TOE
//                    // first array
//                    if (clima1 > maxHist && toeReg[i][k] == TOE_NULL_VAL) {
//                        toeReg[i][k] = TOE_START_YEAR + j;
//                    } else if (clima1 < minHist && toeReg[i][k] == TOE_NULL_VAL) {
//                        //      toeReg[i][k] = (TOE_START_YEAR + j) * -1;
//                    }
//                    // second array
//                    if (clima2 > maxHist && toePls[i][k] == TOE_NULL_VAL) {
//                        toePls[i][k] = TOE_START_YEAR + j;
//                    } else if (clima2 < minHist && toePls[i][k] == TOE_NULL_VAL) {
//                        //       toePls[i][k] = (TOE_START_YEAR + j) * -1;
//                    }
//                    // third array
//                    if (clima3 > maxHist && toeMin[i][k] == TOE_NULL_VAL) {
//                        toeMin[i][k] = TOE_START_YEAR + j;
//                    } else if (clima3 < minHist && toeMin[i][k] == TOE_NULL_VAL) {
//                        //      toeMin[i][k] = (TOE_START_YEAR + j) * -1;
//                    }
//                }
//            }
//        }
//    }
//
//    /**
//     * Inits the ToE arrays with 0 values
//     */
//    protected void initToeArrays() {
//
//        toeReg = new int[x][y];
//        toePls = new int[x][y];
//        toeMin = new int[x][y];
//
//        for (int i = 0; i < x; i++) {
//            for (int k = 0; k < y; k++) {
//                toeReg[i][k] = TOE_NULL_VAL;
//                toePls[i][k] = TOE_NULL_VAL;
//                toeMin[i][k] = TOE_NULL_VAL;
//            }
//        }
//    }
//
//    /**
//     * The main method which drives our calculations
//     */
//    public void executeCalculations() {
//
//        this.nodes = 15;
//        this.asyncAgents = false;
//        this.placesRead = false;
//        // init MASS
//        massInit();
//        initToeArrays();
//        //        places.callAll(TasmaxPlace.setNumberOfNodes, (Object)1);
//
//        // step 1 read data
//        readDataIntoPlaces();
//
//        String msg = "Starting fake read";
//        this.getProvLogger().logProvenance(msg);
//        places.callAll(TasmaxPlace.falsifyDaysOverThreshold); // temp method for testing (speeds up performance)
//        msg = "ending fake read";
//        this.getProvLogger().logProvenance(msg);
//
//        // step 2
//        findHistoricalTolerance();
//        // step 3
//        findClimatology();
//        // step 4
//        leastSquaredRegression();
//        // step 5
//        findToe();
//
////        placesTest();
////        agentTest();
//        places = null;
//        agents = null;
//
//    }
//
//    public void writeNetCdfFiles(String toeRegFile, String toeMinFile, String toeMaxFile) {
//        NetCdf fileWriter = new NetCdf();
//        fileWriter.writeToeFile(toeRegFile, x, y, toeReg);
//        fileWriter.writeToeFile(toeMaxFile, x, y, toePls);
//        fileWriter.writeToeFile(toeMinFile, x, y, toeMin);
//
//        String msg = " Writting files, toeReg, toePls, and toeMin to jobs folder ";
//        this.getProvLogger().logProvenance(msg);
//    }
//
//    /**
//     * reads in an entire lat * long * 365-6 data points and distributes the
//     * array to the places
//     *
//     * @param x - longitude dimension
//     * @param y - latitude dimension
//     * @param z - time dimension
//     * @param yearIndices
//     */
//    public void masterNodeDataRead(int x, int y, int z, int[][] yearIndices) {
//
//        int numYears = inputClimateModel.getNumYears();
//        int startYear = inputClimateModel.getStartYear();
//        // loop through the entire range of years
//        for (int i = 0; i < numYears; i++) {
//            int year = i + startYear;
//
////            String msg = " Reading Year: " + Integer.toString(year);
////            this.getProvLogger().logProvenance(msg);
//            Object obj = inputClimateModel.readFullYear(x, y, i, yearIndices);
//
//            float[][][] tempVals = (float[][][]) obj;
//            float[] yearData = tempVals[0][0];
//
//            Object[] placesArgs = new Object[x * y * z]; // the args we'll be sending to the places
//            for (int a = 0; a < x; a++) {
//                for (int b = 0; b < y; b++) {
//                    /*
//                     * We need to take each x&y coordinate from our temp values
//                     * and put them into the proper index within a 1d object
//                     * array to be sent to the proper places. The formula is:
//                     * Index of array = m *z z = the number of elements in the z
//                     * dimension of the array m = (xindex * ymax) + (yindex %
//                     * ymax)
//                     */
//                    int m = (a * y) + b;
//                    //  int m = (a * y) + (b % y);
//                    int ind = (m * z) + i; // i being the year index modifier
//                    try {
//                        placesArgs[ind] = tempVals[a][b];
//                    } catch (Exception e) {
//                        String s = e.toString();
//                    }
//                }
//            }
//
//            // set the temp year values
//            places.callAll(TasmaxPlace.setDaysArray, placesArgs);
//            tempVals = null;
//            placesArgs = null;
//        }
//    }
//
//    /*
//     * Uses the TasmaxPlace method to read in the 365-6 day float arrays from
//     * within each Place This method is set up to read in certain slices of the
//     * time dimension (z) at a time to improve performance.
//     */
//    public void placesRead(int[][] yearIndices) {
//        // the places will need the climate model for this alg, so send it
//        //    places.callAll(TasmaxPlace.setClimateModel, (Object)inputClimateModel);
//
//        // set the year indexes 
//        places.callAll(TasmaxPlace.setYearIndexArray, (Object) yearIndices);
//        // Read the data into the static variables on the node for data caching   
//        places.callAll(TasmaxPlace.readNetCdfData);
//        // this reads the year data from the stripes cached on the nodes
//        places.callAll(TasmaxPlace.readYearData);
//    }
//
//    /*
//     * ****************************************************************************************************************
//     * Test methods to get place and agent information to verify mass is working
//     * across cluster
//     * ***************************************************************************************************************
//     */
//    /**
//     * Tester method for Places
//     */
//    public void placesTest() {
//
//        Object[] placesIndexes = (Object[]) places.callAll(TasmaxPlace.findHostName, new Object[x * y * z]);
//
//        String s = "";
//
//    }
//
//    /**
//     * Tester method for Agents
//     */
//    public void agentTest() {
//        Object o = agents.callAll(TasmaxAgent.getPlaceIndex, new Object[x * y]);
//        Object[] agentIndexes = (Object[]) o;
//
//        String s = "";
//    }
}
