/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.agents;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.uw.bothell.css.dsl.MASS.Agent;
import edu.uw.bothell.css.dsl.MASS.MASS;

/**
 * Agent which is used to find what x dimension places exist on what nodes.
 * 
 */
@SuppressWarnings("serial")
public class PlaceFinder extends Agent {

    private boolean agentAsync = true;

    public static int X_STRIPE_SIZE = 462;  // TODO: set dynamically to x places dimension
    String hostName = null;
    private int index = 0;
    private List<Integer> readPlaces = new ArrayList<>();

    /**
     * Methods that are callable from callAll
     */
    public static final int setAgentAsync = 0;
    public static final int setInitialPosition = 1; // for beginning of step 3 
    public static final int getPlaceIndex = 2;
    public static final int getReadPlaces = 3;
    public static final int migrate = 4;

    /**
     *
     * @param o
     */
    public PlaceFinder(Object o) {
    }

    /**
     * Call All methods
     *
     * @param funcId
     * @param o
     * @return
     */
    @Override
    public Object callMethod(int funcId, Object o) {
        switch (funcId) {
            case setAgentAsync:
                return setAgentsAsync(o);
            case setInitialPosition:
                return getPlaceIndex(o);
            case getPlaceIndex:
                return getPlaceIndex(o);
            case getReadPlaces:
                return getReadPlaces(o);
            case migrate:
                return migrate(o);
            default:
                return null;
        }
    }

    /**
     * Sets whether agents should work async or not
     *
     * @param o
     * @return
     */
    public Object setAgentsAsync(Object o) {
        this.agentAsync = Boolean.valueOf(o.toString());
        return null;
    }

    /**
     * Migrates the agent
     *
     * @param o
     * @return
     */
    public Object migrate(Object o) {
        try {
            int xModifier = this.getPlace().getIndex()[0];
            int yModifier = this.getPlace().getIndex()[1];
            int zModifier = this.getPlace().getIndex()[2];

            if (xModifier == 0) {
                this.hostName = this.getHostName();
                this.readPlaces.add(0);
            }
            this.index++;
            xModifier++;
//            if (this.agentAsync) {
//                migrateAsync(xModifier, yModifier, zModifier);
//            } else {
                migrate(xModifier, yModifier, zModifier);
//            }

            String newHostName = this.getHostName();
            if (!newHostName.equals(this.hostName)) {
                this.readPlaces.add(this.index);
            }

            this.hostName = this.getHostName();
        } catch (Exception ex) {
            MASS.getLogger().debug("PlaceFinder agent migrated to " + this.hostName);
        }
        return null;
    }

    /**
     * Gets the host name.
     * @return  name of the host
     */
    private String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) {
            Logger.getLogger(PlaceFinder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the index array showing the place location that the agent
     * currently resides on
     *
     * @param o
     * @return
     */
    public Object getPlaceIndex(Object o) {
        // return the place index
        return (Object) this.getPlace().getIndex();
    }

    /**
     * Gets the places read array.
     *
     * @param o
     * @return
     */
    public Object getReadPlaces(Object o) {
        return (Object) this.readPlaces.toArray();
    }
}
