/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.toe;

/**
 * Provides a provenance collection wrapper for TasmaxProtected objects.
 *
 * @author Delmar B. Davis
 */

    /**
     * READ ME
     * Entire class commented out because there were many code changes made to the Tasmax class and this class has too much code duplication. 
     *
     */
public class TasmaxProv{
//public class TasmaxProv extends TasmaxProtected {
//
//    public TasmaxProv(String[] params, String jobsDir) {
//        super(params, jobsDir);
//    }
//
//    /**
//     * The main method which drives our calculations
//     */
//    @Override
//    public void executeCalculations() {
//
//        // set filename to persist to
//        String w3cFile = "jobs/" + jobNumber + "/w3c.ttl";
//        // initialize provMgr
//        ProvMgr provMgr;
//        try {
//            provMgr = ProvMgr.getInstance();
//            if (provMgr != null && !provMgr.isInitialized()) {
//                provMgr.setFileName(w3cFile, false);
//            }
//            recProv(provMgr, w3cFile);
//        } catch (Exception ex) {
//            //log(ex);
//            provMgr = null;
//            Logger.getLogger(Job.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        this.nodes = 15;
//        this.asyncAgents = false;
//        this.placesRead = false;
//        // init MASS
//        massInit();
//        initToeArrays();
//
//        // step 1 read data
//        readDataIntoPlaces();
//        Object[] daysOverThresholdEntity = recFindDaysOverThresholdProv(provMgr);
//        // step 2
//        findHistoricalTolerance();
//        Object[] historicalTolerancesEntity = recFindHistoricalTolerancesProv(provMgr, daysOverThresholdEntity);
//        // step 3
//        findClimatology();
//        Object[] climatologyEntity = recFindClimatologyProv(provMgr, daysOverThresholdEntity);
//        // step 4
//        leastSquaredRegression();
//        Object[] leastSquaredRegressionEntity = findLeastSquaredRegressionProv(provMgr, daysOverThresholdEntity);
//        // step 5
//        findToe();
//        findToeProv(provMgr, historicalTolerancesEntity, climatologyEntity, leastSquaredRegressionEntity, numOfYears);
//        // write the recorded provenance out to a turtle file
//        if (w3cFile != null) {
//            try {
//                provMgr.persist(w3cFile);
//            } catch (IOException ex) {
//                Logger.getLogger(TasmaxProv.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        places = null;
//        agents = null;
//    }
//
//    private void recProv(ProvMgr provMgr, String w3cFile) {
//        Object[] daysOverThresholdEntity = recFindDaysOverThresholdProv(provMgr);
//        Object[] historicalTolerancesEntity = recFindHistoricalTolerancesProv(provMgr, daysOverThresholdEntity);
//        Object[] climatologyEntity = recFindClimatologyProv(provMgr, daysOverThresholdEntity);
//        Object[] leastSquaredRegressionEntity = findLeastSquaredRegressionProv(provMgr, daysOverThresholdEntity);
//        findToeProv(provMgr, historicalTolerancesEntity, climatologyEntity, leastSquaredRegressionEntity, numOfYears);
//
//        // write the recorded provenance out to a turtle file
//        if (w3cFile != null) {
//            try {
//                provMgr.persist(w3cFile);
//            } catch (IOException ex) {
//                Logger.getLogger(TasmaxProv.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
//
//    private Object[] recFindDaysOverThresholdProv(ProvMgr provMgr) {
//        java.util.Date start;
//        // step 1 read data
//        start = new java.util.Date();
//        // record step 1 provenance
//        Object[] daysOverThresholdEntity = null;
//        String[] fileNames = inputClimateModel.getFiles();
//        if (fileNames != null) {
//            String[] daysOverThresholdInput = java.util.Arrays.copyOf(fileNames, fileNames.length + 1);
//            daysOverThresholdInput[fileNames.length] = Float.toString(climateTempThreshold);
//            if (provMgr != null) {
//                daysOverThresholdEntity = TasmaxProvRecorder.record("readDataIntoPlaces", daysOverThresholdInput, provMgr, start);
//            }
//        }
//        return daysOverThresholdEntity;
//    }
//
//    private Object[] recFindHistoricalTolerancesProv(ProvMgr provMgr, Object[] daysOverThresholdEntity) {
//        // step 2
//        java.util.Date start = new java.util.Date();
//        // record step 2 provenance
//        Object[] historicalTolerancesEntity = null;
//        if (provMgr != null && daysOverThresholdEntity != null) {
//            Object[] findHistoricalToleranceInput = java.util.Arrays.copyOf(daysOverThresholdEntity, daysOverThresholdEntity.length + 1);
//            findHistoricalToleranceInput[daysOverThresholdEntity.length] = Double.toString(minMaxTol);
//            historicalTolerancesEntity = TasmaxProvRecorder.record("findHistoricalTolerance", findHistoricalToleranceInput, provMgr, start);
//        }
//        return historicalTolerancesEntity;
//    }
//
//    private Object[] recFindClimatologyProv(ProvMgr provMgr, Object[] daysOverThresholdEntity) {
//        // step 3
//        java.util.Date start = new java.util.Date();
//        // record step 3 provenance
//        Object[] climatologyEntity = null;
//        if (provMgr != null && daysOverThresholdEntity != null) {
//            Object[] findClimatologyInput = java.util.Arrays.copyOf(daysOverThresholdEntity, daysOverThresholdEntity.length + 1);
//            // "30" is not contained in an actual parameter, it is in Tasmax.findClimatology
//            findClimatologyInput[daysOverThresholdEntity.length] = "30";
//            climatologyEntity = TasmaxProvRecorder.record("findClimatology", findClimatologyInput, provMgr, start);
//        }
//
//        return climatologyEntity;
//    }
//
//    private Object[] findLeastSquaredRegressionProv(ProvMgr provMgr, Object[] daysOverThresholdEntity) {
//        // step 4
//        java.util.Date start = new java.util.Date();
//        // record step 4 provenance
//        Object[] leastSquaredRegressionEntity = null;
//        if (provMgr != null && daysOverThresholdEntity != null) {
//            Object[] leastSquaredRegressionInput = java.util.Arrays.copyOf(daysOverThresholdEntity, daysOverThresholdEntity.length + 2);
//            leastSquaredRegressionInput[daysOverThresholdEntity.length] = String.valueOf((2006 - inputClimateModel.getStartYear()));
//            leastSquaredRegressionInput[daysOverThresholdEntity.length + 1] = String.valueOf((z - 1));
//            leastSquaredRegressionEntity = TasmaxProvRecorder.record("leastSquaredRegression", leastSquaredRegressionInput, provMgr, start);
//        }
//        return leastSquaredRegressionEntity;
//    }
//
//    private void findToeProv(ProvMgr provMgr, Object[] historicalTolerancesEntity, Object[] climatologyEntity, Object[] leastSquaredRegressionEntity, int numOfYears) {
//        // step 5
//        java.util.Date start = new java.util.Date();
//        // record step 5 provenance
//        if (historicalTolerancesEntity != null && climatologyEntity != null && leastSquaredRegressionEntity != null) {
//            Object[] toeInputs = new Object[4];
//            toeInputs[0] = historicalTolerancesEntity[0];
//            toeInputs[1] = climatologyEntity[0];
//            toeInputs[2] = leastSquaredRegressionEntity[0];
//            toeInputs[3] = String.valueOf(numOfYears);
//            TasmaxProvRecorder.record("findToe", toeInputs, provMgr, start);
//        }
//    }
//
//    private void log(Exception e) {
//        try {
//            File file = new File("c:/Users/Del/Desktop/z_uwcaProvErrorLog.txt");
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//            // if file doesnt exists, then create it
//            if (!file.exists()) {
//                file.createNewFile();
//            }
//
//            PrintWriter fw = new PrintWriter(file.getAbsoluteFile());
//            e.printStackTrace(fw);
//        } catch (IOException ex) {
//            e.printStackTrace();
//        }
//    }
}
