/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis;

import static edu.uw.bothell.css.mass.apps.climateanalysis.JobRunner.JOBS_DIR;
import edu.uw.bothell.css.mass.apps.climateanalysis.climatemodels.Tasmax_1;
import edu.uw.bothell.css.mass.apps.climateanalysis.toe.Tasmax;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 * This file is meant to be a driver for the mass-climate-analysis library.
 */
public class MassClimateParallelRead {
    
    public static String SOURCE_DIR = "/CSSDIV/research/dslab/UWCA/Projects/mass_java_appl/mass-climate-analysis/target/classes/";
    public static String DEST_DIR = "/CSSDIV/research/dslab/UWCA/mass_home/";

    public static void main(String[] args) {

        /**
         * Lets copy our out put jar over the mass home location to save us some time.
         */
        /**
         * Clean out the old jobs from the directory to avoid clutter.
         */
        File jobsDir = new File(JobRunner.JOBS_DIR);
        try {
            FileUtils.cleanDirectory(jobsDir);
        } catch (IOException ex) {
            Logger.getLogger(JobRunner.class.getName()).log(Level.SEVERE, null, ex);
        }

        /**
         * Copy resources from development location to network location for use by remote nodes. (saves time from manually copying). Comment out this
         * part if developing / running from a different location.
         */
        // copy the jar from the build directory to the mass working directory for nodes
        File srcDir = new File(SOURCE_DIR);

        File destDir = new File(DEST_DIR);
        try {
            FileUtils.copyDirectory(srcDir, destDir);
        } catch (IOException ex) {
            Logger.getLogger(JobRunner.class.getName()).log(Level.SEVERE, null, ex);
        }

        /**
         * Create and run a job.
         */
        try {
            JobRunner jobRunner = new JobRunner();
            jobRunner.initMassLibrary();
            Job job = new Job(JOBS_DIR);
            job.setVariable(new Tasmax(null, JOBS_DIR));
            job.setVarName("tasmax");
            job.setInputModel(new Tasmax_1());
            job.setVariable(new Tasmax(null, JOBS_DIR));
             jobRunner.executeJob(job);
            jobRunner.finishMassLibrary();
            System.out.println("Job Finished");
        } catch (IOException ex) {
            Logger.getLogger(MassClimateParallelRead.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
