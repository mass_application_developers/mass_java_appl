/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.places;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Random;

import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Place;
import ucar.ma2.Array;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 *  TasmaxPlace which reads ands processes data for the tasmax calculation.
 * 
 */
@SuppressWarnings("serial")
public class TasmaxPlace extends Place {

    private static int placesHandle = -1;
    private int[] readPlaces = null;
    private String[] files = null; // netcdf files to read
    private int[][] fileDims = null; // dimensions of the files to read

    // variables for reading netcdf data into places
    private static ArrayList<Array> tempsFileChunks = null;
    // necessary to know so places can know what index the data chunks start at.
    private static Integer xStart = -1;

    /**
     * Step 1 && 2 variables
     */
    // our days over threshold value which each place is responsible for 
    public int daysOverThreshold = 0;
    private float climateTempThreshold = 0;
    private float[] daysTemps;
    // used to mark the location of year beginnings and endings in files
    private int[][] yearIndices;

    /**
     * Step 3 variables historicalThresholds[0] = min value
     * historicalThresholds[1] = max value
     */
    private double[] historicalThresholds;

    private int interval;
    /**
     * Methods that are callable from callAll
     */
    public static final int readNetCdfData = 1;
    public static final int readYearData = 27;
    public static final int getDaysOverThreshold = 4;
    public static final int falsifyDaysOverThreshold = 10;
    public static final int findHostName = 20;
    public static final int setDaysArray = 11;
    public static final int calculateDaysOverThreshold = 12;
    public static final int setClimateTempThreshold = 21;
    public static final int setYearIndexArray = 22;
    public static final int setReadPlaces = 28;
    public static final int setFiles = 29;
    public static final int setFileDims = 30;

    /**
     * public constructor
     *
     * @param interval
     */
    public TasmaxPlace(Object interval) {
        this.interval = ((Integer) interval);
    }

    /**
     *
     * @param method
     * @param o the method parameters
     * @return
     */
    @Override
    public Object callMethod(int method, Object o) {
        switch (method) {
            case getDaysOverThreshold:
                return getDaysOverThreshold(o);
            case falsifyDaysOverThreshold:
                return falsifyDaysOverThreshold(o);
            case findHostName:
                return findHostName(o);
            case setDaysArray:
                return setDaysArray(o);
            case calculateDaysOverThreshold:
                return calculateDaysOverThreshold(o);
            case setClimateTempThreshold:
                return setClimateTempThreshold(o);
            case setYearIndexArray:
                return setYearIndexArray(o);
            case readNetCdfData:
                return readNetCdfData();
            case readYearData:
                return readYearData(o);
            case setReadPlaces:
                return setReadPlaces(o);
            case setFiles:
                return setFiles(o);
            case setFileDims:
                return setFileDims(o);
            default:
                return null;
        }
    }

    /**
     * Sets the file dimensions.
     *
     * @param o
     * @return
     */
    public Object setFileDims(Object o) {
        this.fileDims = (int[][]) o;
        return null;
    }

    /**
     * Sets the files to read.
     *
     * @param o The files
     * @return null
     */
    public Object setFiles(Object o) {
        this.files = (String[]) o;
        return null;
    }

    /**
     * Sets the readPlaces which is determined by PlaceFinder agent class.
     * @param o
     * @return 
     */
    public Object setReadPlaces(Object o) {
        Object[] objArr = (Object[]) o;
        this.readPlaces = new int[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            this.readPlaces[i] = (int) objArr[i];
        }
        return null;
    }

    /**
     * ****************************************************************************************************************
     * STEP 1: Find days over temperature threshold
     * ***************************************************************************************************************
     */
    /**
     * Part of STEP 1: sets the 365-6 days array of values to be processed into
     * management variable
     *
     * @param o
     * @return
     */
    public Object setDaysArray(Object o) {
        int x = this.getIndex()[0];
        int y = this.getIndex()[1];
        int z = this.getIndex()[2];

        if (o != null) {
            try {
                daysTemps = (float[]) o;
                calculateDaysOverThreshold(new Object());
                daysTemps = null;
            } catch (Exception e) {
                String s = "";
            }
        }
        return null;
    }

    /**
     * Part of STEP 1: Set the climate threshold (user defined)
     *
     * @param o
     * @return
     */
    public Object setClimateTempThreshold(Object o) {
        try {
            climateTempThreshold = (float) o;
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * Part of STEP 1: Determines the number of days over the threshold
     *
     * @param o
     * @return
     */
    public Object calculateDaysOverThreshold(Object o) {
        if (daysTemps == null) {
            return null;
        }
        // step through the array stored and see how many days are over threshold
        for (float f : daysTemps) {
            if (climateTempThreshold <= f && f != 1.0E20f) {
                daysOverThreshold++;
            }
        }
        return null;
    }

    /**
     * Simply returns the daysOverThreshold value -- primarily used by Agents in
     * further steps
     *
     * @param o
     * @return
     */
    public Object getDaysOverThreshold(Object o) {
        return daysOverThreshold;
    }

    /**
     * Tester method which bypasses the read operations (not used by sequence
     * normally)
     *
     * @param o
     * @return
     */
    public Object falsifyDaysOverThreshold(Object o) {
        Random rn = new Random();
        daysOverThreshold = rn.nextInt(50);
        return null;
    }

    public Object setYearIndexArray(Object o) {
        yearIndices = (int[][]) o;
        return null;
    }

    /**
     * Reads the NetCdf file data in chunnks and stores it in static memory for
     * access to all places on this node.
     *
     * @return
     */
    public Object readNetCdfData() {

        // get place index
        int x = this.getIndex()[0];
        int y = this.getIndex()[1];
        int z = this.getIndex()[2];

        Boolean readPlace = false;
        int endReadPlace = 0;
        for (int i = 0; i < this.readPlaces.length; i++) {

            if (x == readPlaces[i] && y == 0 && z == 0) {
                readPlace = true;
                if (i == this.readPlaces.length - 1) {
                    endReadPlace = 461;
                } else {
                    endReadPlace = readPlaces[i + 1] - 1;
                }
            }
        }
        // exit if not a readPlace
        if (!readPlace) {
            return null;
        }

        String hostName = (String) findHostName(null);
        MASS.getLogger().debug("HOSTNAME: " + hostName);
        MASS.getLogger().debug("Place " + x + " is beginning read sequence");

        tempsFileChunks = new ArrayList<>();

        // initialize our data chunks variable
        for (int i = 0; i < files.length; i++) {
            tempsFileChunks.add(null);
        }

        int timeOrigin = 0;
        int latitudeOrigin = 0;
        int longitudeOrigin = x;
        xStart = x;

        //    int stripeSize = dims[0][0] / numNodes;
        // FOR EACH FILE, READ IN A CHUNK OF DATA
        int index = -1;
        for (final String file : files) {
            index++;

            int[] origin = new int[]{timeOrigin, latitudeOrigin, longitudeOrigin};

            int timeReadAmount = this.fileDims[index][2];
            int latReadAmount = this.fileDims[index][1];
            int longReadAmount = endReadPlace - x + 1;

            int[] shape = new int[]{timeReadAmount, latReadAmount, longReadAmount};

            int i = index;
            this.readFileChunk(i, file, origin, shape);

        }
        MASS.getLogger().debug("READ SEQEUENCE COMPLETE");
        return null;
    }

    private Object readFileChunk(int index, String file, int[] origin, int[] shape) {
        float[][][] tempData = null;
        String varname = "tasmax";
        Variable ncdfTasMaxVar;
        Array dataSection;
        NetcdfFile inputFile = null;    // target netCDF file
        // open the file
        try {
            MASS.getLogger().debug("Opening file: " + file);
            inputFile = NetcdfFile.open(file);
            MASS.getLogger().debug("Opened file: " + file);
        } catch (Exception e) {
            MASS.getLogger().debug("Error opening file: " + file + " " + e.toString());
            return null;
        }

        ncdfTasMaxVar = inputFile.findVariable(varname);

        try {
            // read and set the array as a class variable
            MASS.getLogger().debug("Reading file: " + file);
            dataSection = ncdfTasMaxVar.read(origin, shape);
            MASS.getLogger().debug("Read complete of file: " + file);
            MASS.getLogger().debug("Transforming to Java array index: " + index);       
            tempsFileChunks.set(index, dataSection);
            MASS.getLogger().debug("Data chunk added, index: " + index);

        } catch (IOException | InvalidRangeException e) {
            MASS.getLogger().debug("Exception in read sequence " + e.toString());
        }
        try {
            inputFile.close();
            MASS.getLogger().debug("END FILE READ : " + file + " closed");
        } catch (Exception ex) {
            MASS.getLogger().debug("Error closing file");
        }
        MASS.getLogger().debug("File read complete for file: " + file);
        return null;
    }

    /**
     *
     * @param o
     * @return
     */
    public Object readYearData(Object o) {
        
        int xIndex = this.getIndex()[0];
        int yIndex = this.getIndex()[1];
        int zIndex = this.getIndex()[2];
        int[] origin = new int[3];
        int[] shape = new int[3];
        int zStartReadPos;
        int zEndReadPos;
        Array dataChunk = null;

        int startTimeRange = yearIndices[zIndex][0];
        try {

            int fileOneEnd = 20820;
            int fileTwoEnd = fileOneEnd + 7670;
            int fileThreeEnd = fileTwoEnd + 8766;
            int fileFourEnd = fileThreeEnd + 8766;
            int fileFiveEnd = fileFourEnd + 9131;

            // BECAUSE THE FILE CHUNKS ARE SEPARATED BY THE TIME DIMENSION, FIGURE OUT
            // WHICH CHUNK WE NEED TO READ FROM
            if (startTimeRange < fileOneEnd) {
                dataChunk = tempsFileChunks.get(0);
            } else if (startTimeRange < fileTwoEnd) {
                dataChunk = tempsFileChunks.get(1);
            } else if (startTimeRange < fileThreeEnd) {
                dataChunk = tempsFileChunks.get(2);
            } else if (startTimeRange < fileFourEnd) {
                dataChunk = tempsFileChunks.get(3);
            } else {
                dataChunk = tempsFileChunks.get(4);
            }

            zStartReadPos = yearIndices[zIndex][0];
            zEndReadPos = yearIndices[zIndex][1];

            origin[0] = zStartReadPos; // time dim
            origin[1] = yIndex; // 222 - lat
            origin[2] = xIndex - xStart; // long dim
            if(origin[2] < 0) origin[2] = 0;
            shape[0] = zEndReadPos - zStartReadPos;
            shape[1] = 1;
            shape[2] = 1;

            Array tmpChunk = dataChunk.section(origin, shape);
            float[] nums = (float[]) tmpChunk.copyTo1DJavaArray();
            for (int i = 0; i < nums.length; i++) {
                try {
                    float f = nums[i];
                    //    daysTemps[i] = f;
                    if (climateTempThreshold <= f && f != 1.0E20f) {
                        daysOverThreshold++;
                    }

                } catch (Exception e) {
                    String s = "";
                }
            }

        } catch (Exception ex) {
            MASS.getLogger().error("Error finding days over threshold in readYearData: x: "
                    + xIndex + " y: " + yIndex + " zIndex: " + zIndex, ex );
            String msg = "\norgin 0 = " + origin[0] + "\n"+
                    "orgin 1 = " + origin[1] + "\n"+
                    "orgin 2 = " + origin[2] + "\n"+
                    "shape 0 = " + shape[0] + "\n"+
                    "shape 1 = " + shape[1] + "\n"+
                    "shape 2 = " + shape[2] + "\n"+
                    "zStart: " + xStart + "\n" +
                    "xIndex " + xIndex + "\n";
            MASS.getLogger().debug(msg);
        }
        return null;
    }

    /**
     * Returns host name and place index
     *
     * @param o
     * @return
     */
    public Object findHostName(Object o) {
        String s = "error";
        try {
            s = InetAddress.getLocalHost().getHostName() + " "
                    + Integer.toString(this.getIndex()[0]) + ":"
                    + Integer.toString(this.getIndex()[1]) + ":"
                    + Integer.toString(this.getIndex()[2]);

        } catch (Exception e) {
        }
        return s;
    }
}
