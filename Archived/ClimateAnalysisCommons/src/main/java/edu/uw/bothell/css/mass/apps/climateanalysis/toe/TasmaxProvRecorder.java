/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis.toe;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Resource;

import edu.uw.bothell.css.mass.apps.climateanalysis.prov.ProvMgr;

/**
 * Provides domain specific provenance recording functions for the Tasmax class.
 *
 * @author Delmar B. Davis
 */
public class TasmaxProvRecorder {

    private static Object[] readDataIntoPlaces(Object[] inputData, ProvMgr provMgr, Date start) {
        Date end = new Date();
        // get provMgr unless uninitialized
//        ProvMgr provMgr = null;
//        provMgr = ProvMgr.getInstance();
//        if (!provMgr.isInitialized()) {
//            return null;
//        }
        /* get input file names */
        String climateTempThreshold = Float.toString(18.3F);
        String[] files = new String[inputData.length - 1];
        for (int i = 0, im = inputData.length; i < im; i++) {
            if (inputData[i] instanceof String) {
                if (i == im - 1) {
                    climateTempThreshold = (String) inputData[i];
                } else {
                    files[i] = (String) inputData[i];
                }
            } else { // just fail before modifying prov model
                return null;
            }
        }
        // add file entities to prov model
        List<Resource> filesResources = new ArrayList<>();
        for (String file : files) {
            filesResources.add(provMgr.addEntity(file.replace("\\\\", "/"), "file", false, false));
        }
        /* add the climate temperature threshold parameter to prov model */
        String id = "Climate_temp_threshold_" + java.util.UUID.randomUUID();
        Resource climateTempThresholdEntity = provMgr.addEntity(id, "parameter", false, false);
        /* show that the climate temperature threshold parameter has its current value */
        provMgr.value(climateTempThresholdEntity, climateTempThreshold);
        /* define the activity that occurs from reading data into places */
        id = "Find_days_over_threshold_" + java.util.UUID.randomUUID();
        Resource findDaysOverThresholdActivity = provMgr.addActivity(id, "Find_days_over_threshold", false, false);
        /* show when the find days over threshold activity started */
        provMgr.startedAtTime(findDaysOverThresholdActivity, start);
        /* show when the find days over threshold activity ended */
        provMgr.endedAtTime(findDaysOverThresholdActivity, end);
        /* show that the activity was associated with the username of the server */
        // note: this should change to the username of the person
        //       responsible for initiating the simulations from the webapp
        try {
            String userName = System.getProperty("user.name");
            Resource user = provMgr.addEntity(userName, "username", false, false);
            provMgr.wasAssociatedWith(findDaysOverThresholdActivity, user);
        } catch (SecurityException e) {
        }
        /* show that the climate temperature threshold was used to find days over threshold */
        provMgr.used(findDaysOverThresholdActivity, climateTempThresholdEntity);
        /* show that the files were used to find days over threshold */
        for (Resource fileEntity : filesResources) {
            provMgr.used(findDaysOverThresholdActivity, fileEntity);
        }
        /* define the resulting days over threshold data */
        id = "Days_over_threshold_" + java.util.UUID.randomUUID();
        Resource daysOverThresholdEntity = provMgr.addEntity(id, "Days_over_threshold", false, false);
        /* show that finding days over threshold generated days over threshold data */
        provMgr.generated(findDaysOverThresholdActivity, daysOverThresholdEntity);
        /* return days over threshold data definition */
        Object[] outputResources = new Object[1];
        outputResources[0] = daysOverThresholdEntity;
        return outputResources;
    }

    private static Object[] findHistoricalTolerance(Object inputData[], ProvMgr provMgr, Date start) {
        Date end = new Date();
        /* get provMgr unless uninitialized */
//        ProvMgr provMgr = null;
//        provMgr = ProvMgr.getInstance();
//        if (!provMgr.isInitialized()) {
//            return null;
//        }
        /* get input if it is actually usable as a resource */
        Resource daysOverThresholdEntity;
        if (inputData != null && inputData.length > 0 && inputData[0] instanceof Resource) {
            daysOverThresholdEntity = (Resource) inputData[0];
        } else { // just fail before modifying prov model
            return null;
        }
        /* define the activity that occurs from finding historical tolerances */
        String id = "Find_historical_tolerances_" + java.util.UUID.randomUUID();
        Resource findHistoricalTolerancesActivity = provMgr.addActivity(id, "Find_historical_tolerances", false, false);
        /* show when the find historical tolerances started */
        provMgr.startedAtTime(findHistoricalTolerancesActivity, start);
        /* show when the find historical tolerances activity ended */
        provMgr.endedAtTime(findHistoricalTolerancesActivity, end);
        /* show that the activity was associated with the username of the server */
        // note: this should change to the username of the person
        //       responsible for initiating the simulations from the webapp
        try {
            String userName = System.getProperty("user.name");
            Resource user = provMgr.addEntity(userName, "username", false, false);
            provMgr.wasAssociatedWith(findHistoricalTolerancesActivity, user);
        } catch (SecurityException e) {
        }
        /* show that the days over threshold data was used to find historical tolerances */
        provMgr.used(findHistoricalTolerancesActivity, daysOverThresholdEntity);
        // record the minimum maximum tolerance parameter
        String minMaxTolerance = "ERROR";
        id = "Min_max_tolerance_" + java.util.UUID.randomUUID();
        if (inputData.length > 1) {
            minMaxTolerance = (String) inputData[1];
            /* define the minimum maximum tolerance entity */
            Resource minMaxToleranceEntity = provMgr.addEntity(id, "parameter", false, false);
            /* show that the minimum maximum tolerance parameter has its current value */
            provMgr.value(minMaxToleranceEntity, minMaxTolerance);
            /* show that the minimum maximum tolerance was used to find historical tolerances */
            provMgr.used(findHistoricalTolerancesActivity, minMaxToleranceEntity);
        }
        /* define the resulting historical tolerances data */
        id = "Historical_tolerances_" + java.util.UUID.randomUUID();
        Resource historicalTolerancesEntity = provMgr.addEntity(id, "Historical_tolerances", false, false);
        /* show that finding historical tolerances generated historical tolerances */
        provMgr.generated(findHistoricalTolerancesActivity, historicalTolerancesEntity);
        /* return historical tolerances data definition */
        Object[] outputResources = new Object[1];
        outputResources[0] = historicalTolerancesEntity;
        return outputResources;
    }

    private static Object[] findClimatology(Object inputData[], ProvMgr provMgr, Date start) {
        Date end = new Date();
        /* get provMgr unless uninitialized */
//        ProvMgr provMgr = null;
//        provMgr = ProvMgr.getInstance();
//        if (!provMgr.isInitialized()) {
//            return null;
//        }
        /* get input if it is actually usable as a resource */
        Resource daysOverThresholdEntity;
        if (inputData != null && inputData.length > 0 && inputData[0] instanceof Resource) {
            daysOverThresholdEntity = (Resource) inputData[0];
        } else { // just fail before modifying prov model
            return null;
        }
        /* define the activity that occurs from finding climatology */
        String id = "Find_climatology_"
                + java.util.UUID.randomUUID();
        Resource findClimatologyActivity = provMgr.addActivity(id, "Find_climatology", false, false);
        /* show when the find climatology activity started */
        provMgr.startedAtTime(findClimatologyActivity, start);
        /* show when the find climatology activity ended */
        provMgr.endedAtTime(findClimatologyActivity, end);
        /* show that the activity was associated with the username of the server */
        // note: this should change to the username of the person
        //       responsible for initiating the simulations from the webapp
        try {
            String userName = System.getProperty("user.name");
            Resource user = provMgr.addEntity(userName, "username", false, false);
            provMgr.wasAssociatedWith(findClimatologyActivity, user);
        } catch (SecurityException e) {
        }
        // record the climatology years parameter
        String climatologyYears = "ERROR";
        id = "Climatology_years_" + java.util.UUID.randomUUID();
        if (inputData.length > 1 && inputData[1] instanceof String) {
            climatologyYears = (String) inputData[1];
            /* define the climatology years entity */
            Resource climatologyYearsEntity = provMgr.addEntity(id, "paramater", false, false);
            /* show that the climatology years parameter has its value */
            provMgr.value(climatologyYearsEntity, climatologyYears);
            /* show that the climatology years parameter was used to find climatology */
            provMgr.used(findClimatologyActivity, climatologyYearsEntity);
        }
        /* show that the days over threshold data was used to find climatology */
        provMgr.used(findClimatologyActivity, daysOverThresholdEntity);
        /* define the resulting climatology data */
        id = "Climatology_" + java.util.UUID.randomUUID();
        Resource climatologyEntity = provMgr.addEntity(id, "Climatology", false, false);
        /* show that finding climatology generated climatology data */
        provMgr.generated(findClimatologyActivity, climatologyEntity);
        /* return climatology data definition */
        Object[] outputResources = new Object[1];
        outputResources[0] = climatologyEntity;
        return outputResources;
    }

    private static Object[] leastSquaredRegression(Object[] inputData, ProvMgr provMgr, Date start) {
        Date end = new Date();
        /* get provMgr unless uninitialized */
//        ProvMgr provMgr = null;
//        provMgr = ProvMgr.getInstance();
//        if (!provMgr.isInitialized()) {
//            return null;
//        }
        /* get input if it is actually usable as a resource */
        Resource daysOverThresholdEntity;
        if (inputData != null && inputData.length > 0 && inputData[0] instanceof Resource) {
            daysOverThresholdEntity = (Resource) inputData[0];
        } else { // just fail before modifying prov model
            return null;
        }
        /* define the activity that occurs from finding least squared regression */
        String id = "Find_least_squared_regression_"
                + java.util.UUID.randomUUID();
        Resource leastSquaredRegressionActivity = provMgr.addActivity(id, "Find_least_squared_regression", false, false);
        /* show when the least squared regression activity started */
        provMgr.startedAtTime(leastSquaredRegressionActivity, start);
        /* show when the least squared regression activity ended */
        provMgr.endedAtTime(leastSquaredRegressionActivity, end);
        /* show that the activity was associated with the username of the server */
        // note: this should change to the username of the person
        //       responsible for initiating the simulations from the webapp
        try {
            String userName = System.getProperty("user.name");
            Resource user = provMgr.addEntity(userName, "username", false, false);
            provMgr.wasAssociatedWith(leastSquaredRegressionActivity, user);
        } catch (SecurityException e) {
        }
        // record the start year position parameter
        String startYearPosition = "ERROR";
        id = "Start_year_position_" + java.util.UUID.randomUUID();
        if (inputData.length > 1 && inputData[1] instanceof String) {
            startYearPosition = (String) inputData[1];
            /* define the start year position entity */
            Resource startYearPositionEntity = provMgr.addEntity(id, "paramater", false, false);
            /* show that the start year position parameter has its value */
            provMgr.value(startYearPositionEntity, startYearPosition);
            /* show that the start year position parameter was used to find climatology */
            provMgr.used(leastSquaredRegressionActivity, startYearPositionEntity);
        }
        // record the end year position parameter
        String endYearPosition = "ERROR";
        id = "End_year_position_" + java.util.UUID.randomUUID();
        if (inputData.length > 2 && inputData[2] instanceof String) {
            endYearPosition = (String) inputData[2];
            /* define the end year position entity */
            Resource endYearPositionEntity = provMgr.addEntity(id, "paramater", false, false);
            /* show that the end year position parameter has its value */
            provMgr.value(endYearPositionEntity, endYearPosition);
            /* show that the end year position parameter was used to find climatology */
            provMgr.used(leastSquaredRegressionActivity, endYearPositionEntity);
        }
        /* show that the days over threshold data was used to find the least squared regression */
        provMgr.used(leastSquaredRegressionActivity, daysOverThresholdEntity);
        /* define the least squared regression data */
        id = "Least_squared_regression_"
                + java.util.UUID.randomUUID();
        Resource leastSquaredRegressionEntity = provMgr.addEntity(id, "Least_squared_regression", false, false);
        /* show that finding the least squared regression generated least squared regression data */
        provMgr.generated(leastSquaredRegressionActivity, leastSquaredRegressionEntity);
        /* return least squared regression data definition */
        Object[] outputResources = new Object[1];
        outputResources[0] = leastSquaredRegressionEntity;
        return outputResources;
    }

    private static Object[] findToe(Object[] inputData, ProvMgr provMgr, Date start) {
        Date end = new Date();
        // get provMgr unless uninitialized
//        ProvMgr provMgr = null;
//        provMgr = ProvMgr.getInstance();
//        if (!provMgr.isInitialized()) {
//            return null;
//        }
        /* get input if it is actually usable as resources */
        Resource historicalTolerancesEntity, climatologyEntity, leastSquaredRegression;
        if (inputData != null && inputData.length > 2 && inputData[0] instanceof Resource && inputData[1] instanceof Resource && inputData[2] instanceof Resource) {
            historicalTolerancesEntity = (Resource) inputData[0];
            climatologyEntity = (Resource) inputData[1];
            leastSquaredRegression = (Resource) inputData[2];
        } else { // just fail before modifying prov model
            return null;
        }
        /* define the activity that occurs from finding time of emergence */
        String id = "Find_Time_of_emergence_"
                + java.util.UUID.randomUUID();
        Resource findTimeOfEmergenceActivity = provMgr.addActivity(id, "Find_Time_of_emergence", false, false);
        /* show when the time of emergence activity started */
        provMgr.startedAtTime(findTimeOfEmergenceActivity, start);
        /* show when the time of emergence activity ended */
        provMgr.endedAtTime(findTimeOfEmergenceActivity, end);
        /* show that the activity was associated with the username of the server */
        // note: this should change to the username of the person
        //       responsible for initiating the simulations from the webapp
        try {
            String userName = System.getProperty("user.name");
            Resource user = provMgr.addEntity(userName, "username", false, false);
            provMgr.wasAssociatedWith(findTimeOfEmergenceActivity, user);
        } catch (SecurityException e) {
        }
        // record the number of years parameter
        String numberOfYears = "200";
        id = "Number_of_years_" + java.util.UUID.randomUUID();
        if (inputData.length > 3 && inputData[3] instanceof String) {
            numberOfYears = (String) inputData[3];
            /* define the number of years entity */
            Resource numberOfYearsEntity = provMgr.addEntity(id, "paramater", false, false);
            /* show that the number of years parameter has its value */
            provMgr.value(numberOfYearsEntity, numberOfYears);
            /* show that the number of years parameter was used to find time of emergence */
            provMgr.used(findTimeOfEmergenceActivity, numberOfYearsEntity);
        }
        /* show that the historical tolerances data was used to find the time of emergence */
        provMgr.used(findTimeOfEmergenceActivity, historicalTolerancesEntity);
        /* show that the climatology data was used to find the time of emergence */
        provMgr.used(findTimeOfEmergenceActivity, climatologyEntity);
        /* show that the least squared regression data was used to find the time of emergence */
        provMgr.used(findTimeOfEmergenceActivity, leastSquaredRegression);
        /* define the time of emergence data */
        id = "Time_of_emergence_"
                + java.util.UUID.randomUUID();
        Resource timeOfEmergenceEntity = provMgr.addEntity(id, "Time_of_emergence", false, false);
        /* show that finding the time of emergence generated time of emergence data */
        provMgr.generated(findTimeOfEmergenceActivity, timeOfEmergenceEntity);
        /* return time of emergence data definition */
        Object[] outputResources = new Object[1];
        outputResources[0] = timeOfEmergenceEntity;
        return outputResources;
    }

    public static Object[] record(String functionName, Object[] args, ProvMgr provMgr, Date start) {
        Object[] outputResources = null;
        switch (functionName) {
            case "readDataIntoPlaces":
                outputResources = readDataIntoPlaces(args, provMgr, start);
                break;
            case "findHistoricalTolerance":
                outputResources = findHistoricalTolerance(args, provMgr, start);
                break;
            case "findClimatology":
                outputResources = findClimatology(args, provMgr, start);
                break;
            case "leastSquaredRegression":
                outputResources = leastSquaredRegression(args, provMgr, start);
                break;
            case "findToe":
                outputResources = findToe(args, provMgr, start);
                break;
            default:
                break;
        }
        return outputResources;
    }
}
