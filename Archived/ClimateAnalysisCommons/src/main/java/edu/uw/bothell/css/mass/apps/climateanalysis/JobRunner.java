/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis;

import edu.uw.bothell.css.dsl.MASS.MASS;

/**
 *
 * JobRunner initializes the mass library and executes jobs.
 */
public class JobRunner {

    public static String JOBS_DIR = "jobs";

    int port = 45454;
    int numProc = 1;
    int numThr = 1;

    /**
     * Starts up MASS on deployment with required libraries for clustering
     */
    public void initMassLibrary() {

        MASS.setCommunicationPort(port);       // port # to use
        MASS.setNumThreads(numThr);             // # of threads to use       
        MASS.init();
    }

    /**
     * Finishes MASS library.
     */
    public void finishMassLibrary() {
        // end mass
        MASS.finish();
    }

    /**
     * Executes a singular job.
     * @param job  job to execute
     */
    public void executeJob(Job job) {
        try {

            job.setStatus("Queued");

            job.executeJob();
            // update job status
            job.setStatus("Completed");

        } catch (Exception e) {
            MASS.getLogger().error("Error processing job",  e);
        }
    }
}
