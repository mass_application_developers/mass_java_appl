/*

 	MASS Java Software License
	© 2012-2017 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2017 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.mass.apps.climateanalysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ucar.ma2.ArrayInt;
import ucar.ma2.DataType;
import ucar.ma2.InvalidRangeException;
import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFileWriter;
import ucar.nc2.Variable;

/**
 * Writes netcdf output files. 
 *
 */
public class NetCdf {

    public void writeToeFile(String filename, int x, int y, int[][] values) {

        NetcdfFileWriter dataFile = null;

        try {
            dataFile = NetcdfFileWriter.createNew(NetcdfFileWriter.Version.netcdf3, filename);

            // Create netCDF dimensions,
            Dimension xDim = dataFile.addDimension(null, "x", x);
            Dimension yDim = dataFile.addDimension(null, "y", y);

            // define dimensions
            List<Dimension> dims = new ArrayList<>();
            dims.add(xDim);
            dims.add(yDim);

            // Define a netCDF variable. The type of the variable in this case
            // is ncInt (32-bit integer).
            Variable dataVariable = dataFile.addVariable(null, "toe", DataType.INT, dims);
            Attribute attr1 = new Attribute("_FillValue", 0);
            dataVariable.addAttribute(attr1);
            // create the file
            dataFile.create();

            // This is the data array we will write. It will just be filled
            // with a progression of numbers for this example.
            ArrayInt.D2 dataOut = new ArrayInt.D2(xDim.getLength(), yDim.getLength());

            // Create some pretend data. If this wasn't an example program, we
            // would have some real data to write, for example, model output.
            int i, j;

            for (i = 0; i < xDim.getLength(); i++) {
                for (j = 0; j < yDim.getLength(); j++) {
                    //  dataOut.set(i, j, i * NY + j);
                    dataOut.set(i, j, values[i][j]);
                }
            }

            // Write the pretend data to the file. Although netCDF supports
            // reading and writing subsets of data, in this case we write all
            // the data in one operation.
            dataFile.write(dataVariable, dataOut);

        } catch (IOException | InvalidRangeException e) {
            e.printStackTrace();

        } finally {
            if (null != dataFile) {
                try {
                    dataFile.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }

}
