#/bin/bash
######################################################################
#
#       Kill MASS.MProcss for user
#       ---------------------------
#       Just a small script to KILL all MASS.MProcess process for the
#	user on each of the machines listed in the machinefile.txt file
#
#       By:     Richard Romanus
#       Date:   07/01/2013
#
######################################################################

filename="./machinefile.txt"

for host in `cat ${filename}`
do
	tmp=`ssh ${USER}@${host} 'ps -ef  | grep "${USER}"
			' | grep "MASS.MProcess" | grep -v "grep"` 

	for pNum in `echo $tmp | awk '{print $2}' `
	do
	  echo -n "${host}: killing ${pNum}..." 
		ssh ${USER}@${host} "kill ${pNum}"
		echo " done."
	done
done
