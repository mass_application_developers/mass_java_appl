/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.mass.apps.sugarscape;

import edu.uw.bothell.css.dsl.MASS.*;             // Library for Multi-Agent Spatial Simulation       
import java.util.*;  

// Land Array
public class Land extends Place {

	// Function identifiers
	public static final int exchange_ = 0;
	public static final int update_ = 1;
	public static final int init_ = 2;
	public static final int collectLocData_ = 3;

   	// Setup the array size and index location (x,y), and sugar inventory
  	private int sizeX, sizeY;
  	private int myX, myY;
	public int sugar;
  	int[] nbrNumAgents = null;
	int[] nbrNumSugar  = null;
	int vDist;

	public Land( Object object ) { 
		int Dist = 1;	
		
		 // Define the neighbors of each cell   
        Vector<int[]> neighbors = new Vector<int[]>( );
		for( int x = 0 - Dist; x <= Dist; x++ ) {
       	    for( int y = 0 - Dist; y <= Dist; y++ ) {
           		if( !(x == 0 && y == 0) ) neighbors.add( new int[]{ x, y } );
       	    }
		}
		setNeighbors(neighbors);
	}

  	/**
     	* @param funcId the function Id to call
     	* @param args argumenets passed to this funcId.
     	*/
  	// --------------------------------------------------------------------------
	public Object callMethod( int funcId, Object args ) {
		switch ( funcId ) {
			case init_ : return init( args );
			case exchange_: return exchange( args );
			case update_: return update( args );
			case collectLocData_ : return collectLocationData( args );
		}
		return null;
	}

	// Initialize the place -----------------------------------------------------
  	public Object init( Object args ) {
        sizeX = getSize()[0]; sizeY = getSize()[1]; // size  is the base data members
        myX = getIndex()[0];  myY = getIndex()[1];  // index is the base data members

		// Setup Visual Distance and the arrays for 
		// storing my neighbors agent and sugar count
		vDist = 8;
        int arrySize =  4 * ((vDist * vDist) + vDist);
  		nbrNumAgents = new int[ arrySize ]; 
		nbrNumSugar  = new int[ arrySize ]; 
        for( int i = 0; i < arrySize; i++ ) {
  			nbrNumAgents[i] = 0;
			nbrNumSugar[i]  = 0;
        }

		// Place Sugar Randomly all over the grid
		int numSugarValues = 5;
		Random gen = new Random();
		int tmpValue = gen.nextInt( numSugarValues * 2 );
		if( tmpValue >= numSugarValues ) 
			sugar = 0;
		else
			sugar = tmpValue;
        return null;
  	}

	/**
	  * Is called from exchangeAll( ) to exchange #agents with my neighbors
	  * @param args formally requested but actuall not used.
	*/
	public Object exchange( Object args ) {
		int[] unitData = { getNumAgents(), sugar };
		return (Object)unitData;
	}


	/**
	  * Is called from callAll( ) to update my neighbors' #agents and #sugar
	  * @param args formally requested but actuall not used.
	*/
	public Object update( Object args ) {
		int arrySize = getInMessages().length;
		for( int i = 0; i < arrySize; i++ ) {
		
			if( getInMessages()[i] == null ) {
				nbrNumAgents[i] = 0;
				nbrNumSugar[i]  = 0;
			} else {			
				nbrNumAgents[i] = ((int[])getInMessages()[i])[0];
				nbrNumSugar[i]  = ((int[])getInMessages()[i])[1];
			}
		}
		return null;
	}

	/** Used by Unit (agent) to get the index of array locations 
	  * that contain the neighbors for the unit with a different 
	  * (smaller) visual distance than what is stored by the land
	  * @param unitVDist 	Unit's visual distance 
   	*/
	public int[] getNbrLocations( int unitVDist ) {
		if( unitVDist > vDist ) return null;

		int arrySize  =  4 * ((unitVDist * unitVDist) + unitVDist);
		int[] nbrList = new int[ arrySize ];

		int maxIndex = 0;
		int tmpIndex = 0;

		for( int x = 0 - vDist; x <= vDist; x++ ) {
			for( int y = 0 - vDist; y <= vDist; y++ ) {
				if( !(x == 0 && y == 0) && 
						( x >= 0 - unitVDist ) && ( x <= unitVDist ) &&
						( y >= 0 - unitVDist ) && ( y <= unitVDist ) ) {

					nbrList[ tmpIndex ] = maxIndex;
					tmpIndex++;
				}
				if( !(x == 0 && y == 0) ) 
					maxIndex++;
			}
		}
		return nbrList;
	}

	/**
	  *
	*/ 
	public boolean consumeSugar() {
		if( sugar > 0 ) { 
			//System.err.print( "Sugar consumed.... Level: " + sugar );
			sugar--;
			//System.err.println( " -> " + sugar );
			return true;
		}
		return false;
	}

  	/** Used for collecting the local data to diplay
   	  * @param args formally declared but actually not used.
   	*/ 
  	public Object collectLocationData( Object args ) {
		int[] unitData = { getNumAgents(), sugar };
      	return (Object)unitData;
  	}
}
