/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.mass.apps.sugarscape;

import edu.uw.bothell.css.dsl.MASS.*;             // Library for Multi-Agent Spatial Simulation
import java.util.*;

import com.sun.org.apache.bcel.internal.generic.LAND;

import java.awt.*;       
import java.awt.event.*;

public class Sugarscape {
	
    public static void main( String[] args ) throws Exception {

		// Verify user provide enough input data
		if( args. length < 4 ) {
			System.err.println( "\nUsage:\n\tjava SugarScape size nAgents maxTime interval");
			System.exit( -1 );
		}

		// Set variables with the user input data
		int size = Integer.parseInt( args[0] );
		int nAgents = Integer.parseInt( args[1] );
        int maxTime = Integer.parseInt( args[2] );
        int interval = Integer.parseInt( args[3] );
        boolean showGfx = args.length == 5;

		//int vDist = 1;	

		//MASS.setLoggingLevel( LogLevel.DEBUG );
        
        Date startTime = new Date();
        
        long initTime;
        long placesCallAllTime = 0;
        long placesExchangeAllTime = 0;
        long agentsCallAllTime = 0;
        long agentsManageAllTime = 0;
	  
		MASS.init( );

        // Create a Land array.
        Places land = new Places( 1, Land.class.getName(), null, size, size );				//<< exchangeAll()
		land.callAll( Land.init_ );
        //Places land = new Places( 1, "Land", null, vDist, false, size, size );	//<< exchangeBoundary()

        // Populate Agents (unit) on the Land array
        Agents unit = new Agents( 2, Unit.class.getName(), null, land, nAgents );

        // Define the neighbors of each cell   
        //Vector<int[]> neighbors = new Vector<int[]>( );
		//for( int x = 0 - vDist; x <= vDist; x++ ) {
       	//    for( int y = 0 - vDist; y <= vDist; y++ ) {
        //   		if( !(x == 0 && y == 0) ) neighbors.add( new int[]{ x, y } );
       	//    }
		//}
		//land.setAllPlacesNeighbors( neighbors );
        
		if ( interval > 0 && showGfx ) startGraphics( size );
		
		//System.out.println("Begin Simulation");
		
		initTime = ( new Date() ).getTime() - startTime.getTime();
		Date simStart = new Date();
		
        // Start simulatin time
        for ( int time = 0; time < maxTime; time++ ) {
			// Exchange #agents with neighbors
        	Date exchangeTime = new Date();
            land.exchangeAll( 1, Land.exchange_ );			// << exchangeAll()
            placesExchangeAllTime += ( new Date() ).getTime() - exchangeTime.getTime();

            Date PCATime = new Date();
            land.callAll( Land.update_ );
            placesCallAllTime += ( new Date() ).getTime() - PCATime.getTime();

            // Move agents to a neighbor with the least population
            Date ACATime = new Date();
            unit.callAll( Unit.decideNewPosition_ );
            agentsCallAllTime += ( new Date() ).getTime() - ACATime.getTime();
            
            Date manageTime = new Date();
            unit.manageAll( );
            agentsManageAllTime += ( new Date() ).getTime() - manageTime.getTime();
            

			// Collect and print (if selected) the local data
            if ( time % interval == 0 ) {
                Object[] locData = land.callAll( Land.collectLocData_, (Object[]) null );
				if ( showGfx ) writeToGraphics( locData, size );
            }
            
            //System.out.println("Loop Done");
        }
        
        long simulationTime = ( new Date() ).getTime() - simStart.getTime();

		// End graphics
		if( showGfx ) finishGraphics();

        // finish MASS
        MASS.finish( );
        
        Date endTime = new Date();
        System.out.println( "Total Time (ms): " + ( endTime.getTime() - startTime.getTime() ) ); 
        System.out.println( "Init Time (ms): " + initTime );
        System.out.println(" Places callAll Time (ms): " + placesCallAllTime );
        System.out.println(" Places exchangeAll Time (ms): " + placesExchangeAllTime );
        System.out.println(" Agents callAll Time (ms): " + agentsCallAllTime );
        System.out.println(" Agents manageAll Time (ms): " + agentsManageAllTime );
    }
	
	// Graphics ---------------------------------------------------------------------------------
  	private static final int defaultN = 100; 	// the default system size
  	private static final int defaultCellWidth = 8;
  	private static Color 	 bgColor;            	//white background
  	private static Frame 	 gWin;               	// a graphics window
  	private static int 	 cellWidth;            	// each cell's width in the window
  	private static Insets 	 theInsets;         	// the insets of the window 
  	private static Color 	 agentColor[];         	// agent color
  	private static Color 	 sugarColor[];         	// sugar color
  	private static int 	 N = 0;                	// array size
	
	// Start a graphics window ------------------------------------------------------------------
	public static void startGraphics( int arraySize ) {
		// define the array size
		N = arraySize;

		// Graphics must be handled by a single thread
		bgColor = new Color( 255, 255, 255 );	//white background

		// Calculate the cell width in a window
		cellWidth = (int)((double) defaultCellWidth / ((double) N / (double) defaultN ));
		if ( cellWidth == 0 ) cellWidth = 1;

		// Initialize window and graphics:
		gWin = new Frame( "SugarScape Simulation" );
		gWin.setLocation( 50, 50 );  		// screen coordinates of top left corner

		gWin.setResizable( false );
		gWin.setVisible( true );     
		theInsets = gWin.getInsets();
		Dimension frameDim = new Dimension (N * cellWidth + theInsets.left + theInsets.right,
											N * cellWidth + theInsets.top + theInsets.bottom);
		gWin.setSize(frameDim);

		// Wait for frame to get initialized
		long resumeTime = System.currentTimeMillis() + 1000;
		do {} while (System.currentTimeMillis() < resumeTime);

		// Paint the background
		Graphics g = gWin.getGraphics( );
		g.setColor( bgColor );
		g.fillRect( theInsets.left, theInsets.top, N * cellWidth, N * cellWidth );

		// Agent Colors
		agentColor = new Color[10];
		agentColor[0] = new Color( 0x0066FF );
		agentColor[1] = new Color( 0x0033FF );
		agentColor[2] = new Color( 0x0000FF );   // blue
		agentColor[3] = new Color( 0x0000CC );
		agentColor[4] = new Color( 0x000099 );
		agentColor[5] = new Color( 0x330099 );
		agentColor[6] = new Color( 0x660099 );
		agentColor[7] = new Color( 0x660066 );
		agentColor[8] = new Color( 0x660033 );
		agentColor[9] = new Color( 0x660000 );

		// Sugar Colors
		sugarColor = new Color[10];
		sugarColor[0] = new Color( 0xFFFFFF ); 	// white
		sugarColor[1] = new Color( 0x00FF99 );	// Lt Green
		sugarColor[2] = new Color( 0x00FF66 );
		sugarColor[3] = new Color( 0x00FF33 );
		sugarColor[4] = new Color( 0x00CC33 );
		sugarColor[5] = new Color( 0x00FF00 );
		sugarColor[6] = new Color( 0x00CC00 );
		sugarColor[7] = new Color( 0x009900 );
		sugarColor[8] = new Color( 0x006600 );
		sugarColor[9] = new Color( 0x003300 );
  	}
	
	// Update graphics window with new cell information -----------------------------------------
  	public static void writeToGraphics( Object[] locData, int arraySize ) {
        Graphics g = gWin.getGraphics( );

		int maxAgentColor = agentColor.length;
		int maxSugarColor = sugarColor.length;

		for ( int i = 0; i < arraySize; i++  ) {
		   for ( int j = 0; j < arraySize; j++ ) {

				Color cellColor = null;
				int numAgents = ((int[])( (Object)locData[ i * arraySize + j ] ))[0];
				int numSugar  = ((int[])( (Object)locData[ i * arraySize + j ] ))[1];

				// **********************************************************************************
				if( numAgents > 0 ) { 		// Has agents
					cellColor = ( numAgents >= maxAgentColor ) ? agentColor[ maxAgentColor - 1 ]  : agentColor[ numAgents ];
				} else if( numSugar > 0 ) {	// Has sugar
					cellColor = ( numSugar >= maxSugarColor ) ? sugarColor[ maxSugarColor - 1 ] : sugarColor[ numSugar ];
				} else {
					cellColor = bgColor;
				}

				// show a cell
				g.setColor( cellColor );
				g.fill3DRect( theInsets.left + i * cellWidth, theInsets.top  + j * cellWidth, cellWidth, cellWidth, true ); //mod by John
			}
		}
  	}
	
	// Finish graphics window -------------------------------------------------------------------
  	public static void finishGraphics() {
        Graphics g = gWin.getGraphics( );

		if( g != null ) {
        	g.dispose( );
        	gWin.removeNotify( );
        	gWin = null;
		}
  	}
}
