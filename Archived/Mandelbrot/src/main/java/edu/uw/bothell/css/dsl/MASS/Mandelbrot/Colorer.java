/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.Mandelbrot;

import edu.uw.bothell.css.dsl.MASS.Agent;

@SuppressWarnings("serial")
public class Colorer extends Agent {

  public static final int CALCULATE_COLOR = 0;
  public static final int MIGRATE = 1;
  public static final int INIT_MIGRATE = 2;

  /**
   * This constructor will be called upon instantiation by MASS The Object
   * supplied MAY be the same object supplied when Places was created
   * 
   * @param obj
   */
  public Colorer(Object obj) { }

  /**
   * This method is called when "callAll" is invoked from the master node
   */
  public Object callMethod(int method, Object o) {

    switch (method) {

    case CALCULATE_COLOR:
      return calculateColor(o);

    case MIGRATE:
      return move(o);
    case INIT_MIGRATE:
      return initMigrate(o);
    default:
      return new String("Unknown Method Number: " + method);

    }

  }

  private Object initMigrate(Object o) {
    Point p = (Point)o;
    migrate(p.x, p.y);
    return o;
  }

  /**
   * Calculate the color of the current place in the Matrix
   * http://en.wikipedia.org/wiki/Mandelbrot_set#Computer_drawings
   * 
   * @param o
   * @return
   */
  public Object calculateColor(Object o) {
    int xModifier = this.getPlace().getIndex()[0];
    int yModifier = this.getPlace().getIndex()[1];
    double x0 = -2.5 + ((double)yModifier / (double) this.getPlace().getSize()[1]) * 3.5;
    double y0 = -1.0 + ((double)xModifier / (double) this.getPlace().getSize()[0]) * 2.0;
    double x = 0.0, y = 0.0;
    int iteration = 0;
    while (x*x + y*y < 4.0 && iteration < Program.MAX_ITERATION) {
      double xtemp = x*x - y*y + x0;
      y = 2 * x * y + y0;
      x = xtemp;
      iteration++;
    }
    return new Integer(iteration);
  }

  /**
   * Move this Agent to the next position in the X-coordinate
   * 
   * @param o
   * @return
   */
  public Object move(Object o) {

    int xModifier = this.getPlace().getIndex()[0];
    int yModifier = this.getPlace().getIndex()[1];
    yModifier++;
    if(yModifier >= this.getPlace().getSize()[1]) {
      yModifier = 0;
      ++xModifier;
    }
    migrate(xModifier, yModifier);
    return o;
  }

}
