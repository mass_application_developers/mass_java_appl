/*

 	MASS Java Software License
	© 2012-2015 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:      

	© 2012-2015 University of Washington. MASS was developed by Computing and Software Systems at University of 
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS.Mandelbrot;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.uw.bothell.css.dsl.MASS.Agents;
import edu.uw.bothell.css.dsl.MASS.MASS;
import edu.uw.bothell.css.dsl.MASS.Places;

public class Program {

	private static final String NODE_FILE = "nodes.xml";
  public static final int MAX_ITERATION = 200000;
  public static int MATRIX_SIZE = 4032, NTHREADS = 4, CALC_PER_AGENT, AGENT_SIZE = 4032;
	
	public static void main(String[] args) {
    if(args.length > 1) {
      MATRIX_SIZE = Integer.parseInt(args[0]);
      NTHREADS = Integer.parseInt(args[1]);
      AGENT_SIZE = Integer.parseInt(args[2]);
	  }
    CALC_PER_AGENT = (MATRIX_SIZE * MATRIX_SIZE) / AGENT_SIZE;
    System.out.println("CALC_PER_AGENT = " + CALC_PER_AGENT);

		// init MASS library
		MASS.setNodeFilePath(NODE_FILE);
		MASS.setCommunicationPort(50951);
		MASS.setNumThreads(NTHREADS);

    String startStr = "START - " + (new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS").format(new Date())) + " - MAX ITER SYNC = " + MAX_ITERATION;
    System.out.println(startStr);
    long massStart = System.nanoTime();
    // start MASS
    MASS.init();
		
		int[][] colors = new int[MATRIX_SIZE][MATRIX_SIZE];
		
		Places places = new Places(1, "edu.uw.bothell.css.dsl.MASS.Mandelbrot.Matrix", (Object) new Integer(0), MATRIX_SIZE, MATRIX_SIZE);
		System.err.println((new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS").format(new Date())) + " Places init done");
		// create Agents (number of Agents = y in this case), in Places
		Agents agents = new Agents(1, "edu.uw.bothell.css.dsl.MASS.Mandelbrot.Colorer", null, places, AGENT_SIZE);
		System.err.println((new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS").format(new Date())) + " Agents init done");
    long syncStart = System.nanoTime();
		Object[] agentsCallAllObjs = new Object[AGENT_SIZE];
		for(int i = 0; i < AGENT_SIZE; i++){
		  Point p = new Point();
		  p.x = (i * CALC_PER_AGENT / MATRIX_SIZE);
		  p.y = (i * CALC_PER_AGENT) % MATRIX_SIZE;
		  agentsCallAllObjs[i] = p;
		}
		System.err.println("callAll sync start");
		Object[] calledAgentsResults = (Object[]) agents.callAll(Colorer.INIT_MIGRATE, agentsCallAllObjs);
		agents.manageAll();
		calledAgentsResults = (Object[]) agents.callAll(Colorer.CALCULATE_COLOR, agentsCallAllObjs);
		for(int i = 0; i < AGENT_SIZE; i++)
		{
		  colors[i * CALC_PER_AGENT / MATRIX_SIZE][(i * CALC_PER_AGENT) % MATRIX_SIZE] = (int)calledAgentsResults[i];
		}
		
		// move all Agents four times to cover all dimensions in Places
		for (int i = 1; i < CALC_PER_AGENT; i ++) {
			
			// tell Agents to move
			agents.callAll(Colorer.MIGRATE);
			
			// sync all Agent status
			agents.manageAll();
			
			calledAgentsResults = (Object[]) agents.callAll(Colorer.CALCULATE_COLOR, agentsCallAllObjs);
			for(int j = 0; j < AGENT_SIZE; j++)
	    {
	      colors[(j * CALC_PER_AGENT + i) / MATRIX_SIZE][(j * CALC_PER_AGENT + i) % MATRIX_SIZE] = (int)calledAgentsResults[j];
	    }
		}
		long syncEnd = System.nanoTime();
		
		// orderly shutdown
		MASS.finish();
    
    long massEnd = System.nanoTime();
    String endStr = "END - " + (new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS").format(new Date()));
    System.out.println(endStr);

    double massTime = (double)(massEnd - massStart) / 1000000000.0;
    double syncTime = (double)(syncEnd - syncStart) / 1000000000.0;
    String durationStr = "DONE: massTime = " + massTime + " sec; async Time = " + syncTime + " sec";
    System.out.println(durationStr);
    saveToFile(startStr+ " " + endStr + " " + durationStr, colors);
	 }
	
private static void saveToFile(String firstline, int[][] result) {
    FileWriter fw;
    BufferedWriter bw = null;
    try {
      fw = new FileWriter("result-sync.txt");
      bw = new BufferedWriter(fw);
      bw.write(firstline);
      bw.newLine();
      
      for(int i = 0; i < result.length; i++)
      {
        for(int j = 0; j < result[i].length; j++)
        {
          bw.write(result[i][j] + "");
          if(j < result[i].length - 1) {
            bw.write(" ");
          }
        }
        bw.newLine();
      }
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      if (bw != null) {
        try {
          bw.close();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
  }
	 
}
